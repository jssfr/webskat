FROM rust:1-slim-bullseye AS build

RUN cd /tmp && cargo init foo && cd foo && cargo add tokio poem && cargo build --release && cd / && rm -rf /tmp/foo
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y && DEBIAN_FRONTEND=noninteractive apt-get install -y libclang-dev pkg-config build-essential libclang-common-*-dev libgcc-*-dev libssl-dev
RUN cargo install wasm-bindgen-cli
RUN rustup target add wasm32-unknown-unknown

RUN mkdir /app
ADD Cargo.toml Cargo.lock /app/
ADD webskat-common /app/webskat-common/
ADD webskat-js /app/webskat-js/
ADD webskat-server /app/webskat-server/
ADD webskat-client /app/webskat-client/
ADD skat /app/skat/
RUN cd /app/webskat-js && cargo build --release
RUN cd /app/webskat-js && RUST_LOG=info wasm-bindgen --target web --out-dir ../webskat-server/static ../target/wasm32-unknown-unknown/release/webskat_js.wasm
RUN cd /app && cp target/wasm32-unknown-unknown/release/webskat_js.wasm webskat-server/static
RUN cd /app/webskat-server && cargo build --release

FROM debian:bullseye-slim
RUN mkdir /app
ADD webskat/templates /app/templates
COPY --from=build /app/target/release/server /app/server
COPY --from=build /app/webskat-server/static /app/static
WORKDIR /app
CMD ["/app/server"]
EXPOSE 7528
