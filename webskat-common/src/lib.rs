mod api;
mod seat;

use serde::{Deserialize, Serialize};

use skat::{GameType, GameValue, LossReasons, Matadors, Modifiers};

pub use api::{PlayRequest, PlayResponse};
pub use seat::{PerSeat, Seat};

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct GameSummary {
	pub declarer: Seat,
	pub declarer_won_points: u8,
	pub declarer_won_cards: u8,
	pub loss_reasons: LossReasons,
	pub matadors: Matadors,
	pub game_type: GameType,
	pub game_value: GameValue,
	pub modifiers: Modifiers,
	pub scores: PerSeat<i32>,
}

impl GameSummary {
	pub fn map(other: skat::GameSummary, declarer: Seat) -> Self {
		let mut scores = PerSeat::init_fn(|| 0i32);
		// TODO(4seats): Seeger/Fabian require +30 for a 4 seat game
		if other.loss_reasons.is_empty() {
			scores[declarer] += 50 + other.game_value as i32;
		} else {
			for (seat, score) in scores.enumerate_mut() {
				if seat == declarer {
					*score -= (other.game_value as i32) * 2;
				} else {
					*score += 40;
				}
			}
		}
		Self {
			declarer,
			declarer_won_cards: other.declarer_won_cards,
			declarer_won_points: other.declarer_won_points,
			loss_reasons: other.loss_reasons,
			matadors: other.matadors,
			game_type: other.game_type,
			game_value: other.game_value,
			modifiers: other.modifiers,
			scores,
		}
	}
}
