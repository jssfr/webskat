use serde::{Deserialize, Serialize};

use skat::{AnnouncableModifier, Card, Commitment, GameType, GameValue, PerPlayer, Seed, State};

use crate::{GameSummary, PerSeat};

#[derive(Deserialize, Serialize, Debug)]
#[serde(tag = "kind")]
pub enum PlayRequest {
	#[serde(rename = "poll")]
	PollState,
	#[serde(rename = "commit_seed")]
	CommitSeed { commitment: Commitment },
	#[serde(rename = "set_seed")]
	SetSeed { seed: Seed },
	#[serde(rename = "call_bid")]
	CallBid { bid: Option<GameValue> },
	#[serde(rename = "respond_bid")]
	RespondBid { hold: bool },
	#[serde(rename = "take_skat")]
	TakeSkat,
	#[serde(rename = "declare")]
	Declare {
		game_type: GameType,
		modifiers: Option<AnnouncableModifier>,
		push_cards: Option<[Card; 2]>,
	},
	#[serde(rename = "play")]
	Play { card: Card },
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(tag = "kind")]
pub enum PlayResponse {
	#[serde(rename = "lobby")]
	Lobby { seats: PerSeat<Option<String>> },
	#[serde(rename = "playing")]
	Playing {
		players: PerPlayer<String>,
		seats: PerSeat<String>,
		past_games: Option<Vec<Option<GameSummary>>>,
		game: State,
	},
	#[serde(rename = "error")]
	Error { message: String },
}
