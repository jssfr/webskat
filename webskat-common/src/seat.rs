use std::fmt;
use std::ops::{Index, IndexMut};

use num_enum::TryFromPrimitive;

use serde::{de::Deserializer, ser::Serializer, Deserialize, Serialize};

use serde_repr::{Deserialize_repr, Serialize_repr};

const NSEATS: usize = 3;

#[derive(
	Debug,
	Clone,
	Copy,
	PartialEq,
	Eq,
	PartialOrd,
	Ord,
	Hash,
	Serialize_repr,
	Deserialize_repr,
	TryFromPrimitive,
)]
#[repr(u8)]
pub enum Seat {
	First,
	Second,
	Third,
	// TODO: Fourth seat
}

pub const SEATS: [Seat; NSEATS] = [Seat::First, Seat::Second, Seat::Third];

#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct PerSeat<T>([T; NSEATS]);

impl<'de, T: Deserialize<'de>> Deserialize<'de> for PerSeat<T> {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> std::result::Result<Self, D::Error> {
		Ok(<[T; NSEATS] as Deserialize<'de>>::deserialize(deserializer)?.into())
	}
}

impl<T: Serialize> Serialize for PerSeat<T> {
	fn serialize<S: Serializer>(&self, serializer: S) -> std::result::Result<S::Ok, S::Error> {
		<[T; NSEATS] as Serialize>::serialize(&self.0, serializer)
	}
}

impl<T: fmt::Debug> fmt::Debug for PerSeat<T> {
	fn fmt<'f>(&self, f: &'f mut fmt::Formatter) -> fmt::Result {
		let mut map = f.debug_map();
		for (player, data) in self.enumerate_ref() {
			map.entry(&player, data);
		}
		map.finish()
	}
}

type PerSeatEnumerate<T> =
	std::iter::Zip<std::array::IntoIter<Seat, 3>, std::array::IntoIter<T, 3>>;
type PerSeatEnumerateRef<'x, T> =
	std::iter::Zip<std::array::IntoIter<Seat, 3>, std::slice::Iter<'x, T>>;
type PerSeatEnumerateMut<'x, T> =
	std::iter::Zip<std::array::IntoIter<Seat, 3>, std::slice::IterMut<'x, T>>;

impl<T> PerSeat<T> {
	pub fn iter(&self) -> std::slice::Iter<T> {
		self.0.iter()
	}

	pub fn iter_mut(&mut self) -> std::slice::IterMut<T> {
		self.0.iter_mut()
	}

	pub fn enumerate_ref(&self) -> PerSeatEnumerateRef<T> {
		SEATS.into_iter().zip(self.0.iter())
	}

	pub fn enumerate_mut(&mut self) -> PerSeatEnumerateMut<T> {
		SEATS.into_iter().zip(self.0.iter_mut())
	}

	pub fn enumerate(self) -> PerSeatEnumerate<T> {
		SEATS.into_iter().zip(self.0.into_iter())
	}

	pub fn init_fn<F: FnMut() -> T>(mut f: F) -> Self {
		Self([f(), f(), f()])
	}

	pub fn from_fn<F: FnMut(Seat) -> T>(mut f: F) -> Self {
		Self([f(Seat::First), f(Seat::Second), f(Seat::Third)])
	}

	pub fn map<U, F: FnMut(T) -> U>(self, f: F) -> PerSeat<U> {
		let mut iter = self.0.into_iter().map(f);
		PerSeat([
			iter.next().unwrap(),
			iter.next().unwrap(),
			iter.next().unwrap(),
		])
	}

	pub fn map_ref<'x, U: 'x, F: FnMut(&'x T) -> U>(&'x self, f: F) -> PerSeat<U> {
		let mut iter = self.0.iter().map(f);
		PerSeat([
			iter.next().unwrap(),
			iter.next().unwrap(),
			iter.next().unwrap(),
		])
	}
}

impl<T: Default> Default for PerSeat<T> {
	fn default() -> Self {
		Self(<[T; NSEATS] as Default>::default())
	}
}

impl<A> FromIterator<A> for PerSeat<A> {
	fn from_iter<T: IntoIterator<Item = A>>(iter: T) -> Self {
		let mut iter = iter.into_iter();
		let inner = [
			iter.next().expect("iterator too short"),
			iter.next().expect("iterator too short"),
			iter.next().expect("iterator too short"),
		];
		if iter.next().is_some() {
			panic!("iterator too long");
		}
		Self(inner)
	}
}

impl<T> IntoIterator for PerSeat<T> {
	type IntoIter = std::array::IntoIter<T, 3>;
	type Item = T;

	fn into_iter(self) -> Self::IntoIter {
		self.0.into_iter()
	}
}

impl<'x, T> IntoIterator for &'x PerSeat<T> {
	type IntoIter = std::slice::Iter<'x, T>;
	type Item = &'x T;

	fn into_iter(self) -> Self::IntoIter {
		self.0.iter()
	}
}

impl<'x, T> IntoIterator for &'x mut PerSeat<T> {
	type IntoIter = std::slice::IterMut<'x, T>;
	type Item = &'x mut T;

	fn into_iter(self) -> Self::IntoIter {
		self.0.iter_mut()
	}
}

impl<T> From<[T; NSEATS]> for PerSeat<T> {
	fn from(other: [T; NSEATS]) -> Self {
		Self(other)
	}
}

impl<T> Index<usize> for PerSeat<T> {
	type Output = T;

	fn index(&self, index: usize) -> &Self::Output {
		&self.0[index]
	}
}

impl<T> IndexMut<usize> for PerSeat<T> {
	fn index_mut(&mut self, index: usize) -> &mut Self::Output {
		&mut self.0[index]
	}
}

impl<T> Index<Seat> for PerSeat<T> {
	type Output = T;

	fn index(&self, index: Seat) -> &Self::Output {
		&self.0[index as u8 as usize]
	}
}

impl<T> IndexMut<Seat> for PerSeat<T> {
	fn index_mut(&mut self, index: Seat) -> &mut Self::Output {
		&mut self.0[index as u8 as usize]
	}
}
