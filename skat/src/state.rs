use std::fmt;
use std::ops::{Index, IndexMut};

use serde::{de::Deserializer, ser::Serializer, Deserialize, Serialize};

use thiserror::Error;

use crate::bidding::{BiddingResult, BiddingState, GameValue};
use crate::cards::{Card, CardSet, CardStack};
use crate::model::{
	calculate_matadors, AnnouncableModifier, GameSummary, GameType, Modifier, Modifiers, Player,
	Trick, PLAYERS,
};
use crate::play::PlayingState;
use crate::shuffle::{Seed, Shuffler};

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub struct Commitment([u8; 16]);

impl Commitment {
	pub fn from_seed(seed: &Seed) -> Self {
		let digest = ring::digest::digest(&ring::digest::SHA256, seed.as_ref());
		let mut result = [0u8; 16];
		result.copy_from_slice(&digest.as_ref()[..16]);
		Self(result)
	}
}

#[derive(Error, Debug, Clone, Serialize, Deserialize)]
pub enum GameError {
	#[error("not your turn")]
	NotYourTurn,
	#[error("wrong phase")]
	WrongPhase,
	#[error("invalid value")]
	InvalidValue,
	#[error("incomplete state")]
	IncompleteState,
	#[error("bid too low")]
	BidTooLow,
	#[error("must follow suit")]
	MustFollowSuit,
	#[error("no such card")]
	NoSuchCard,
}

pub type Result<T> = std::result::Result<T, GameError>;

#[derive(Clone, Copy)]
pub struct PerPlayer<T> {
	inner: [T; 3],
}

impl<'de, T: Deserialize<'de>> Deserialize<'de> for PerPlayer<T> {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> std::result::Result<Self, D::Error> {
		Ok(<[T; 3] as Deserialize<'de>>::deserialize(deserializer)?.into())
	}
}

impl<T: Serialize> Serialize for PerPlayer<T> {
	fn serialize<S: Serializer>(&self, serializer: S) -> std::result::Result<S::Ok, S::Error> {
		<[T; 3] as Serialize>::serialize(&self.inner, serializer)
	}
}

impl<T: fmt::Debug> fmt::Debug for PerPlayer<T> {
	fn fmt<'f>(&self, f: &'f mut fmt::Formatter) -> fmt::Result {
		let mut map = f.debug_map();
		for (player, data) in self.enumerate_ref() {
			map.entry(&player, data);
		}
		map.finish()
	}
}

type PerPlayerEnumerate<T> =
	std::iter::Zip<std::array::IntoIter<Player, 3>, std::array::IntoIter<T, 3>>;
type PerPlayerEnumerateRef<'x, T> =
	std::iter::Zip<std::array::IntoIter<Player, 3>, std::slice::Iter<'x, T>>;
type PerPlayerEnumerateMut<'x, T> =
	std::iter::Zip<std::array::IntoIter<Player, 3>, std::slice::IterMut<'x, T>>;

impl<T> PerPlayer<T> {
	pub fn iter(&self) -> std::slice::Iter<T> {
		self.inner.iter()
	}

	pub fn iter_mut(&mut self) -> std::slice::IterMut<T> {
		self.inner.iter_mut()
	}

	pub fn enumerate_ref(&self) -> PerPlayerEnumerateRef<T> {
		PLAYERS.into_iter().zip(self.inner.iter())
	}

	pub fn enumerate_mut(&mut self) -> PerPlayerEnumerateMut<T> {
		PLAYERS.into_iter().zip(self.inner.iter_mut())
	}

	pub fn enumerate(self) -> PerPlayerEnumerate<T> {
		PLAYERS.into_iter().zip(self.inner.into_iter())
	}

	pub fn reduce<F: FnMut(T, T) -> T>(self, f: F) -> T {
		self.into_iter().reduce(f).unwrap()
	}

	pub fn init_fn<F: FnMut() -> T>(mut f: F) -> Self {
		Self {
			inner: [f(), f(), f()],
		}
	}

	pub fn from_fn<F: FnMut(Player) -> T>(mut f: F) -> Self {
		Self {
			inner: [f(Player::First), f(Player::Second), f(Player::Third)],
		}
	}

	pub fn map<U, F: FnMut(T) -> U>(self, f: F) -> PerPlayer<U> {
		let mut iter = self.inner.into_iter().map(f);
		PerPlayer {
			inner: [
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
			],
		}
	}

	pub fn map_ref<'x, U: 'x, F: FnMut(&'x T) -> U>(&'x self, f: F) -> PerPlayer<U> {
		let mut iter = self.inner.iter().map(f);
		PerPlayer {
			inner: [
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
			],
		}
	}
}

impl<T: Default> Default for PerPlayer<T> {
	fn default() -> Self {
		Self {
			inner: <[T; 3] as Default>::default(),
		}
	}
}

impl<A> FromIterator<A> for PerPlayer<A> {
	fn from_iter<T: IntoIterator<Item = A>>(iter: T) -> Self {
		let mut iter = iter.into_iter();
		let inner = [
			iter.next().expect("iterator too short"),
			iter.next().expect("iterator too short"),
			iter.next().expect("iterator too short"),
		];
		if iter.next().is_some() {
			panic!("iterator too long");
		}
		Self { inner }
	}
}

impl<T> IntoIterator for PerPlayer<T> {
	type IntoIter = std::array::IntoIter<T, 3>;
	type Item = T;

	fn into_iter(self) -> Self::IntoIter {
		self.inner.into_iter()
	}
}

impl<'x, T> IntoIterator for &'x PerPlayer<T> {
	type IntoIter = std::slice::Iter<'x, T>;
	type Item = &'x T;

	fn into_iter(self) -> Self::IntoIter {
		self.inner.iter()
	}
}

impl<'x, T> IntoIterator for &'x mut PerPlayer<T> {
	type IntoIter = std::slice::IterMut<'x, T>;
	type Item = &'x mut T;

	fn into_iter(self) -> Self::IntoIter {
		self.inner.iter_mut()
	}
}

impl<T> From<[T; 3]> for PerPlayer<T> {
	fn from(other: [T; 3]) -> Self {
		Self { inner: other }
	}
}

impl<T> Index<usize> for PerPlayer<T> {
	type Output = T;

	fn index(&self, index: usize) -> &Self::Output {
		&self.inner[index]
	}
}

impl<T> IndexMut<usize> for PerPlayer<T> {
	fn index_mut(&mut self, index: usize) -> &mut Self::Output {
		&mut self.inner[index]
	}
}

impl<T> Index<Player> for PerPlayer<T> {
	type Output = T;

	fn index(&self, index: Player) -> &Self::Output {
		&self.inner[index as u8 as usize]
	}
}

impl<T> IndexMut<Player> for PerPlayer<T> {
	fn index_mut(&mut self, index: Player) -> &mut Self::Output {
		&mut self.inner[index as u8 as usize]
	}
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum State {
	Committing {
		your_index: Player,
		player_commitments: PerPlayer<Option<Commitment>>,
		dealer_commitment: Commitment,
	},
	Shuffling {
		your_index: Player,
		player_commitments: PerPlayer<Commitment>,
		player_seeds: PerPlayer<Option<Seed>>,
		dealer_commitment: Commitment,
	},
	Bidding {
		your_index: Player,
		player_seeds: PerPlayer<Seed>,
		dealer_commitment: Commitment,
		player_bids: PerPlayer<Option<GameValue>>,
		caller: Player,
		responder: Option<Player>,
		awaiting_response: bool,
		your_hand: CardSet,
		hands: PerPlayer<u8>,
	},
	PreDeclaration {
		your_index: Player,
		declarer: Player,
		called_game_value: GameValue,
		your_hand: CardSet,
		hands: PerPlayer<u8>,
		skat_taken: bool,
	},
	Playing {
		your_index: Player,
		declarer: Player,
		turn: Player,
		declarer_hand: Option<CardSet>,
		your_hand: CardSet,
		hands: PerPlayer<u8>,
		game_type: GameType,
		last_trick: Option<Trick>,
		base_modifiers: Modifiers,
		table: CardStack,
		forehand: Player,
	},
	PassedOut {
		your_index: Player,
		dealer_seed: Seed,
		hands: PerPlayer<CardSet>,
		skat: CardSet,
	},
	Concluded {
		your_index: Player,
		dealer_seed: Seed,
		won_cards: PerPlayer<CardStack>,
		called_game_value: GameValue,
		summary: GameSummary,
	},
}

impl State {
	pub fn your_hand(&self) -> Option<&CardSet> {
		match self {
			Self::Committing { .. } => None,
			Self::Shuffling { .. } => None,
			Self::Bidding { your_hand, .. } => Some(your_hand),
			Self::PreDeclaration { your_hand, .. } => Some(your_hand),
			Self::Playing { your_hand, .. } => Some(your_hand),
			Self::PassedOut {
				your_index, hands, ..
			} => Some(&hands[*your_index]),
			Self::Concluded { .. } => None,
		}
	}
}

fn blind_hands<T: AsRef<CardSet>>(hands: &PerPlayer<T>) -> PerPlayer<u8> {
	hands.map_ref(|x| {
		x.as_ref()
			.len()
			.try_into()
			.expect("hand size should fit in u8")
	})
}

#[derive(Debug, Serialize, Deserialize)]
pub enum ServerGameState {
	ShuffleCommit {
		player_commitments: PerPlayer<Option<Commitment>>,
		dealer_commitment: Commitment,
		dealer_seed: Seed,
	},
	ShuffleSeed {
		player_commitments: PerPlayer<Commitment>,
		player_seeds: PerPlayer<Option<Seed>>,
		dealer_commitment: Commitment,
		dealer_seed: Seed,
	},
	Bidding {
		hands: PerPlayer<CardSet>,
		player_seeds: PerPlayer<Seed>,
		skat: [Card; 2],
		dealer_seed: Seed,
		dealer_commitment: Commitment,
		bidding: BiddingState,
	},
	Declaration {
		hands: PerPlayer<CardSet>,
		skat: Option<[Card; 2]>,
		declarer: Player,
		called_game_value: GameValue,
		dealer_seed: Seed,
	},
	Playing {
		play: PlayingState,
		declarer_initial_hand: CardSet,
		dealer_seed: Seed,
		called_game_value: GameValue,
		base_modifiers: Modifiers,
	},
	PassedOut {
		hands: PerPlayer<CardSet>,
		skat: [Card; 2],
		dealer_seed: Seed,
	},
	Concluded {
		won_cards: PerPlayer<CardStack>,
		dealer_seed: Seed,
		called_game_value: GameValue,
		summary: GameSummary,
	},
}

fn verify_commitment(commitment: &Commitment, seed: &Seed) -> bool {
	let check = Commitment::from_seed(seed);
	ring::constant_time::verify_slices_are_equal(&check.0, &commitment.0).is_ok()
}

fn deal(deck: CardStack) -> (PerPlayer<CardSet>, [Card; 2]) {
	assert_eq!(deck.len(), 32);
	let mut hands = PerPlayer::init_fn(CardSet::new);

	let mut deck = deck.into_iter();

	for hand in hands.iter_mut() {
		for _ in 0..3 {
			hand.insert(deck.next().expect("premature end of deck"));
		}
	}

	let skat = [
		deck.next().expect("premature end of deck"),
		deck.next().expect("premature end of deck"),
	];

	for hand in hands.iter_mut() {
		for _ in 0..4 {
			hand.insert(deck.next().expect("premature end of deck"));
		}
	}

	for hand in hands.iter_mut() {
		for _ in 0..3 {
			hand.insert(deck.next().expect("premature end of deck"));
		}
	}

	(hands, skat)
}

impl ServerGameState {
	pub fn new_game() -> Self {
		let dealer_seed = Seed::new();
		let dealer_commitment = Commitment::from_seed(&dealer_seed);

		Self::ShuffleCommit {
			player_commitments: PerPlayer::default(),
			dealer_seed,
			dealer_commitment,
		}
	}

	fn try_complete_seed_phase(&mut self) -> Result<()> {
		let (player_seeds, dealer_commitment, dealer_seed): (PerPlayer<Seed>, _, _) = match self {
			Self::ShuffleSeed {
				player_seeds,
				dealer_commitment,
				dealer_seed,
				..
			} => {
				if !player_seeds.iter().all(|x| x.is_some()) {
					return Err(GameError::IncompleteState);
				}
				let player_seeds = player_seeds.iter().map(|x| *x.as_ref().unwrap()).collect();
				(player_seeds, dealer_commitment, dealer_seed)
			}
			_ => return Err(GameError::WrongPhase),
		};
		let mut shuffler = Shuffler::new();
		for seed in player_seeds.iter() {
			shuffler.feed(seed.as_ref()).expect("shuffle state error");
		}
		shuffler
			.feed(dealer_seed.as_ref())
			.expect("shuffle state error");

		let mut deck = crate::cards::full_deck();
		shuffler.shuffle(&mut deck);

		let (hands, skat) = deal(deck);
		*self = Self::Bidding {
			hands,
			skat,
			bidding: BiddingState::new(),
			// needed for state broadcast -- players may not have seen the last shuffling state maybe
			player_seeds,
			dealer_commitment: *dealer_commitment,
			// needed for later proof
			dealer_seed: *dealer_seed,
		};

		Ok(())
	}

	fn try_complete_commit_phase(&mut self) -> Result<()> {
		let (player_commitments, dealer_commitment, dealer_seed) = match self {
			Self::ShuffleCommit {
				player_commitments,
				dealer_commitment,
				dealer_seed,
			} => {
				if !player_commitments.iter().all(|x| x.is_some()) {
					return Err(GameError::IncompleteState);
				}
				let player_commitments = player_commitments
					.iter()
					.map(|x| *x.as_ref().unwrap())
					.collect();
				(player_commitments, dealer_commitment, dealer_seed)
			}
			_ => return Err(GameError::WrongPhase),
		};
		let dealer_commitment = *dealer_commitment;
		let dealer_seed = *dealer_seed;
		*self = Self::ShuffleSeed {
			player_commitments,
			player_seeds: PerPlayer::default(),
			dealer_commitment,
			dealer_seed,
		};
		Ok(())
	}

	fn conclude_bidding(
		&mut self,
		hands: PerPlayer<CardSet>,
		skat: [Card; 2],
		dealer_seed: Seed,
		result: BiddingResult,
	) {
		*self = match result {
			BiddingResult::Won(declarer, called_game_value) => Self::Declaration {
				hands,
				skat: Some(skat),
				declarer,
				called_game_value,
				dealer_seed,
			},
			BiddingResult::PassedOut => {
				// passed out game; reveal all cards, no score.
				Self::PassedOut {
					hands,
					skat,
					dealer_seed,
				}
			}
		};
	}

	pub fn commit_seed(&mut self, player: Player, commitment: Commitment) -> Result<()> {
		match self {
			Self::ShuffleCommit {
				player_commitments, ..
			} => {
				let player_commitment = &mut player_commitments[player];
				if player_commitment.is_some() {
					return Err(GameError::WrongPhase);
				}
				*player_commitment = Some(commitment);
				match self.try_complete_commit_phase() {
					Ok(()) => Ok(()),
					Err(GameError::IncompleteState) => Ok(()),
					Err(other) => Err(other),
				}
			}
			_ => Err(GameError::WrongPhase),
		}
	}

	pub fn set_seed(&mut self, player: Player, seed: Seed) -> Result<()> {
		match self {
			Self::ShuffleSeed {
				player_seeds,
				player_commitments,
				..
			} => {
				let player_seed = &mut player_seeds[player];
				if player_seed.is_some() {
					return Err(GameError::WrongPhase);
				}
				let player_commitment = &player_commitments[player];
				if !verify_commitment(player_commitment, &seed) {
					return Err(GameError::InvalidValue);
				}
				*player_seed = Some(seed);
				match self.try_complete_seed_phase() {
					Ok(()) => Ok(()),
					Err(GameError::IncompleteState) => Ok(()),
					Err(other) => Err(other),
				}
			}
			_ => Err(GameError::WrongPhase),
		}
	}

	pub fn call_bid(&mut self, player: Player, bid: Option<GameValue>) -> Result<()> {
		match self {
			Self::Bidding {
				bidding,
				hands,
				skat,
				dealer_seed,
				..
			} => {
				bidding.bid(player, bid)?;
				if let Some(result) = bidding.finished() {
					let hands = *hands;
					let skat = *skat;
					let dealer_seed = *dealer_seed;
					self.conclude_bidding(hands, skat, dealer_seed, result);
				}
				Ok(())
			}
			_ => Err(GameError::WrongPhase),
		}
	}

	pub fn respond_bid(&mut self, player: Player, hold: bool) -> Result<()> {
		match self {
			Self::Bidding {
				bidding,
				hands,
				skat,
				dealer_seed,
				..
			} => {
				bidding.respond(player, hold)?;
				if let Some(result) = bidding.finished() {
					let hands = *hands;
					let skat = *skat;
					let dealer_seed = *dealer_seed;
					self.conclude_bidding(hands, skat, dealer_seed, result);
				}
				Ok(())
			}
			_ => Err(GameError::WrongPhase),
		}
	}

	pub fn take_skat(&mut self, player: Player) -> Result<()> {
		match self {
			Self::Declaration {
				skat,
				hands,
				declarer,
				..
			} => {
				if *declarer != player {
					return Err(GameError::NotYourTurn);
				}
				if let Some(skat) = skat.take() {
					hands[*declarer].extend(skat.into_iter());
					Ok(())
				} else {
					Err(GameError::NoSuchCard)
				}
			}
			_ => Err(GameError::WrongPhase),
		}
	}

	pub fn declare(
		&mut self,
		player: Player,
		game_type: GameType,
		push_cards: Option<[Card; 2]>,
		modifier: Option<AnnouncableModifier>,
	) -> Result<()> {
		match self {
			Self::Declaration {
				skat,
				hands,
				declarer,
				called_game_value,
				dealer_seed,
			} => {
				if player != *declarer {
					return Err(GameError::NotYourTurn);
				}

				let base_modifiers = match skat {
					None => match modifier {
						None => Modifiers::new(),
						Some(AnnouncableModifier::Ouvert) if game_type == GameType::Null => {
							Modifier::Ouvert.into()
						}
						_ => return Err(GameError::InvalidValue),
					},
					Some(_) => {
						Modifier::Hand
							| match modifier {
								None => Modifiers::new(),
								Some(AnnouncableModifier::Schneider)
									if game_type != GameType::Null =>
								{
									Modifier::SchneiderAnnounced.into()
								}
								Some(AnnouncableModifier::Schwarz)
									if game_type != GameType::Null =>
								{
									Modifier::SchneiderAnnounced | Modifier::SchwarzAnnounced
								}
								Some(AnnouncableModifier::Ouvert)
									if game_type != GameType::Null =>
								{
									Modifier::SchneiderAnnounced
										| Modifier::SchwarzAnnounced | Modifier::Ouvert
								}
								Some(AnnouncableModifier::Ouvert) => Modifier::Ouvert.into(),
								// trips if null and schneider/schwarz
								_ => return Err(GameError::InvalidValue),
							}
					}
				};

				let pushed_cards = match (push_cards.as_ref(), skat) {
					(Some(push_cards), None) => {
						let declarer_hand = &mut hands[*declarer];
						if !push_cards.iter().all(|x| declarer_hand.contains(x)) {
							return Err(GameError::NoSuchCard);
						}
						// only remove the cards once validation has passed
						for card in push_cards.iter() {
							declarer_hand.remove(card);
						}
						push_cards
					}
					(None, Some(skat)) => skat,
					(None, None) => {
						// attempt to steal cards :-O
						return Err(GameError::IncompleteState);
					}
					(Some(_), Some(_)) => {
						// attempt to … whatever this is!
						return Err(GameError::IncompleteState);
					}
				};

				let mut declarer_initial_hand = hands[*declarer];
				// ensure that the skat is within the declared hand
				declarer_initial_hand.extend(pushed_cards.iter().copied());

				*self = Self::Playing {
					play: PlayingState::new(*declarer, game_type, *hands, *pushed_cards),
					declarer_initial_hand,
					dealer_seed: *dealer_seed,
					called_game_value: *called_game_value,
					base_modifiers,
				};
				Ok(())
			}
			_ => Err(GameError::WrongPhase),
		}
	}

	pub fn play(&mut self, player: Player, card: Card) -> Result<()> {
		match self {
			Self::Playing {
				play,
				dealer_seed,
				base_modifiers,
				called_game_value,
				declarer_initial_hand,
				..
			} => {
				play.play(player, card)?;
				if play.hands().iter().all(|x| x.len() == 0) {
					// end of game!
					let declarer_won_cards = play.won_cards(play.declarer()).iter().collect();
					let matadors =
						calculate_matadors(*declarer_initial_hand, play.game_type().primary_suit());
					let (final_game_value, final_modifiers, loss_reasons) =
						crate::model::evaluate_game(
							play.game_type(),
							*base_modifiers,
							matadors,
							declarer_won_cards,
							*called_game_value,
						);
					*self = Self::Concluded {
						won_cards: play.all_won_cards().map(|x| *x),
						dealer_seed: *dealer_seed,
						called_game_value: *called_game_value,
						summary: GameSummary {
							declarer: play.declarer(),
							declarer_won_cards: declarer_won_cards.len() as u8,
							declarer_won_points: declarer_won_cards
								.iter()
								.map(|x| x.value())
								.sum::<u8>(),
							game_type: play.game_type(),
							loss_reasons,
							modifiers: final_modifiers,
							game_value: final_game_value,
							matadors,
						},
					}
				}
				Ok(())
			}
			_ => Err(GameError::WrongPhase),
		}
	}

	pub fn state(&self, player: Player) -> State {
		match self {
			Self::ShuffleCommit {
				player_commitments,
				dealer_commitment,
				..
			} => State::Committing {
				your_index: player,
				player_commitments: *player_commitments,
				dealer_commitment: *dealer_commitment,
			},
			Self::ShuffleSeed {
				player_seeds,
				player_commitments,
				dealer_commitment,
				..
			} => State::Shuffling {
				your_index: player,
				player_commitments: *player_commitments,
				dealer_commitment: *dealer_commitment,
				player_seeds: *player_seeds,
			},
			Self::Bidding {
				hands,
				dealer_commitment,
				player_seeds,
				bidding,
				..
			} => State::Bidding {
				your_index: player,
				player_seeds: *player_seeds,
				dealer_commitment: *dealer_commitment,
				player_bids: bidding.player_bids(),
				caller: bidding
					.caller()
					.expect("bidding must not be done in this state"),
				responder: bidding.responder(),
				hands: blind_hands(hands),
				your_hand: hands[player],
				awaiting_response: bidding.awaiting_response(),
			},
			Self::Declaration {
				hands,
				skat,
				declarer,
				called_game_value,
				..
			} => State::PreDeclaration {
				your_index: player,
				hands: blind_hands(hands),
				your_hand: hands[player],
				called_game_value: *called_game_value,
				declarer: *declarer,
				skat_taken: skat.is_none(),
			},
			Self::Playing {
				play,
				base_modifiers,
				..
			} => State::Playing {
				your_index: player,
				declarer: play.declarer(),
				game_type: play.game_type(),
				last_trick: play.last_trick().copied(),
				your_hand: *play.hand(player),
				turn: play.current_player(),
				hands: blind_hands(&play.hands()),
				declarer_hand: if base_modifiers.contains(Modifier::Ouvert) {
					Some(*play.hand(play.declarer()))
				} else {
					None
				},
				base_modifiers: *base_modifiers,
				table: *play.table(),
				forehand: play.forehand(),
			},
			Self::PassedOut {
				hands,
				skat,
				dealer_seed,
			} => State::PassedOut {
				your_index: player,
				skat: (*skat).into_iter().collect(),
				hands: *hands,
				dealer_seed: *dealer_seed,
			},
			Self::Concluded {
				won_cards,
				dealer_seed,
				called_game_value,
				summary,
			} => State::Concluded {
				your_index: player,
				dealer_seed: *dealer_seed,
				won_cards: *won_cards,
				summary: *summary,
				called_game_value: *called_game_value,
			},
		}
	}

	pub fn summarize(&self) -> Option<GameSummary> {
		match self {
			Self::Concluded { summary, .. } => Some(*summary),
			_ => None,
		}
	}

	pub fn passed_out(&self) -> bool {
		match self {
			Self::PassedOut { .. } => true,
			_ => false,
		}
	}
}
