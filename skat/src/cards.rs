use std::borrow::Borrow;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::marker::PhantomData;
use std::num::NonZeroU8;
use std::ops::{BitOr, BitOrAssign, Bound, Deref, DerefMut, Not, RangeBounds, Sub};

use rand::distributions::Distribution;

use num_enum::{IntoPrimitive, TryFromPrimitive};

use serde::{
	de::{Deserializer, Error, SeqAccess, Visitor},
	ser::{SerializeSeq, Serializer},
	Deserialize, Serialize,
};

#[derive(
	Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, IntoPrimitive, TryFromPrimitive,
)]
#[repr(u8)]
pub enum Suit {
	#[serde(rename = "d")]
	Diamond = 0,
	#[serde(rename = "h")]
	Heart = 1,
	#[serde(rename = "s")]
	Spade = 2,
	#[serde(rename = "c")]
	Club = 3,
}

impl BitOr<Rank> for Suit {
	type Output = Card;

	fn bitor(self, other: Rank) -> Card {
		Card::from_parts(other, self)
	}
}

pub const SUITS: [Suit; 4] = [Suit::Diamond, Suit::Heart, Suit::Spade, Suit::Club];

#[derive(
	Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize, IntoPrimitive, TryFromPrimitive,
)]
#[repr(u8)]
pub enum Rank {
	#[serde(rename = "7")]
	Seven = 0,
	#[serde(rename = "8")]
	Eight = 1,
	#[serde(rename = "9")]
	Nine = 2,
	#[serde(rename = "0")]
	Ten = 3,
	#[serde(rename = "J")]
	Jack = 4,
	#[serde(rename = "Q")]
	Queen = 5,
	#[serde(rename = "K")]
	King = 6,
	#[serde(rename = "A")]
	Ace = 7,
}

impl BitOr<Suit> for Rank {
	type Output = Card;

	fn bitor(self, other: Suit) -> Card {
		Card::from_parts(self, other)
	}
}

impl Rank {
	pub fn value(self) -> u8 {
		match self {
			Self::Seven | Self::Eight | Self::Nine => 0,
			Self::Ten => 10,
			Self::Jack => 2,
			Self::Queen => 3,
			Self::King => 4,
			Self::Ace => 11,
		}
	}
}

pub const RANKS: [Rank; 8] = [
	Rank::Seven,
	Rank::Eight,
	Rank::Nine,
	Rank::Ten,
	Rank::Jack,
	Rank::Queen,
	Rank::King,
	Rank::Ace,
];

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
#[repr(transparent)]
pub struct Card(NonZeroU8);

impl fmt::Debug for Card {
	fn fmt<'f>(&self, fmt: &'f mut fmt::Formatter) -> fmt::Result {
		let (rank, suit) = self.split();
		fmt.debug_tuple("Card").field(&rank).field(&suit).finish()
	}
}

impl Card {
	pub const fn from_parts(rank: Rank, suit: Suit) -> Self {
		let bits = ((suit as u8) << 3) | (rank as u8);
		match Self::from_bits(bits) {
			Some(v) => v,
			None => unreachable!(),
		}
	}

	#[inline(always)]
	pub fn split(self) -> (Rank, Suit) {
		let v = self.bits();
		let rank = v & 0b0_0111;
		let suit = (v & 0b1_1000) >> 3;
		(
			Rank::try_from_primitive(rank).unwrap(),
			Suit::try_from_primitive(suit).unwrap(),
		)
	}

	pub fn bits(&self) -> u8 {
		let result = self.0.get() - 1;
		debug_assert!(result <= 31);
		result
	}

	pub const fn from_bits(v: u8) -> Option<Self> {
		if v >= 32 {
			return None;
		}
		match NonZeroU8::new(v + 1) {
			Some(v) => Some(Self(v)),
			None => None,
		}
	}

	fn bitmask(&self) -> u32 {
		1u32 << (self.bits() as u32)
	}

	#[inline(always)]
	pub fn rank(&self) -> Rank {
		self.split().0
	}

	#[inline(always)]
	pub fn suit(&self) -> Suit {
		self.split().1
	}

	#[inline(always)]
	pub fn value(self) -> u8 {
		self.rank().value()
	}
}

impl Serialize for Card {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		self.bits().serialize(serializer)
	}
}

impl<'de> Deserialize<'de> for Card {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		let bits = u8::deserialize(deserializer)?;
		Ok(Self::from_bits(bits).ok_or(D::Error::custom("invalid bits for Card"))?)
	}
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
#[repr(transparent)]
pub struct CardSet(u32);

impl fmt::Debug for CardSet {
	fn fmt<'f>(&self, f: &'f mut fmt::Formatter) -> fmt::Result {
		f.debug_set().entries(self.iter()).finish()
	}
}

impl CardSet {
	pub fn new() -> Self {
		Self(0)
	}

	pub fn with_capacity(_capacity: usize) -> Self {
		Self::new()
	}

	pub fn insert(&mut self, card: Card) -> bool {
		let mask = card.bitmask();
		if self.0 & mask != 0 {
			return false;
		}
		self.0 |= mask;
		true
	}

	pub fn remove<K: Borrow<Card>>(&mut self, card: K) -> bool {
		let mask = card.borrow().bitmask();
		let was_present = self.0 & mask != 0;
		self.0 = self.0 & !mask;
		was_present
	}

	pub fn len(&self) -> usize {
		self.0.count_ones() as usize
	}

	pub fn iter(&self) -> CardSetIter<'_> {
		CardSetIter {
			inner: (*self).into_iter(),
			_lifetime: PhantomData,
		}
	}

	pub fn contains<K: Borrow<Card>>(&self, card: K) -> bool {
		self.0 & card.borrow().bitmask() != 0
	}

	pub fn full() -> Self {
		Self(0xffff_ffff)
	}

	pub fn take_sampled<R: rand::Rng + ?Sized>(&mut self, rng: &mut R) -> Card {
		let card = self.sample(rng);
		self.remove(&card);
		card
	}
}

impl Distribution<Card> for CardSet {
	fn sample<R: rand::Rng + ?Sized>(&self, rng: &mut R) -> Card {
		let len = self.len();
		let i = rand::distributions::Uniform::from(0..len).sample(rng);
		self.into_iter()
			.nth(i)
			.expect("cannot sample from empty set")
	}
}

impl Serialize for CardSet {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		let mut seq = serializer.serialize_seq(Some(self.len()))?;
		for card in self.iter() {
			seq.serialize_element(&card)?;
		}
		seq.end()
	}
}

struct CardSetVisitor {}

impl<'de> Visitor<'de> for CardSetVisitor {
	type Value = CardSet;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("a set of Cards")
	}

	fn visit_seq<A: SeqAccess<'de>>(self, mut seq: A) -> Result<Self::Value, A::Error> {
		let mut result = CardSet::new();
		while let Some(item) = seq.next_element()? {
			result.insert(item);
		}
		Ok(result)
	}
}

impl<'de> Deserialize<'de> for CardSet {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		deserializer.deserialize_seq(CardSetVisitor {})
	}
}

impl AsRef<CardSet> for CardSet {
	fn as_ref(&self) -> &CardSet {
		self
	}
}

impl FromIterator<Card> for CardSet {
	fn from_iter<T: IntoIterator<Item = Card>>(iter: T) -> Self {
		let mut result = Self::new();
		result.extend(iter);
		result
	}
}

impl Extend<Card> for CardSet {
	fn extend<T: IntoIterator<Item = Card>>(&mut self, iter: T) {
		for card in iter {
			self.0 |= card.bitmask();
		}
	}
}

impl BitOr for CardSet {
	type Output = CardSet;

	fn bitor(self, rhs: Self) -> Self {
		Self(self.0 | rhs.0)
	}
}

impl BitOrAssign for CardSet {
	fn bitor_assign(&mut self, rhs: Self) {
		self.0 |= rhs.0;
	}
}

impl Sub for CardSet {
	type Output = CardSet;

	fn sub(self, rhs: Self) -> Self {
		Self(self.0 & (!rhs.0))
	}
}

impl Not for CardSet {
	type Output = CardSet;

	fn not(self) -> Self {
		Self(!self.0)
	}
}

impl IntoIterator for CardSet {
	type Item = Card;
	type IntoIter = CardSetIntoIter;

	fn into_iter(self) -> Self::IntoIter {
		CardSetIntoIter { set: self, card: 0 }
	}
}

impl<'x> IntoIterator for &'x CardSet {
	type Item = Card;
	type IntoIter = CardSetIter<'x>;

	fn into_iter(self) -> Self::IntoIter {
		self.iter()
	}
}

pub struct CardSetIter<'x> {
	inner: CardSetIntoIter,
	_lifetime: PhantomData<&'x u32>,
}

impl<'x> Iterator for CardSetIter<'x> {
	type Item = Card;

	fn next(&mut self) -> Option<Self::Item> {
		self.inner.next()
	}
}

pub struct CardSetIntoIter {
	set: CardSet,
	card: u8,
}

impl Iterator for CardSetIntoIter {
	type Item = Card;

	fn next(&mut self) -> Option<Self::Item> {
		loop {
			let card_id = self.card;
			let card = Card::from_bits(card_id)?;
			self.card = card_id.wrapping_add(1);
			if self.set.contains(&card) {
				return Some(card);
			}
		}
	}
}

#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct CardStack([Card; 32]);

impl fmt::Debug for CardStack {
	fn fmt<'f>(&self, f: &'f mut fmt::Formatter) -> fmt::Result {
		f.debug_list().entries(self.iter()).finish()
	}
}

impl PartialEq for CardStack {
	fn eq(&self, other: &Self) -> bool {
		(self.deref() as &[Card]) == (other.deref() as &[Card])
	}
}

impl Eq for CardStack {}

impl Hash for CardStack {
	fn hash<H: Hasher>(&self, state: &mut H) {
		(self.deref() as &[Card]).hash(state)
	}
}

impl CardStack {
	pub fn new() -> Self {
		let mut inner = [Card(NonZeroU8::new(0xff).unwrap()); 32];
		// 33 = length 0
		inner[31] = Card(NonZeroU8::new(33).unwrap());
		Self(inner)
	}

	pub fn with_capacity(_capacity: usize) -> Self {
		Self::new()
	}

	fn write_len(&mut self, new_len: usize) {
		assert!(new_len <= 32);
		let new_len = new_len as u8;
		if new_len < 32 {
			self.0[31] = Card(NonZeroU8::new(33 + new_len).unwrap());
		}
		debug_assert_eq!(self.len(), new_len as usize);
	}

	pub fn len(&self) -> usize {
		let last = self.0[31].0.get();
		let len = if last > 32 { (last - 33) as usize } else { 32 };
		len
	}

	pub fn push(&mut self, card: Card) {
		let curr = self.len();
		if curr >= 32 {
			panic!("cannot push more than 32 cards to CardStack");
		}
		self.0[curr] = card;
		let new_len = curr + 1;
		self.write_len(new_len);
	}

	pub fn get(&self, index: usize) -> Option<&Card> {
		let len = self.len();
		if index >= len {
			None
		} else {
			Some(&self.0[index])
		}
	}

	pub fn drain<R: RangeBounds<usize>>(&mut self, range: R) -> CardStackDrain<'_> {
		let len = self.len();
		let start = match range.start_bound() {
			Bound::Included(v) => *v,
			Bound::Excluded(v) => (*v) + 1,
			Bound::Unbounded => 0,
		};
		let end_excl = match range.end_bound() {
			Bound::Included(v) => *v,
			Bound::Excluded(v) => v.saturating_sub(1),
			Bound::Unbounded => len,
		};
		if end_excl > len {
			panic!("range end {} out of bounds ({})", end_excl, len);
		}
		let count = end_excl - start;
		let subpart: CardStack = self.0[start..end_excl].iter().copied().collect();
		self.0.copy_within(end_excl.., start);
		let new_len = len - count;
		self.write_len(new_len);
		CardStackDrain {
			inner: subpart.into_iter(),
			_lifetime: PhantomData,
		}
	}

	pub fn iter(&self) -> CardStackIter<'_> {
		CardStackIter {
			source: self,
			index: 0,
		}
	}

	pub fn contains(&self, card: &Card) -> bool {
		let card = *card;
		self.iter().any(|x| x == card)
	}
}

impl Deref for CardStack {
	type Target = [Card];

	fn deref(&self) -> &Self::Target {
		&self.0[..self.len()]
	}
}

impl DerefMut for CardStack {
	fn deref_mut(&mut self) -> &mut Self::Target {
		let len = self.len();
		&mut self.0[..len]
	}
}

impl Extend<Card> for CardStack {
	fn extend<T: IntoIterator<Item = Card>>(&mut self, iter: T) {
		for item in iter {
			self.push(item)
		}
	}
}

impl FromIterator<Card> for CardStack {
	fn from_iter<T: IntoIterator<Item = Card>>(iter: T) -> Self {
		let mut result = Self::new();
		for item in iter {
			result.push(item)
		}
		result
	}
}

impl Serialize for CardStack {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		let mut seq = serializer.serialize_seq(Some(self.len()))?;
		for card in self.iter() {
			seq.serialize_element(&card)?;
		}
		seq.end()
	}
}

impl IntoIterator for CardStack {
	type Item = Card;
	type IntoIter = CardStackIntoIter;

	fn into_iter(self) -> Self::IntoIter {
		CardStackIntoIter {
			source: self,
			index: 0,
		}
	}
}

impl<'x> IntoIterator for &'x CardStack {
	type Item = Card;
	type IntoIter = CardStackIter<'x>;

	fn into_iter(self) -> Self::IntoIter {
		self.iter()
	}
}

struct CardStackVisitor {}

impl<'de> Visitor<'de> for CardStackVisitor {
	type Value = CardStack;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("a sequence of Cards")
	}

	fn visit_seq<A: SeqAccess<'de>>(self, mut seq: A) -> Result<Self::Value, A::Error> {
		let mut result = CardStack::new();
		while let Some(item) = seq.next_element()? {
			result.push(item);
		}
		Ok(result)
	}
}

impl<'de> Deserialize<'de> for CardStack {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		deserializer.deserialize_seq(CardStackVisitor {})
	}
}

pub struct CardStackIntoIter {
	source: CardStack,
	index: u8,
}

impl Iterator for CardStackIntoIter {
	type Item = Card;

	fn next(&mut self) -> Option<Self::Item> {
		match self.source.get(self.index as usize) {
			Some(v) => {
				self.index += 1;
				Some(*v)
			}
			None => None,
		}
	}
}

pub struct CardStackIter<'x> {
	source: &'x CardStack,
	index: u8,
}

impl<'x> Iterator for CardStackIter<'x> {
	type Item = Card;

	fn next(&mut self) -> Option<Self::Item> {
		match self.source.get(self.index as usize) {
			Some(v) => {
				self.index += 1;
				Some(*v)
			}
			None => None,
		}
	}
}

pub struct CardStackDrain<'x> {
	inner: CardStackIntoIter,
	_lifetime: PhantomData<&'x mut CardStack>,
}

impl<'x> Iterator for CardStackDrain<'x> {
	type Item = Card;

	fn next(&mut self) -> Option<Self::Item> {
		self.inner.next()
	}
}

pub fn full_deck() -> CardStack {
	let mut result = CardStack::new();
	for suit in SUITS.iter().copied() {
		for rank in RANKS.iter().copied() {
			result.push(rank | suit)
		}
	}
	debug_assert_eq!(result.len(), 32);
	result
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn basic_card_set() {
		let mut set = CardSet::new();
		assert_eq!(set.len(), 0);
		assert!(set.insert(Rank::Seven | Suit::Diamond));
		assert_eq!(set.len(), 1);
		assert!(!set.insert(Rank::Seven | Suit::Diamond));
		assert_eq!(set.len(), 1);
		assert!(set.insert(Rank::Ace | Suit::Club));
		assert_eq!(set.len(), 2);
		assert!(!set.insert(Rank::Ace | Suit::Club));
		assert!(set.contains(&(Rank::Seven | Suit::Diamond)));
		assert!(set.contains(&(Rank::Ace | Suit::Club)));
		assert!(!set.contains(&(Rank::Ace | Suit::Diamond)));
		assert!(!set.contains(&(Rank::Seven | Suit::Club)));

		assert!(set.remove(&(Rank::Seven | Suit::Diamond)));
		assert_eq!(set.len(), 1);
		assert!(!set.contains(&(Rank::Seven | Suit::Diamond)));
		assert!(!set.remove(&(Rank::Seven | Suit::Diamond)));
		assert_eq!(set.len(), 1);

		assert!(set.remove(&(Rank::Ace | Suit::Club)));
		assert_eq!(set.len(), 0);
		assert!(!set.contains(&(Rank::Ace | Suit::Club)));
		assert!(!set.remove(&(Rank::Ace | Suit::Club)));
		assert_eq!(set.len(), 0);
	}
}
