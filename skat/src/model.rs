use std::cmp::Ordering;
use std::fmt;
use std::ops::{Deref, DerefMut, Index, IndexMut};

use serde::{Deserialize, Deserializer, Serialize, Serializer};

use num_enum::TryFromPrimitive;

use serde_repr::{Deserialize_repr, Serialize_repr};

use crate::bidding::GameValue;
use crate::cards::{Card, CardSet, Rank, Suit};

#[derive(
	Debug,
	Clone,
	Copy,
	PartialEq,
	Eq,
	PartialOrd,
	Ord,
	Hash,
	Serialize_repr,
	Deserialize_repr,
	TryFromPrimitive,
)]
#[repr(u8)]
pub enum Player {
	First = 0,
	Second = 1,
	Third = 2,
}

impl Player {
	pub fn wrapping_inc(&mut self) {
		*self = match *self {
			Self::First => Self::Second,
			Self::Second => Self::Third,
			Self::Third => Self::First,
		}
	}
}

impl From<Player> for u8 {
	fn from(other: Player) -> u8 {
		other as u8
	}
}

impl From<Player> for usize {
	fn from(other: Player) -> usize {
		other as u8 as usize
	}
}

pub const PLAYERS: [Player; 3] = [Player::First, Player::Second, Player::Third];

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
#[repr(transparent)]
pub struct Trick([Card; 3]);

impl Deref for Trick {
	type Target = [Card];

	fn deref(&self) -> &Self::Target {
		&self.0[..]
	}
}

impl DerefMut for Trick {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0[..]
	}
}

impl From<[Card; 3]> for Trick {
	fn from(other: [Card; 3]) -> Self {
		Self(other)
	}
}

impl From<Trick> for [Card; 3] {
	fn from(other: Trick) -> Self {
		other.0
	}
}

impl Index<RelativePlayer> for Trick {
	type Output = Card;

	fn index(&self, index: RelativePlayer) -> &Self::Output {
		&self.0[index as u8 as usize]
	}
}

impl IndexMut<RelativePlayer> for Trick {
	fn index_mut(&mut self, index: RelativePlayer) -> &mut Self::Output {
		&mut self.0[index as u8 as usize]
	}
}

impl Trick {
	pub fn iter(&self) -> std::slice::Iter<'_, Card> {
		self.0.iter()
	}

	pub fn into_iter(self) -> std::array::IntoIter<Card, 3> {
		self.0.into_iter()
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
#[repr(u8)]
pub enum RelativePlayer {
	Forehand = 0,
	Midhand = 1,
	Rearhand = 2,
}

impl RelativePlayer {
	pub fn into_absolute(self, forehand: Player) -> Player {
		let index = ((self as u8) + (forehand as u8)) % 3;
		match index {
			0 => Player::First,
			1 => Player::Second,
			2 => Player::Third,
			_ => unreachable!(),
		}
	}

	pub fn into_absolute_reverse(self, new_forehand: Player) -> Player {
		let index = ((new_forehand as u8) + (3 - (self as u8))) % 3;
		match index {
			0 => Player::First,
			1 => Player::Second,
			2 => Player::Third,
			_ => unreachable!(),
		}
	}

	pub fn from_absolute(forehand: Player, v: Player) -> Self {
		let index = ((v as u8) + (3 - (forehand as u8))) % 3;
		match index {
			0 => Self::Forehand,
			1 => Self::Midhand,
			2 => Self::Rearhand,
			_ => unreachable!(),
		}
	}
}

pub const RELATIVE_PLAYERS: [RelativePlayer; 3] = [
	RelativePlayer::Forehand,
	RelativePlayer::Midhand,
	RelativePlayer::Rearhand,
];

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum AnnouncableModifier {
	#[serde(rename = "schneider")]
	Schneider = 0,
	#[serde(rename = "schwarz")]
	Schwarz = 1,
	#[serde(rename = "ouvert")]
	Ouvert = 2,
}

#[derive(enumset::EnumSetType, Debug)]
#[repr(u8)]
pub enum Modifier {
	Hand = 0,
	Schneider = 1,
	SchneiderAnnounced = 2,
	Schwarz = 3,
	SchwarzAnnounced = 4,
	Ouvert = 5,
}

pub type Modifiers = enumset::EnumSet<Modifier>;

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum GameType {
	#[serde(rename = "diamonds")]
	Diamonds,
	#[serde(rename = "hearts")]
	Hearts,
	#[serde(rename = "spades")]
	Spades,
	#[serde(rename = "clubs")]
	Clubs,
	#[serde(rename = "grand")]
	Grand,
	#[serde(rename = "null")]
	Null,
}

impl fmt::Display for GameType {
	fn fmt<'f>(&self, f: &'f mut fmt::Formatter) -> fmt::Result {
		f.write_str(match self {
			Self::Diamonds => "diamonds",
			Self::Hearts => "hearts",
			Self::Spades => "spades",
			Self::Clubs => "clubs",
			Self::Grand => "grand",
			Self::Null => "null",
		})
	}
}

impl From<Suit> for GameType {
	fn from(other: Suit) -> Self {
		match other {
			Suit::Diamond => Self::Diamonds,
			Suit::Heart => Self::Hearts,
			Suit::Spade => Self::Spades,
			Suit::Club => Self::Clubs,
		}
	}
}

impl GameType {
	pub(crate) fn primary_suit(&self) -> Option<Suit> {
		match self {
			Self::Diamonds => Some(Suit::Diamond),
			Self::Hearts => Some(Suit::Heart),
			Self::Spades => Some(Suit::Spade),
			Self::Clubs => Some(Suit::Club),
			Self::Grand => None,
			Self::Null => None,
		}
	}

	pub fn effective_suit(&self, card: &Card) -> EffectiveSuit {
		if let Some(suit) = self.primary_suit() {
			// suit game
			if suit == card.suit() || card.rank() == Rank::Jack {
				return EffectiveSuit::Trump;
			} else {
				return EffectiveSuit::from_suit(card.suit());
			}
		}

		match self {
			Self::Diamonds | Self::Hearts | Self::Spades | Self::Clubs => unreachable!(),
			Self::Grand => {
				if card.rank() == Rank::Jack {
					EffectiveSuit::Trump
				} else {
					EffectiveSuit::from_suit(card.suit())
				}
			}
			Self::Null => EffectiveSuit::from_suit(card.suit()),
		}
	}

	fn value(&self, card: &Card) -> u8 {
		match self {
			Self::Null => match card.rank() {
				Rank::Seven => 0,
				Rank::Eight => 1,
				Rank::Nine => 2,
				Rank::Ten => 3,
				Rank::Jack => 4,
				Rank::Queen => 5,
				Rank::King => 6,
				Rank::Ace => 7,
			},
			Self::Diamonds | Self::Hearts | Self::Spades | Self::Clubs | Self::Grand => {
				match card.rank() {
					Rank::Seven => 0,
					Rank::Eight => 1,
					Rank::Nine => 2,
					Rank::Queen => 3,
					Rank::King => 4,
					Rank::Ten => 5,
					Rank::Ace => 6,
					// note: trump effective suit is handled in score separately, but we still need to distinguish the different suit value when they meet the others.
					Rank::Jack => match card.suit() {
						Suit::Diamond => 7,
						Suit::Heart => 8,
						Suit::Spade => 9,
						Suit::Club => 10,
					},
				}
			}
		}
	}

	pub fn power(&self, card: &Card) -> CardPower {
		let suit = self.effective_suit(card);
		let value = self.value(card);
		CardPower(suit == EffectiveSuit::Trump, value)
	}

	pub fn trick_taker(&self, trick: &Trick) -> RelativePlayer {
		let trick_suit = self.effective_suit(&trick[RelativePlayer::Forehand]);
		let mut highest_power = self.power(&trick[RelativePlayer::Forehand]);
		let mut highest_player = RelativePlayer::Forehand;
		for player in RELATIVE_PLAYERS.iter().copied().skip(1) {
			let card = &trick[player];
			let card_suit = self.effective_suit(card);
			let card_power = self.power(card);
			if card_suit != trick_suit && card_suit != EffectiveSuit::Trump {
				// not matching suit && not a trump -> cannot win the trick
				continue;
			}
			if card_power > highest_power {
				highest_player = player;
				highest_power = card_power;
			}
		}
		highest_player
	}

	pub fn base_value(&self) -> GameValue {
		match self {
			Self::Diamonds => 9,
			Self::Hearts => 10,
			Self::Spades => 11,
			Self::Clubs => 12,
			Self::Grand => 24,
			Self::Null => 23,
		}
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Matadors {
	With(u8),
	Without(u8),
}

impl Matadors {
	fn factor(self) -> GameValue {
		match self {
			Self::With(v) => v as GameValue,
			Self::Without(v) => v as GameValue,
		}
	}
}

impl Serialize for Matadors {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		MatadorSerialization::from(*self).serialize(serializer)
	}
}

impl<'de> Deserialize<'de> for Matadors {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		let ser = MatadorSerialization::deserialize(deserializer)?;
		Ok(ser.into())
	}
}

#[derive(Serialize, Deserialize)]
struct MatadorSerialization {
	is_with: bool,
	count: u8,
}

impl From<Matadors> for MatadorSerialization {
	fn from(other: Matadors) -> Self {
		match other {
			Matadors::With(count) => Self {
				is_with: true,
				count,
			},
			Matadors::Without(count) => Self {
				is_with: false,
				count,
			},
		}
	}
}

impl From<MatadorSerialization> for Matadors {
	fn from(other: MatadorSerialization) -> Self {
		match other.is_with {
			true => Self::With(other.count),
			false => Self::Without(other.count),
		}
	}
}

const JACKS: [Card; 4] = [
	Card::from_parts(Rank::Jack, Suit::Club),
	Card::from_parts(Rank::Jack, Suit::Spade),
	Card::from_parts(Rank::Jack, Suit::Heart),
	Card::from_parts(Rank::Jack, Suit::Diamond),
];

const MATATOR_RANKS: [Rank; 7] = [
	Rank::Ace,
	Rank::Ten,
	Rank::King,
	Rank::Queen,
	Rank::Nine,
	Rank::Eight,
	Rank::Seven,
];

struct MatadorSuitSubsequence {
	suit: Suit,
	ranks: std::slice::Iter<'static, Rank>,
}

impl Iterator for MatadorSuitSubsequence {
	type Item = Card;

	fn next(&mut self) -> Option<Self::Item> {
		Some(self.suit | *self.ranks.next()?)
	}
}

struct MatadorSequence {
	jacks: std::slice::Iter<'static, Card>,
	suit: Option<MatadorSuitSubsequence>,
}

impl MatadorSequence {
	fn new(suit: Option<Suit>) -> Self {
		Self {
			jacks: JACKS.iter(),
			suit: suit.map(|x| MatadorSuitSubsequence {
				suit: x,
				ranks: MATATOR_RANKS.iter(),
			}),
		}
	}
}

impl Iterator for MatadorSequence {
	type Item = Card;

	fn next(&mut self) -> Option<Self::Item> {
		if let Some(jack) = self.jacks.next() {
			return Some(*jack);
		}

		Some(self.suit.as_mut()?.next()?)
	}
}

pub fn derive_modifiers_from_won_cards(own_won_cards: CardSet) -> Modifiers {
	let ncards = own_won_cards.len();
	let value = own_won_cards.iter().map(|x| x.value()).sum::<u8>();
	if ncards <= 2 || ncards == 32 {
		// <= 2 instead of == 0 because of pushed cards!
		Modifier::Schwarz | Modifier::Schneider
	} else if value >= 90 || value <= 30 {
		Modifier::Schneider.into()
	} else {
		Modifiers::empty()
	}
}

pub fn calculate_matadors(hand: CardSet, suit: Option<Suit>) -> Matadors {
	let sequence = MatadorSequence::new(suit);
	if hand.contains(Rank::Jack | Suit::Club) {
		// with
		let mut n = 0u8;
		for card in sequence {
			if !hand.contains(card) {
				break;
			}
			n = n.checked_add(1).expect("not too many matadors");
		}
		Matadors::With(n)
	} else {
		// without
		let mut n = 0u8;
		for card in sequence {
			if hand.contains(card) {
				break;
			}
			n = n.checked_add(1).expect("not too many matadors");
		}
		Matadors::Without(n)
	}
}

pub fn calculate_win_factor(modifiers: Modifiers) -> GameValue {
	if modifiers.contains(Modifier::Hand) {
		// hand game: announcements may exist
		if modifiers.contains(Modifier::Ouvert) {
			7
		} else if modifiers.contains(Modifier::SchwarzAnnounced) {
			6
		} else if modifiers.is_superset(Modifier::SchneiderAnnounced | Modifier::Schwarz) {
			5
		} else if modifiers.contains(Modifier::Schwarz) {
			4
		} else if modifiers.contains(Modifier::SchneiderAnnounced) {
			4
		} else if modifiers.contains(Modifier::Schneider) {
			3
		} else {
			// hand is checked above
			2
		}
	} else {
		// not a hand game: announcements cannot exist and do not count
		if modifiers.contains(Modifier::Schwarz) {
			3
		} else if modifiers.contains(Modifier::Schneider) {
			2
		} else {
			1
		}
	}
}

pub fn calculate_game_value(
	game_type: GameType,
	modifiers: Modifiers,
	matadors: Matadors,
) -> GameValue {
	if game_type == GameType::Null {
		let is_hand = !(modifiers & Modifier::Hand).is_empty();
		let is_ouvert = !(modifiers & Modifier::Ouvert).is_empty();
		return match (is_hand, is_ouvert) {
			(false, false) => 23,
			(true, false) => 35,
			(false, true) => 46,
			(true, true) => 59,
		};
	}

	let win_factor = calculate_win_factor(modifiers);
	(matadors.factor() + win_factor) * game_type.base_value()
}

pub fn find_point_loss_reasons(own_cards: CardSet) -> LossReasons {
	let mut reasons = LossReasons::empty();
	if own_cards.iter().map(|x| x.value()).sum::<u8>() <= 60 {
		reasons |= LossReason::NotEnoughPoints;
	}
	reasons
}

pub fn find_modifier_loss_reasons(modifiers: Modifiers) -> LossReasons {
	let mut reasons = LossReasons::empty();
	if modifiers.contains(Modifier::SchwarzAnnounced) && !modifiers.contains(Modifier::Schwarz) {
		reasons |= LossReason::MissingSchwarz;
	}
	if modifiers.contains(Modifier::SchneiderAnnounced) && !modifiers.contains(Modifier::Schneider)
	{
		reasons |= LossReason::MissingSchneider;
	}
	reasons
}

pub fn evaluate_game(
	game_type: GameType,
	mut modifiers: Modifiers,
	matadors: Matadors,
	declarer_won_cards: CardSet,
	called_game_value: GameValue,
) -> (GameValue, Modifiers, LossReasons) {
	modifiers |= derive_modifiers_from_won_cards(declarer_won_cards);

	let final_game_value = calculate_game_value(game_type, modifiers, matadors);

	let mut loss_reasons = match game_type {
		GameType::Null => {
			if declarer_won_cards.len() > 2 {
				// > 2 instead of != 0 because of pushed cards!
				LossReason::NotNull.into()
			} else {
				LossReasons::empty()
			}
		}
		_ => find_point_loss_reasons(declarer_won_cards) | find_modifier_loss_reasons(modifiers),
	};
	if final_game_value < called_game_value {
		loss_reasons |= LossReason::Overbid;
	}
	(final_game_value, modifiers, loss_reasons)
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct CardPower(bool, u8);

impl PartialOrd for CardPower {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		(self.0 as u8, self.1).partial_cmp(&(other.0 as u8, other.1))
	}
}

impl Ord for CardPower {
	fn cmp(&self, other: &Self) -> Ordering {
		(self.0 as u8, self.1).cmp(&(other.0 as u8, other.1))
	}
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum EffectiveSuit {
	Diamond = 0,
	Heart = 1,
	Spade = 2,
	Club = 3,
	Trump = 4,
}

impl EffectiveSuit {
	fn from_suit(suit: Suit) -> Self {
		match suit {
			Suit::Diamond => Self::Diamond,
			Suit::Heart => Self::Heart,
			Suit::Spade => Self::Spade,
			Suit::Club => Self::Club,
		}
	}
}

pub const EFFECTIVE_SUITS: [EffectiveSuit; 5] = [
	EffectiveSuit::Diamond,
	EffectiveSuit::Heart,
	EffectiveSuit::Spade,
	EffectiveSuit::Club,
	EffectiveSuit::Trump,
];

#[derive(Clone, Copy)]
pub struct PerEffectiveSuit<T> {
	inner: [T; 5],
}

impl<T: fmt::Debug> fmt::Debug for PerEffectiveSuit<T> {
	fn fmt<'f>(&self, f: &'f mut fmt::Formatter) -> fmt::Result {
		let mut map = f.debug_map();
		for (suit, data) in self.enumerate_ref() {
			map.entry(&suit, data);
		}
		map.finish()
	}
}

type PerEffectiveSuitEnumerate<T> =
	std::iter::Zip<std::array::IntoIter<EffectiveSuit, 5>, std::array::IntoIter<T, 5>>;
type PerEffectiveSuitEnumerateRef<'x, T> =
	std::iter::Zip<std::array::IntoIter<EffectiveSuit, 5>, std::slice::Iter<'x, T>>;
type PerEffectiveSuitEnumerateMut<'x, T> =
	std::iter::Zip<std::array::IntoIter<EffectiveSuit, 5>, std::slice::IterMut<'x, T>>;

impl<T> PerEffectiveSuit<T> {
	pub fn iter(&self) -> std::slice::Iter<T> {
		self.inner.iter()
	}

	pub fn iter_mut(&mut self) -> std::slice::IterMut<T> {
		self.inner.iter_mut()
	}

	pub fn enumerate_ref(&self) -> PerEffectiveSuitEnumerateRef<T> {
		EFFECTIVE_SUITS.into_iter().zip(self.inner.iter())
	}

	pub fn enumerate_mut(&mut self) -> PerEffectiveSuitEnumerateMut<T> {
		EFFECTIVE_SUITS.into_iter().zip(self.inner.iter_mut())
	}

	pub fn enumerate(self) -> PerEffectiveSuitEnumerate<T> {
		EFFECTIVE_SUITS.into_iter().zip(self.inner.into_iter())
	}

	pub fn reduce<F: FnMut(T, T) -> T>(self, f: F) -> T {
		self.into_iter().reduce(f).unwrap()
	}

	pub fn init_fn<F: FnMut() -> T>(mut f: F) -> Self {
		Self {
			inner: [f(), f(), f(), f(), f()],
		}
	}

	pub fn from_fn<F: FnMut(EffectiveSuit) -> T>(mut f: F) -> Self {
		Self {
			inner: [
				f(EffectiveSuit::Diamond),
				f(EffectiveSuit::Heart),
				f(EffectiveSuit::Spade),
				f(EffectiveSuit::Club),
				f(EffectiveSuit::Trump),
			],
		}
	}

	pub fn map<U, F: FnMut(T) -> U>(self, f: F) -> PerEffectiveSuit<U> {
		let mut iter = self.inner.into_iter().map(f);
		PerEffectiveSuit {
			inner: [
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
			],
		}
	}

	pub fn map_ref<'x, U: 'x, F: FnMut(&'x T) -> U>(&'x self, f: F) -> PerEffectiveSuit<U> {
		let mut iter = self.inner.iter().map(f);
		PerEffectiveSuit {
			inner: [
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
				iter.next().unwrap(),
			],
		}
	}
}

impl<T: Default> Default for PerEffectiveSuit<T> {
	fn default() -> Self {
		Self {
			inner: <[T; 5] as Default>::default(),
		}
	}
}

impl<A> FromIterator<A> for PerEffectiveSuit<A> {
	fn from_iter<T: IntoIterator<Item = A>>(iter: T) -> Self {
		let mut iter = iter.into_iter();
		let inner = [
			iter.next().expect("iterator too short"),
			iter.next().expect("iterator too short"),
			iter.next().expect("iterator too short"),
			iter.next().expect("iterator too short"),
			iter.next().expect("iterator too short"),
		];
		if iter.next().is_some() {
			panic!("iterator too long");
		}
		Self { inner }
	}
}

impl<T> IntoIterator for PerEffectiveSuit<T> {
	type IntoIter = std::array::IntoIter<T, 5>;
	type Item = T;

	fn into_iter(self) -> Self::IntoIter {
		self.inner.into_iter()
	}
}

impl<'x, T> IntoIterator for &'x PerEffectiveSuit<T> {
	type IntoIter = std::slice::Iter<'x, T>;
	type Item = &'x T;

	fn into_iter(self) -> Self::IntoIter {
		self.inner.iter()
	}
}

impl<'x, T> IntoIterator for &'x mut PerEffectiveSuit<T> {
	type IntoIter = std::slice::IterMut<'x, T>;
	type Item = &'x mut T;

	fn into_iter(self) -> Self::IntoIter {
		self.inner.iter_mut()
	}
}

impl<T> From<[T; 5]> for PerEffectiveSuit<T> {
	fn from(other: [T; 5]) -> Self {
		Self { inner: other }
	}
}

impl<T> Index<usize> for PerEffectiveSuit<T> {
	type Output = T;

	fn index(&self, index: usize) -> &Self::Output {
		&self.inner[index]
	}
}

impl<T> IndexMut<usize> for PerEffectiveSuit<T> {
	fn index_mut(&mut self, index: usize) -> &mut Self::Output {
		&mut self.inner[index]
	}
}

impl<T> Index<EffectiveSuit> for PerEffectiveSuit<T> {
	type Output = T;

	fn index(&self, index: EffectiveSuit) -> &Self::Output {
		&self.inner[index as u8 as usize]
	}
}

impl<T> IndexMut<EffectiveSuit> for PerEffectiveSuit<T> {
	fn index_mut(&mut self, index: EffectiveSuit) -> &mut Self::Output {
		&mut self.inner[index as u8 as usize]
	}
}

#[derive(enumset::EnumSetType, Debug)]
pub enum LossReason {
	NotEnoughPoints = 0,
	MissingSchneider = 1,
	MissingSchwarz = 2,
	Overbid = 3,
	NotNull = 4,
}

pub type LossReasons = enumset::EnumSet<LossReason>;

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct GameSummary {
	pub declarer: Player,
	pub declarer_won_points: u8,
	pub declarer_won_cards: u8,
	pub loss_reasons: LossReasons,
	pub matadors: Matadors,
	pub game_type: GameType,
	pub game_value: GameValue,
	pub modifiers: Modifiers,
}

#[cfg(test)]
mod tests {
	use super::*;

	use crate::cards::SUITS;

	#[test]
	fn derive_modifiers_from_won_cards_opponent_schwarz() {
		let modifiers = derive_modifiers_from_won_cards(CardSet::full());
		assert_eq!(modifiers, Modifier::Schneider | Modifier::Schwarz);
	}

	#[test]
	fn derive_modifiers_from_won_cards_opponent_barely_schneider() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Ten | Suit::Diamond);
		cards.insert(Rank::Ten | Suit::Heart);
		cards.insert(Rank::Ten | Suit::Spade);

		let own_cards = CardSet::full() - cards;
		assert_eq!(own_cards.iter().map(|x| x.value()).sum::<u8>(), 90);

		let modifiers = derive_modifiers_from_won_cards(own_cards);
		assert_eq!(modifiers, Modifier::Schneider);
	}

	#[test]
	fn derive_modifiers_from_won_cards_opponent_barely_not_schneider() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Ten | Suit::Diamond);
		cards.insert(Rank::Ten | Suit::Heart);
		cards.insert(Rank::King | Suit::Diamond);
		cards.insert(Rank::King | Suit::Heart);
		cards.insert(Rank::Queen | Suit::Diamond);

		let own_cards = CardSet::full() - cards;
		assert_eq!(own_cards.iter().map(|x| x.value()).sum::<u8>(), 89);

		let modifiers = derive_modifiers_from_won_cards(own_cards);
		assert_eq!(modifiers, Modifiers::empty());
	}

	#[test]
	fn derive_modifiers_from_won_cards_self_schwarz() {
		let modifiers = derive_modifiers_from_won_cards(CardSet::new());
		assert_eq!(modifiers, Modifier::Schneider | Modifier::Schwarz);
	}

	#[test]
	fn derive_modifiers_from_won_cards_self_schwarz_with_skat() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Seven | Suit::Diamond);
		cards.insert(Rank::Eight | Suit::Diamond);
		let modifiers = derive_modifiers_from_won_cards(cards);
		assert_eq!(modifiers, Modifier::Schneider | Modifier::Schwarz);
	}

	#[test]
	fn derive_modifiers_from_won_cards_self_barely_schneider() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Ten | Suit::Diamond);
		cards.insert(Rank::Ten | Suit::Heart);
		cards.insert(Rank::Ten | Suit::Spade);

		let modifiers = derive_modifiers_from_won_cards(cards);
		assert_eq!(modifiers, Modifier::Schneider);
	}

	#[test]
	fn derive_modifiers_from_won_cards_self_barely_not_schneider() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Ten | Suit::Diamond);
		cards.insert(Rank::Ten | Suit::Heart);
		cards.insert(Rank::King | Suit::Diamond);
		cards.insert(Rank::King | Suit::Heart);
		cards.insert(Rank::Queen | Suit::Diamond);

		let modifiers = derive_modifiers_from_won_cards(cards);
		assert_eq!(modifiers, Modifiers::empty());
	}

	#[test]
	fn derive_modifiers_from_won_cards_middle() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Ten | Suit::Diamond);
		cards.insert(Rank::Ten | Suit::Heart);
		cards.insert(Rank::Ten | Suit::Spade);
		cards.insert(Rank::Ace | Suit::Diamond);
		cards.insert(Rank::Ace | Suit::Heart);
		cards.insert(Rank::Ace | Suit::Spade);

		let modifiers = derive_modifiers_from_won_cards(cards);
		assert_eq!(modifiers, Modifiers::empty());
	}

	#[test]
	fn calculate_matadors_with_1() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Jack | Suit::Club);
		cards.insert(Rank::Jack | Suit::Heart);
		cards.insert(Rank::Seven | Suit::Diamond);

		assert_eq!(calculate_matadors(cards, None), Matadors::With(1));
		for suit in SUITS.iter() {
			assert_eq!(calculate_matadors(cards, Some(*suit)), Matadors::With(1));
		}
	}

	#[test]
	fn calculate_matadors_with_2() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Jack | Suit::Club);
		cards.insert(Rank::Jack | Suit::Spade);
		cards.insert(Rank::Jack | Suit::Diamond);
		cards.insert(Rank::Seven | Suit::Diamond);

		assert_eq!(calculate_matadors(cards, None), Matadors::With(2));
		for suit in SUITS.iter() {
			assert_eq!(calculate_matadors(cards, Some(*suit)), Matadors::With(2));
		}
	}

	#[test]
	fn calculate_matadors_with_3() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Jack | Suit::Club);
		cards.insert(Rank::Jack | Suit::Spade);
		cards.insert(Rank::Jack | Suit::Heart);
		cards.insert(Rank::Seven | Suit::Diamond);

		assert_eq!(calculate_matadors(cards, None), Matadors::With(3));
		for suit in SUITS.iter() {
			let mut card_copy = cards;
			card_copy.insert(Rank::Ace | *suit);
			assert_eq!(
				calculate_matadors(card_copy, Some(*suit)),
				Matadors::With(3)
			);
		}
	}

	#[test]
	fn calculate_matadors_with_4() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Jack | Suit::Club);
		cards.insert(Rank::Jack | Suit::Spade);
		cards.insert(Rank::Jack | Suit::Heart);
		cards.insert(Rank::Jack | Suit::Diamond);
		cards.insert(Rank::Seven | Suit::Diamond);

		assert_eq!(calculate_matadors(cards, None), Matadors::With(4));
		for suit in SUITS.iter() {
			let mut card_copy = cards;
			card_copy.insert(Rank::Ten | *suit);
			assert_eq!(
				calculate_matadors(card_copy, Some(*suit)),
				Matadors::With(4)
			);
		}
	}

	#[test]
	fn calculate_matadors_with_5() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Jack | Suit::Club);
		cards.insert(Rank::Jack | Suit::Spade);
		cards.insert(Rank::Jack | Suit::Heart);
		cards.insert(Rank::Jack | Suit::Diamond);
		cards.insert(Rank::Seven | Suit::Diamond);

		for suit in SUITS.iter() {
			let mut card_copy = cards;
			card_copy.insert(Rank::Ace | *suit);
			card_copy.insert(Rank::King | *suit);
			assert_eq!(
				calculate_matadors(card_copy, Some(*suit)),
				Matadors::With(5)
			);
		}
	}

	#[test]
	fn calculate_matadors_with_all() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Jack | Suit::Club);
		cards.insert(Rank::Jack | Suit::Spade);
		cards.insert(Rank::Jack | Suit::Heart);
		cards.insert(Rank::Jack | Suit::Diamond);
		cards.insert(Rank::Seven | Suit::Diamond);

		for suit in SUITS.iter() {
			let mut card_copy = cards;
			card_copy.insert(Rank::Ace | *suit);
			card_copy.insert(Rank::Ten | *suit);
			card_copy.insert(Rank::King | *suit);
			card_copy.insert(Rank::Queen | *suit);
			card_copy.insert(Rank::Nine | *suit);
			card_copy.insert(Rank::Eight | *suit);
			card_copy.insert(Rank::Seven | *suit);
			assert_eq!(
				calculate_matadors(card_copy, Some(*suit)),
				Matadors::With(11)
			);
		}
	}

	#[test]
	fn calculate_matadors_without_1() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Jack | Suit::Spade);
		cards.insert(Rank::Seven | Suit::Diamond);

		assert_eq!(calculate_matadors(cards, None), Matadors::Without(1));
		for suit in SUITS.iter() {
			assert_eq!(calculate_matadors(cards, Some(*suit)), Matadors::Without(1));
		}
	}

	#[test]
	fn calculate_matadors_without_2() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Jack | Suit::Heart);
		cards.insert(Rank::Seven | Suit::Diamond);

		assert_eq!(calculate_matadors(cards, None), Matadors::Without(2));
		for suit in SUITS.iter() {
			assert_eq!(calculate_matadors(cards, Some(*suit)), Matadors::Without(2));
		}
	}

	#[test]
	fn calculate_matadors_without_3() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Jack | Suit::Diamond);
		cards.insert(Rank::Seven | Suit::Diamond);

		assert_eq!(calculate_matadors(cards, None), Matadors::Without(3));
		for suit in SUITS.iter() {
			assert_eq!(calculate_matadors(cards, Some(*suit)), Matadors::Without(3));
		}
	}

	#[test]
	fn calculate_matadors_without_4() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Seven | Suit::Diamond);

		assert_eq!(calculate_matadors(cards, None), Matadors::Without(4));
		for suit in SUITS.iter() {
			let mut card_copy = cards;
			card_copy.insert(Rank::Ace | *suit);
			assert_eq!(
				calculate_matadors(card_copy, Some(*suit)),
				Matadors::Without(4)
			);
		}
	}

	#[test]
	fn calculate_matadors_without_5() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Seven | Suit::Diamond);

		for suit in SUITS.iter() {
			let mut card_copy = cards;
			card_copy.insert(Rank::Ten | *suit);
			assert_eq!(
				calculate_matadors(card_copy, Some(*suit)),
				Matadors::Without(5)
			);
		}
	}

	#[test]
	fn calculate_matadors_without_all() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Seven | Suit::Diamond);
		cards.insert(Rank::Seven | Suit::Heart);
		cards.insert(Rank::Seven | Suit::Spade);
		cards.insert(Rank::Seven | Suit::Club);

		for suit in SUITS.iter() {
			let mut card_copy = cards;
			card_copy.remove(Rank::Seven | *suit);
			assert_eq!(
				calculate_matadors(card_copy, Some(*suit)),
				Matadors::Without(11)
			);
		}
	}

	#[test]
	fn calculate_game_value_null_games() {
		assert_eq!(
			calculate_game_value(GameType::Null, Modifiers::empty(), Matadors::With(255)),
			23
		);
		assert_eq!(
			calculate_game_value(GameType::Null, Modifier::Hand.into(), Matadors::With(255)),
			35
		);
		assert_eq!(
			calculate_game_value(GameType::Null, Modifier::Ouvert.into(), Matadors::With(255)),
			46
		);
		assert_eq!(
			calculate_game_value(
				GameType::Null,
				Modifier::Hand | Modifier::Ouvert,
				Matadors::With(255)
			),
			59
		);
	}

	#[test]
	fn calculate_game_value_suit_game_with_5() {
		assert_eq!(
			calculate_game_value(GameType::Diamonds, Modifiers::empty(), Matadors::With(5)),
			9 * 6,
		)
	}

	#[test]
	fn calculate_game_value_grand_without_2() {
		assert_eq!(
			calculate_game_value(GameType::Grand, Modifiers::empty(), Matadors::Without(2)),
			24 * 3,
		)
	}

	#[test]
	fn calculate_game_value_grand_ouvert_with_4() {
		assert_eq!(
			calculate_game_value(
				GameType::Grand,
				Modifier::Ouvert
					| Modifier::SchwarzAnnounced
					| Modifier::SchneiderAnnounced
					| Modifier::Hand,
				Matadors::With(4),
			),
			24 * 11,
		)
	}

	#[test]
	fn win_factor_won_ouvert() {
		assert_eq!(
			calculate_win_factor(
				Modifier::Ouvert
					| Modifier::SchwarzAnnounced
					| Modifier::Schwarz | Modifier::SchneiderAnnounced
					| Modifier::Schneider
					| Modifier::Hand
			),
			7,
		);
	}

	#[test]
	fn win_factor_non_schwarz_ouvert() {
		assert_eq!(
			calculate_win_factor(
				Modifier::Ouvert
					| Modifier::SchwarzAnnounced
					| Modifier::SchneiderAnnounced
					| Modifier::Schneider
					| Modifier::Hand
			),
			7,
		);
	}

	#[test]
	fn win_factor_non_schneider_ouvert() {
		assert_eq!(
			calculate_win_factor(
				Modifier::Ouvert
					| Modifier::SchwarzAnnounced
					| Modifier::SchneiderAnnounced
					| Modifier::Hand
			),
			7,
		);
	}

	#[test]
	fn win_factor_won_schneider_announcement() {
		assert_eq!(
			calculate_win_factor(
				Modifier::SchneiderAnnounced | Modifier::Schneider | Modifier::Hand
			),
			4,
		);
	}

	#[test]
	fn win_factor_schneider_announcement_with_schwarz() {
		assert_eq!(
			calculate_win_factor(
				Modifier::SchneiderAnnounced
					| Modifier::Schneider
					| Modifier::Hand | Modifier::Schwarz
			),
			5,
		);
	}

	#[test]
	fn win_factor_schneider() {
		assert_eq!(calculate_win_factor(Modifier::Schneider.into()), 2,);
	}

	#[test]
	fn win_factor_schwarz() {
		assert_eq!(
			calculate_win_factor(Modifier::Schneider | Modifier::Schwarz),
			3,
		);
	}

	#[test]
	fn win_factor_base() {
		assert_eq!(calculate_win_factor(Modifiers::empty()), 1,);
	}

	#[test]
	fn find_non_bidding_loss_reasons_not_enough_points() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Seven | Suit::Diamond);

		assert_eq!(
			find_point_loss_reasons(cards),
			LossReasons::from(LossReason::NotEnoughPoints)
		);
	}

	#[test]
	fn find_non_bidding_loss_reasons_enough_points() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Ace | Suit::Diamond);
		cards.insert(Rank::Ace | Suit::Heart);
		cards.insert(Rank::Ace | Suit::Spade);
		cards.insert(Rank::Ace | Suit::Club);
		cards.insert(Rank::Ten | Suit::Diamond);
		cards.insert(Rank::King | Suit::Diamond);
		cards.insert(Rank::Queen | Suit::Diamond);

		assert_eq!(find_point_loss_reasons(cards), LossReasons::empty());
	}

	#[test]
	fn find_non_bidding_loss_reasons_barely_not_enough_points() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Ace | Suit::Diamond);
		cards.insert(Rank::Ace | Suit::Heart);
		cards.insert(Rank::Ace | Suit::Spade);
		cards.insert(Rank::Ace | Suit::Club);
		cards.insert(Rank::Ten | Suit::Diamond);
		cards.insert(Rank::King | Suit::Diamond);
		cards.insert(Rank::Jack | Suit::Diamond);

		assert_eq!(
			find_point_loss_reasons(cards),
			LossReasons::from(LossReason::NotEnoughPoints)
		);
	}

	#[test]
	fn find_modifier_loss_reasons_missing_schneider() {
		assert_eq!(
			find_modifier_loss_reasons(Modifier::SchneiderAnnounced | Modifier::Hand),
			LossReasons::from(LossReason::MissingSchneider)
		);
	}

	#[test]
	fn find_modifier_loss_reasons_missing_schwarz() {
		assert_eq!(
			find_modifier_loss_reasons(
				Modifier::SchwarzAnnounced
					| Modifier::SchneiderAnnounced
					| Modifier::Schneider
					| Modifier::Hand,
			),
			LossReasons::from(LossReason::MissingSchwarz)
		);
	}

	#[test]
	fn find_modifier_loss_reasons_with_announced_schneider() {
		assert_eq!(
			find_modifier_loss_reasons(
				Modifier::SchneiderAnnounced | Modifier::Schneider | Modifier::Hand
			),
			LossReasons::empty()
		);
	}

	#[test]
	fn find_modifier_loss_reasons_with_announced_schwarz() {
		assert_eq!(
			find_modifier_loss_reasons(
				Modifier::SchwarzAnnounced
					| Modifier::Schwarz | Modifier::SchneiderAnnounced
					| Modifier::Schneider
					| Modifier::Hand,
			),
			LossReasons::empty()
		);
	}

	#[test]
	fn evaluate_game_suit_game_won() {
		let (value, _, reasons) = evaluate_game(
			GameType::Diamonds,
			Modifiers::empty(),
			Matadors::With(11),
			CardSet::full(),
			18,
		);
		assert_eq!(reasons, LossReasons::empty());
		assert_eq!(value, 9 * 14)
	}

	#[test]
	fn evaluate_game_null_game_won() {
		let (value, _, reasons) = evaluate_game(
			GameType::Null,
			Modifiers::empty(),
			Matadors::With(11),
			CardSet::new(),
			18,
		);
		assert_eq!(reasons, LossReasons::empty());
		assert_eq!(value, 23)
	}

	#[test]
	fn evaluate_game_null_game_overbid() {
		let (value, _, reasons) = evaluate_game(
			GameType::Null,
			Modifiers::empty(),
			Matadors::With(11),
			CardSet::new(),
			24,
		);
		assert_eq!(reasons, LossReasons::from(LossReason::Overbid));
		assert_eq!(value, 23)
	}

	#[test]
	fn evaluate_game_null_game_won_with_pushed_skat() {
		let mut cards = CardSet::new();
		cards.insert(Rank::Seven | Suit::Diamond);
		cards.insert(Rank::Eight | Suit::Diamond);

		let (value, _, reasons) = evaluate_game(
			GameType::Null,
			Modifiers::empty(),
			Matadors::With(11),
			cards,
			18,
		);
		assert_eq!(reasons, LossReasons::empty());
		assert_eq!(value, 23)
	}

	#[test]
	fn relative_player_reverse() {
		let new_forehand = Player::Second;
		assert_eq!(
			RelativePlayer::Forehand.into_absolute_reverse(new_forehand),
			Player::Second
		);
		assert_eq!(
			RelativePlayer::Midhand.into_absolute_reverse(new_forehand),
			Player::First
		);
		assert_eq!(
			RelativePlayer::Rearhand.into_absolute_reverse(new_forehand),
			Player::Third
		);

		let new_forehand = Player::First;
		assert_eq!(
			RelativePlayer::Forehand.into_absolute_reverse(new_forehand),
			Player::First
		);
		assert_eq!(
			RelativePlayer::Midhand.into_absolute_reverse(new_forehand),
			Player::Third
		);
		assert_eq!(
			RelativePlayer::Rearhand.into_absolute_reverse(new_forehand),
			Player::Second
		);

		let new_forehand = Player::Third;
		assert_eq!(
			RelativePlayer::Forehand.into_absolute_reverse(new_forehand),
			Player::Third
		);
		assert_eq!(
			RelativePlayer::Midhand.into_absolute_reverse(new_forehand),
			Player::Second
		);
		assert_eq!(
			RelativePlayer::Rearhand.into_absolute_reverse(new_forehand),
			Player::First
		);
	}

	#[test]
	fn relative_player_from_absolute() {
		let forehand = Player::First;
		assert_eq!(
			RelativePlayer::from_absolute(forehand, Player::First),
			RelativePlayer::Forehand
		);
		assert_eq!(
			RelativePlayer::from_absolute(forehand, Player::Second),
			RelativePlayer::Midhand
		);
		assert_eq!(
			RelativePlayer::from_absolute(forehand, Player::Third),
			RelativePlayer::Rearhand
		);

		let forehand = Player::Second;
		assert_eq!(
			RelativePlayer::from_absolute(forehand, Player::Second),
			RelativePlayer::Forehand
		);
		assert_eq!(
			RelativePlayer::from_absolute(forehand, Player::Third),
			RelativePlayer::Midhand
		);
		assert_eq!(
			RelativePlayer::from_absolute(forehand, Player::First),
			RelativePlayer::Rearhand
		);

		let forehand = Player::Third;
		assert_eq!(
			RelativePlayer::from_absolute(forehand, Player::Third),
			RelativePlayer::Forehand
		);
		assert_eq!(
			RelativePlayer::from_absolute(forehand, Player::First),
			RelativePlayer::Midhand
		);
		assert_eq!(
			RelativePlayer::from_absolute(forehand, Player::Second),
			RelativePlayer::Rearhand
		);
	}
}
