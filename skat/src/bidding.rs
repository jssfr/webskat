use serde::{Deserialize, Serialize};

use crate::model::Player;
use crate::state::{GameError, PerPlayer};

pub type GameValue = u32;

#[derive(Debug, Serialize, Deserialize)]
struct PlayerState {
	last_bid: Option<GameValue>,
	has_passed: bool,
}

impl Default for PlayerState {
	fn default() -> Self {
		Self {
			last_bid: None,
			has_passed: false,
		}
	}
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum BiddingResult {
	PassedOut,
	Won(Player, GameValue),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct BiddingState {
	players: PerPlayer<PlayerState>,
	result: Option<BiddingResult>,
	last_bid: Option<GameValue>,
	awaiting_response: bool,
}

impl BiddingState {
	pub fn new() -> Self {
		Self {
			players: PerPlayer::default(),
			result: None,
			last_bid: None,
			awaiting_response: false,
		}
	}

	fn determine_participants(&self) -> (Option<Player>, Option<Player>) {
		if self.result.is_some() {
			// bidding is over, exit
			return (None, None);
		}

		let forehand_passed = self.players[Player::First].has_passed;
		let midhand_passed = self.players[Player::Second].has_passed;
		let rearhand_passed = self.players[Player::Third].has_passed;

		if forehand_passed {
			if rearhand_passed {
				if midhand_passed {
					// everyone passed!
					(None, None)
				} else {
					(Some(Player::Second), None)
				}
			} else {
				if midhand_passed {
					(Some(Player::Third), None)
				} else {
					(Some(Player::Third), Some(Player::Second))
				}
			}
		} else {
			if midhand_passed {
				if rearhand_passed {
					(Some(Player::First), None)
				} else {
					(Some(Player::Third), Some(Player::First))
				}
			} else {
				(Some(Player::Second), Some(Player::First))
			}
		}
	}

	fn autoconclude(&mut self) {
		if self.result.is_some() {
			return;
		}
		let (caller, responder) = self.determine_participants();
		if let Some(caller) = caller {
			// if there still is a responder, we cannot conclude
			if responder.is_some() {
				return;
			}
			let bid = match self.players[caller].last_bid {
				// if there is no responder, but the caller also hasn't made a bid, the caller needs to make a bid first
				// this can happen if First and Second pass
				None => return,
				Some(v) => v,
			};
			// the caller is the only one left, and they made a bid -> conclude to that.
			self.result = Some(BiddingResult::Won(caller, bid));
		} else {
			// there cannot be a responder without a caller
			assert!(responder.is_none());
			self.result = Some(BiddingResult::PassedOut);
		}
	}

	pub fn responder(&self) -> Option<Player> {
		self.determine_participants().1
	}

	pub fn caller(&self) -> Option<Player> {
		self.determine_participants().0
	}

	pub fn finished(&self) -> Option<BiddingResult> {
		self.result
	}

	pub fn last_bid(&self) -> Option<GameValue> {
		self.last_bid
	}

	pub fn player_bids(&self) -> PerPlayer<Option<GameValue>> {
		self.players.iter().map(|x| x.last_bid).collect()
	}

	pub fn bid(&mut self, player: Player, value: Option<GameValue>) -> Result<(), GameError> {
		if self.awaiting_response {
			return Err(GameError::NotYourTurn);
		}

		let (caller, _) = self.determine_participants();
		if !caller.map(|x| x == player).unwrap_or(false) {
			return Err(GameError::NotYourTurn);
		}

		if let Some(value) = value {
			if self.last_bid.map(|x| x >= value).unwrap_or(false) {
				return Err(GameError::BidTooLow);
			}
			self.players[player].last_bid = Some(value);
			self.last_bid = Some(value);
			self.awaiting_response = true;
		} else {
			// pass
			self.players[player].has_passed = true;
		}
		self.autoconclude();
		Ok(())
	}

	pub fn respond(&mut self, player: Player, hold: bool) -> Result<(), GameError> {
		if !self.awaiting_response {
			return Err(GameError::NotYourTurn);
		}

		let (caller, responder) = self.determine_participants();
		if !responder.map(|x| x == player).unwrap_or(false) {
			return Err(GameError::NotYourTurn);
		}
		let caller = caller.expect("caller for responder");

		self.players[player].has_passed = !hold;
		if hold {
			self.players[player].last_bid = self.players[caller].last_bid;
		}
		self.awaiting_response = false;
		self.autoconclude();
		Ok(())
	}

	pub fn awaiting_response(&self) -> bool {
		self.awaiting_response
	}
}

pub const BIDDING_SEQUENCE: [u16; 75] = [
	18, 20, 22, 23, 24, 27, 30, 33, 35, 36, 40, 44, 45, 46, 48, 50, 54, 55, 59, 60, 63, 66, 70, 72,
	77, 80, 81, 84, 88, 90, 96, 99, 100, 108, 110, 117, 120, 121, 126, 130, 132, 135, 140, 143,
	144, 150, 153, 154, 156, 160, 162, 165, 168, 170, 171, 176, 180, 187, 190, 192, 198, 204, 209,
	216, 228, 240, 264, 288, 312, 336, 360, 384, 408, 432, 456,
];

pub fn get_next_bid(curr: u16) -> Option<u16> {
	match BIDDING_SEQUENCE.binary_search(&curr) {
		Ok(i) => BIDDING_SEQUENCE.get(i + 1).copied(),
		Err(i) => Some(BIDDING_SEQUENCE[i]),
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn bidding_sequence_finds_next_if_equal_exists() {
		for (i, v) in BIDDING_SEQUENCE.iter().copied().enumerate() {
			assert_eq!(get_next_bid(v), BIDDING_SEQUENCE.get(i + 1).copied());
		}
	}

	#[test]
	fn bidding_sequence_first_bid() {
		assert_eq!(get_next_bid(0), Some(18));
	}

	#[test]
	fn bidding_sequence_from_inbetween_number() {
		for (i, v) in BIDDING_SEQUENCE.iter().copied().enumerate() {
			assert_eq!(get_next_bid(v - 1), Some(BIDDING_SEQUENCE[i]));
		}
	}

	#[test]
	fn hörer_sager() {
		let state = BiddingState::new();
		assert_eq!(state.caller(), Some(Player::Second));
		assert_eq!(state.responder(), Some(Player::First));
		match state.finished() {
			None => (),
			other => panic!("unexpected finished state: {:?}", other),
		};
	}

	#[test]
	fn weitersager() {
		let mut state = BiddingState::new();
		assert_eq!(state.caller(), Some(Player::Second));
		assert_eq!(state.responder(), Some(Player::First));
		state.bid(Player::Second, None).expect("all is fine");
		assert_eq!(state.caller(), Some(Player::Third));
		assert_eq!(state.responder(), Some(Player::First));
		match state.finished() {
			None => (),
			other => panic!("unexpected finished state: {:?}", other),
		};
	}

	#[test]
	fn ramsch() {
		let mut state = BiddingState::new();
		state.bid(Player::Second, None).expect("all is fine");
		state.bid(Player::Third, None).expect("all is fine");
		state.bid(Player::First, None).expect("all is fine");
		match state.finished() {
			Some(BiddingResult::PassedOut) => (),
			other => panic!("unexpected finished state in ramsch: {:?}", other),
		};
	}

	#[test]
	fn basic_bidding() {
		let mut state = BiddingState::new();
		state.bid(Player::Second, Some(18)).expect("all is fine");
		assert_eq!(state.last_bid(), Some(18));
		state.respond(Player::First, true).expect("all is fine");
		state.bid(Player::Second, Some(20)).expect("all is fine");
		state.respond(Player::First, true).expect("all is fine");
		state.bid(Player::Second, None).expect("all is fine");
		state.bid(Player::Third, Some(22)).expect("all is fine");
		state.respond(Player::First, false).expect("all is fine");

		match state.finished() {
			Some(BiddingResult::Won(Player::Third, 22)) => (),
			other => panic!("unexpected finished state: {:?}", other),
		};
	}

	#[test]
	fn reject_initial_bid_from_non_caller() {
		let mut state = BiddingState::new();

		match state.bid(Player::First, Some(18)) {
			Err(GameError::NotYourTurn) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Third, Some(18)) {
			Err(GameError::NotYourTurn) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};
	}

	#[test]
	fn reject_initial_response() {
		let mut state = BiddingState::new();

		match state.respond(Player::First, true) {
			Err(GameError::NotYourTurn) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::Second, true) {
			Err(GameError::NotYourTurn) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::Third, true) {
			Err(GameError::NotYourTurn) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};
	}

	#[test]
	fn reject_response_from_non_responder() {
		let mut state = BiddingState::new();

		match state.bid(Player::Second, Some(18)) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::Second, true) {
			Err(GameError::NotYourTurn) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::Third, true) {
			Err(GameError::NotYourTurn) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};
	}

	#[test]
	fn reject_bid_after_bid() {
		let mut state = BiddingState::new();

		match state.bid(Player::Second, Some(18)) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::First, Some(20)) {
			Err(GameError::NotYourTurn) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Second, Some(20)) {
			Err(GameError::NotYourTurn) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Third, Some(20)) {
			Err(GameError::NotYourTurn) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};
	}

	#[test]
	fn reject_lower_or_equal_bid() {
		let mut state = BiddingState::new();

		match state.bid(Player::Second, Some(18)) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::First, true) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Second, Some(18)) {
			Err(GameError::BidTooLow) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Second, Some(17)) {
			Err(GameError::BidTooLow) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};
	}

	#[test]
	fn middlehand_takes_with_18() {
		let mut state = BiddingState::new();

		match state.bid(Player::Second, Some(18)) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::First, false) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Third, None) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.finished() {
			Some(BiddingResult::Won(Player::Second, 18)) => (),
			other => panic!("unexpected finished state: {:?}", other),
		};
	}

	#[test]
	fn forehand_takes_with_18() {
		let mut state = BiddingState::new();

		match state.bid(Player::Second, Some(18)) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::First, true) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Second, None) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Third, None) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.finished() {
			Some(BiddingResult::Won(Player::First, 18)) => (),
			other => panic!("unexpected finished state: {:?}", other),
		};
	}

	#[test]
	fn rearhand_takes_with_18() {
		let mut state = BiddingState::new();

		match state.bid(Player::Second, None) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Third, Some(18)) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::First, false) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.finished() {
			Some(BiddingResult::Won(Player::Third, 18)) => (),
			other => panic!("unexpected finished state: {:?}", other),
		};
	}

	#[test]
	fn forehand_takes_with_18_as_last() {
		let mut state = BiddingState::new();

		match state.bid(Player::Second, None) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Third, Some(18)) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::First, true) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Third, None) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.finished() {
			Some(BiddingResult::Won(Player::First, 18)) => (),
			other => panic!("unexpected finished state: {:?}", other),
		};
	}

	#[test]
	fn forehand_takes_with_20_after_some_haggling() {
		let mut state = BiddingState::new();

		match state.bid(Player::Second, Some(18)) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::First, true) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Second, Some(20)) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::First, true) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Second, None) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Third, None) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.finished() {
			Some(BiddingResult::Won(Player::First, 20)) => (),
			other => panic!("unexpected finished state: {:?}", other),
		};
	}

	#[test]
	fn rearhand_takes_with_20_after_some_haggling() {
		let mut state = BiddingState::new();

		match state.bid(Player::Second, Some(18)) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::First, true) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Second, None) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Third, Some(20)) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::First, false) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.finished() {
			Some(BiddingResult::Won(Player::Third, 20)) => (),
			other => panic!("unexpected finished state: {:?}", other),
		};
	}

	#[test]
	fn rearhand_takes_with_20_after_some_haggling_after_inital_pass() {
		let mut state = BiddingState::new();

		match state.bid(Player::Second, None) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Third, Some(18)) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::First, true) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.bid(Player::Third, Some(20)) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.respond(Player::First, false) {
			Ok(()) => (),
			other => panic!("unexpected bid result: {:?}", other),
		};

		match state.finished() {
			Some(BiddingResult::Won(Player::Third, 20)) => (),
			other => panic!("unexpected finished state: {:?}", other),
		};
	}
}
