#![allow(unused_variables)]

use rand::distributions::Distribution;

use crate::bidding::GameValue;
use crate::cards::{Card, CardSet, CardStack};
use crate::model::{GameType, Modifiers, Player, Trick};
use crate::state::PerPlayer;

use super::{Declaration, HandDeclaration, HandDeclarationStep, PlayError, PlaySkat};

pub struct RandomPlayer {}

impl Default for RandomPlayer {
	fn default() -> Self {
		Self {}
	}
}

impl PlaySkat for RandomPlayer {
	fn call_bid(
		&mut self,
		your_index: Player,
		your_hand: CardSet,
		player_bids: PerPlayer<Option<GameValue>>,
	) -> Result<Option<GameValue>, PlayError> {
		if player_bids.iter().all(|x| x.is_none()) {
			log::debug!("nobody called 18 yet, so I'm gonna ('Achtzehn hat man immer!')");
			Ok(Some(18))
		} else {
			log::debug!("someone else will take it, so I'm not going to call a bid");
			Ok(None)
		}
	}

	fn respond_bid(
		&mut self,
		your_index: Player,
		your_hand: CardSet,
		player_bids: PerPlayer<Option<GameValue>>,
		caller: Player,
	) -> Result<bool, PlayError> {
		log::debug!("passing");
		Ok(false)
	}

	fn declare_hand(
		&mut self,
		your_index: Player,
		your_hand: CardSet,
		called_game_value: GameValue,
	) -> Result<HandDeclarationStep, PlayError> {
		log::debug!("I don't know how to play so I just announce Hearts");
		Ok(HandDeclarationStep::Declare(HandDeclaration {
			game_type: GameType::Hearts,
			modifiers: None,
		}))
	}

	fn declare(
		&mut self,
		your_index: Player,
		your_hand: CardSet,
		called_game_value: GameValue,
	) -> Result<Declaration, PlayError> {
		unreachable!();
	}

	fn play(
		&mut self,
		your_index: Player,
		your_hand: CardSet,
		hands: PerPlayer<u8>,
		declarer: Player,
		forehand: Player,
		declarer_hand: Option<CardSet>,
		game_type: GameType,
		base_modifiers: Modifiers,
		table: CardStack,
		last_trick: Option<Trick>,
	) -> Result<Card, PlayError> {
		let candidates = if table.len() > 0 {
			let mut candidates = CardStack::new();
			// uuuuhhh we have to figure out how the heck to follow suit
			let first_card = table[0];
			let effective_suit = game_type.effective_suit(&first_card);
			for card in your_hand.iter() {
				if game_type.effective_suit(&card) == effective_suit {
					candidates.push(card);
				}
			}
			if candidates.len() == 0 {
				// no matching card, so just play any
				your_hand.iter().collect()
			} else {
				candidates
			}
		} else {
			your_hand.iter().collect()
		};
		assert!(candidates.len() > 0);
		let i =
			rand::distributions::Uniform::from(0..candidates.len()).sample(&mut rand::thread_rng());
		log::debug!(
			"picked {:?} out of {:?} (hand = {:?})",
			candidates[i],
			candidates,
			your_hand
		);
		Ok(candidates[i])
	}
}
