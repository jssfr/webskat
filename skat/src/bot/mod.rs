use crate::bidding::GameValue;
use crate::cards::{Card, CardSet, CardStack};
use crate::model::{AnnouncableModifier, GameType, Modifiers, Player, Trick};
use crate::state::PerPlayer;

mod pimc;
mod random;

pub use pimc::PimcPlayer;
pub use random::RandomPlayer;

#[derive(thiserror::Error, Debug)]
pub enum PlayError {}

pub trait PlaySkat {
	fn call_bid(
		&mut self,
		your_index: Player,
		your_hand: CardSet,
		player_bids: PerPlayer<Option<GameValue>>,
	) -> Result<Option<GameValue>, PlayError>;

	fn respond_bid(
		&mut self,
		your_index: Player,
		your_hand: CardSet,
		player_bids: PerPlayer<Option<GameValue>>,
		caller: Player,
	) -> Result<bool, PlayError>;

	fn declare_hand(
		&mut self,
		your_index: Player,
		your_hand: CardSet,
		called_game_value: GameValue,
	) -> Result<HandDeclarationStep, PlayError>;

	fn declare(
		&mut self,
		your_index: Player,
		your_hand: CardSet,
		called_game_value: GameValue,
	) -> Result<Declaration, PlayError>;

	fn play(
		&mut self,
		your_index: Player,
		your_hand: CardSet,
		hands: PerPlayer<u8>,
		declarer: Player,
		forehand: Player,
		declarer_hand: Option<CardSet>,
		game_type: GameType,
		base_modifiers: Modifiers,
		table: CardStack,
		last_trick: Option<Trick>,
	) -> Result<Card, PlayError>;
}

pub struct HandDeclaration {
	pub game_type: GameType,
	pub modifiers: Option<AnnouncableModifier>,
}

pub struct Declaration {
	pub game_type: GameType,
	pub push_cards: [Card; 2],
}

pub enum HandDeclarationStep {
	Declare(HandDeclaration),
	TakeSkat,
}
