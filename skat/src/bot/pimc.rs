/*!
# Perfect-Information Monte-Carlo search

This skat player uses simple perfect information monte carlo sampling, without
taking into account per-player information.

This work is laregly based on Long, 2011 and Knorr, 2018.

Its bidding logic is abhorrent.
*/
use std::time::{Duration, Instant};

use rand::seq::SliceRandom;
use rand::Rng;

use crate::bidding::{get_next_bid, GameValue};
use crate::cards::{Card, CardSet, CardStack, Rank, SUITS};
use crate::model::{GameType, Modifiers, PerEffectiveSuit, Player, RelativePlayer, Trick};
use crate::play::{PlayerState, PlayingState};
use crate::state::{GameError, PerPlayer};

use super::{Declaration, HandDeclaration, HandDeclarationStep, PlayError, PlaySkat};

const MIN_MATADORS: u8 = 5;

#[derive(Debug, Clone)]
struct World {
	#[allow(dead_code)]
	player: Player,
	state: PlayingState,
}

impl World {
	fn sample<R: Rng + ?Sized>(
		r: &mut R,
		player: Player,
		declarer: Player,
		forehand: Player,
		game_type: GameType,
		won_cards: PerPlayer<CardSet>,
		last_trick: Option<Trick>,
		own_hand: CardSet,
		table: CardStack,
		_suitfreeness: &PerPlayer<PerEffectiveSuit<bool>>,
	) -> World {
		let mut hands = PerPlayer::init_fn(CardSet::new);
		hands[player] = own_hand;

		let player_from_forehand = RelativePlayer::from_absolute(forehand, player);
		let card_count = PerPlayer::from_fn(|i| {
			let from_forehand = RelativePlayer::from_absolute(forehand, i);
			if from_forehand >= player_from_forehand {
				own_hand.len()
			} else {
				own_hand.len() - 1
			}
		});

		let mut unavailable_cards = won_cards.reduce(|a, b| a | b);
		unavailable_cards.extend(table.into_iter());
		unavailable_cards |= own_hand;

		let mut available_cards = !unavailable_cards;
		for (hand, target_count) in hands.iter_mut().zip(card_count.into_iter()) {
			while hand.len() < target_count {
				hand.insert(available_cards.take_sampled(r));
			}
		}
		assert!(
			available_cards.len() == 2 || available_cards.len() == 0,
			"unexpectedly have cards left over. left over cards = {:?}; hands: {:?}, counts: {:?}",
			available_cards,
			hands,
			card_count
		);

		Self {
			player,
			state: PlayingState {
				forehand,
				current: player,
				declarer,
				game_type,
				last_trick,
				table,
				players: hands
					.into_iter()
					.zip(won_cards.into_iter())
					.map(|(hand, won_cards)| PlayerState {
						won_cards: won_cards.into_iter().collect(),
						hand,
					})
					.collect(),
			},
		}
	}

	fn sign_for_player(&self, winning_player: Player, from_perspective: Player) -> i32 {
		if winning_player == from_perspective
			|| (from_perspective != self.state.declarer && winning_player != self.state.declarer)
		{
			1
		} else {
			-1
		}
	}

	#[allow(dead_code)]
	fn best_move_for_turn_player_by_won_points(&self) -> (i32, Card) {
		if self.state.is_over() {
			panic!("no moves left");
		}

		let current = self.state.current;
		// iterate over all options and clone the world and see what's happening there
		let mut best_score: Option<i32> = None;
		let mut best: Option<Card> = None;
		for card in self.state.players[current].hand.into_iter() {
			let mut move_score = 0i32;
			let mut new_world = self.clone();
			match new_world.state.play(current, card) {
				Ok(Some((winning_player, trick))) => {
					let points: i32 = trick.iter().map(|card| card.value() as i32).sum();
					move_score += points * self.sign_for_player(winning_player, current);
				}
				Ok(None) => (),
				Err(_) => continue,
			}
			if !new_world.state.is_over() {
				let (subscore, _) = new_world.best_move_for_turn_player_by_won_points();
				move_score += subscore * self.sign_for_player(new_world.state.current, current);
			}
			if best_score.is_none() || best_score.unwrap() < move_score {
				best_score = Some(move_score);
				best = Some(card);
			}
		}
		(best_score.unwrap(), best.unwrap())
	}

	fn sample_move_for_turn_player_by_won_points<R: Rng + ?Sized>(
		mut self,
		r: &mut R,
	) -> (i32, Card) {
		if self.state.is_over() {
			panic!("no moves left");
		}
		let target = self.state.current;
		let mut score = 0i32;
		let mut chosen_card = None;
		while !self.state.is_over() {
			let current = self.state.current;
			let mut hand: CardStack = self.state.players[current].hand.into_iter().collect();
			hand.shuffle(r);
			for card in hand {
				match self.state.play(current, card) {
					Ok(Some((winning_player, trick))) => {
						let points: i32 = trick.iter().map(|card| card.value() as i32).sum();
						score += points * self.sign_for_player(winning_player, target);
					}
					Ok(None) => (),
					Err(GameError::MustFollowSuit) => continue,
					Err(other) => panic!("invalid move generated: {}", other),
				}
				chosen_card.get_or_insert(card);
				break;
			}
		}
		(score, chosen_card.unwrap())
	}
}

pub struct PimcPlayer {
	won_cards: PerPlayer<CardSet>,
	suitfreeness: PerPlayer<PerEffectiveSuit<bool>>,
}

impl PimcPlayer {
	pub fn new() -> Self {
		Self {
			won_cards: PerPlayer::init_fn(CardSet::new),
			suitfreeness: PerPlayer::default(),
		}
	}

	fn detect_viable_game_type(hand: CardSet) -> Option<GameType> {
		let mut suits = [0u8; 4];
		let mut any_jack = false;
		for card in hand.into_iter() {
			let (rank, suit) = card.split();
			suits[suit as u8 as usize] += 1;
			if rank == Rank::Jack {
				any_jack = true;
			}
		}
		if !any_jack {
			return None;
		}
		let mut highest = None;
		let mut highest_count = 0;
		for (suit, count) in SUITS.into_iter().zip(suits.into_iter()) {
			if count >= MIN_MATADORS && (count > highest_count || highest.is_none()) {
				highest = Some(suit.into());
				highest_count = count;
			}
		}
		highest
	}

	fn judge_game_value(hand: CardSet) -> Option<GameValue> {
		let game_type = Self::detect_viable_game_type(hand)?;
		match game_type {
			GameType::Null => Some(game_type.base_value()),
			_ => Some(game_type.base_value() * 2),
		}
	}

	fn pimc_search(
		&self,
		player: Player,
		declarer: Player,
		forehand: Player,
		game_type: GameType,
		last_trick: Option<Trick>,
		own_hand: CardSet,
		table: CardStack,
	) -> Card {
		const TMAX: Duration = Duration::new(2, 0);
		let t0 = Instant::now();
		let tend = t0 + TMAX;
		let mut rng = rand::thread_rng();
		let mut move_scores: [Option<i32>; 32] = [None; 32];
		let mut n = 0;
		log::debug!("starting PIMC search");
		while n < 20 || Instant::now() < tend {
			let world = World::sample(
				&mut rng,
				player,
				declarer,
				forehand,
				game_type,
				self.won_cards,
				last_trick,
				own_hand,
				table,
				&self.suitfreeness,
			);
			log::trace!("looking at world {:?}", world);
			let (score, card) = world.sample_move_for_turn_player_by_won_points(&mut rng);
			move_scores[card.bits() as usize] = Some(
				move_scores[card.bits() as usize]
					.get_or_insert(0)
					.saturating_add(score),
			);
			n += 1;
		}
		let t1 = Instant::now();
		let (i, score) = move_scores
			.into_iter()
			.enumerate()
			.filter_map(|(i, x)| x.map(|x| (i, x)))
			.max_by_key(|(_, v)| *v)
			.unwrap();
		let card = Card::from_bits(i as u8).unwrap();
		log::debug!(
			"evaluated {} worlds in {} seconds: selected card {:?} from hand {:?} with score {}",
			n,
			(t1 - t0).as_secs_f32(),
			card,
			own_hand,
			score
		);
		card
	}
}

impl PlaySkat for PimcPlayer {
	fn call_bid(
		&mut self,
		_your_index: Player,
		your_hand: CardSet,
		player_bids: PerPlayer<Option<GameValue>>,
	) -> Result<Option<GameValue>, PlayError> {
		let game_value = match Self::judge_game_value(your_hand) {
			Some(v) => v,
			None => return Ok(None),
		};
		let highest_bid = player_bids.into_iter().filter_map(|x| x).max();
		let next_bid = highest_bid
			.and_then(|x| get_next_bid(x as u16))
			.unwrap_or(18) as u32;
		if next_bid <= game_value {
			Ok(Some(next_bid))
		} else {
			Ok(None)
		}
	}

	fn respond_bid(
		&mut self,
		_your_index: Player,
		your_hand: CardSet,
		player_bids: PerPlayer<Option<GameValue>>,
		_caller: Player,
	) -> Result<bool, PlayError> {
		let game_value = match Self::judge_game_value(your_hand) {
			Some(v) => v,
			None => return Ok(false),
		};
		let highest_bid = player_bids.into_iter().filter_map(|x| x).max().unwrap();
		Ok(highest_bid <= game_value)
	}

	fn declare_hand(
		&mut self,
		_your_index: Player,
		your_hand: CardSet,
		_called_game_value: GameValue,
	) -> Result<HandDeclarationStep, PlayError> {
		let game_type = Self::detect_viable_game_type(your_hand).unwrap_or(GameType::Null);
		Ok(HandDeclarationStep::Declare(HandDeclaration {
			game_type,
			modifiers: None,
		}))
	}

	fn declare(
		&mut self,
		_your_index: Player,
		_your_hand: CardSet,
		_called_game_value: GameValue,
	) -> Result<Declaration, PlayError> {
		unreachable!();
	}

	fn play(
		&mut self,
		your_index: Player,
		your_hand: CardSet,
		_hands: PerPlayer<u8>,
		declarer: Player,
		forehand: Player,
		_declarer_hand: Option<CardSet>,
		game_type: GameType,
		_base_modifiers: Modifiers,
		table: CardStack,
		last_trick: Option<Trick>,
	) -> Result<Card, PlayError> {
		if let Some(last_trick) = last_trick {
			self.won_cards[forehand].extend(last_trick.into_iter());
		}
		Ok(self.pimc_search(
			your_index, declarer, forehand, game_type, last_trick, your_hand, table,
		))
	}
}
