use serde::{Deserialize, Serialize};

use crate::cards::{Card, CardSet, CardStack};
use crate::model::{EffectiveSuit, GameType, Player, Trick};
use crate::state::{GameError, PerPlayer};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct PlayerState {
	pub(crate) won_cards: CardStack,
	pub(crate) hand: CardSet,
}

impl PlayerState {
	fn init(hand: CardSet) -> Self {
		Self {
			hand,
			won_cards: CardStack::new(),
		}
	}
}

impl Default for PlayerState {
	fn default() -> Self {
		Self {
			won_cards: CardStack::new(),
			hand: CardSet::with_capacity(10),
		}
	}
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PlayingState {
	pub(crate) forehand: Player,
	pub(crate) current: Player,
	pub(crate) declarer: Player,
	pub(crate) game_type: GameType,
	pub(crate) last_trick: Option<Trick>,
	pub(crate) table: CardStack,
	pub(crate) players: PerPlayer<PlayerState>,
}

impl PlayingState {
	pub fn new(
		declarer: Player,
		game_type: GameType,
		hands: PerPlayer<CardSet>,
		pushed_cards: [Card; 2],
	) -> Self {
		let mut result = Self {
			forehand: Player::First,
			current: Player::First,
			declarer,
			game_type,
			last_trick: None,
			table: CardStack::with_capacity(3),
			players: hands.into_iter().map(PlayerState::init).collect(),
		};
		result.players[declarer]
			.won_cards
			.extend(pushed_cards.into_iter());
		result
	}

	pub fn is_over(&self) -> bool {
		self.players.iter().all(|x| x.hand.len() == 0)
	}

	pub fn declarer(&self) -> Player {
		self.declarer
	}

	pub fn game_type(&self) -> GameType {
		self.game_type
	}

	pub fn last_trick(&self) -> Option<&Trick> {
		self.last_trick.as_ref()
	}

	pub fn won_cards(&self, player: Player) -> &CardStack {
		&self.players[player].won_cards
	}

	pub fn current_player(&self) -> Player {
		self.current
	}

	fn table_suit(&self) -> Option<EffectiveSuit> {
		Some(self.game_type.effective_suit(self.table.get(0)?))
	}

	pub fn play(
		&mut self,
		player: Player,
		card: Card,
	) -> Result<Option<(Player, Trick)>, GameError> {
		if player != self.current {
			return Err(GameError::NotYourTurn);
		}

		if let Some(table_suit) = self.table_suit() {
			let card_suit = self.game_type.effective_suit(&card);
			if table_suit != card_suit
				&& self.players[player]
					.hand
					.iter()
					.any(|x| self.game_type.effective_suit(&x) == table_suit)
			{
				// the player *can* follow suit, so we reject this move
				return Err(GameError::MustFollowSuit);
			}
		}

		if !self.players[player].hand.remove(&card) {
			return Err(GameError::NoSuchCard);
		}

		self.table.push(card);
		self.current.wrapping_inc();
		if self.table.len() == 3 {
			Ok(Some(self.conclude_trick()))
		} else {
			Ok(None)
		}
	}

	fn conclude_trick(&mut self) -> (Player, Trick) {
		let mut table = self.table.drain(..);
		let trick: Trick = [
			table.next().unwrap(),
			table.next().unwrap(),
			table.next().unwrap(),
		]
		.into();
		let last_trick_winner = self.game_type.trick_taker(&trick);
		let new_forehand = last_trick_winner.into_absolute(self.forehand);
		self.players[new_forehand]
			.won_cards
			.extend(trick.iter().copied());
		self.last_trick = Some(trick);
		self.forehand = new_forehand;
		self.current = new_forehand;
		(new_forehand, trick)
	}

	pub fn table(&self) -> &CardStack {
		&self.table
	}

	pub fn hand(&self, player: Player) -> &CardSet {
		&self.players[player].hand
	}

	pub fn hands(&self) -> PerPlayer<&CardSet> {
		self.players.map_ref(|x| &x.hand)
	}

	pub fn all_won_cards(&self) -> PerPlayer<&CardStack> {
		self.players.map_ref(|x| &x.won_cards)
	}

	pub fn forehand(&self) -> Player {
		self.forehand
	}
}

/**
 * From a trick and the current forehand, return the cards played by each
 * player as well as the forehand when the trick was played.
 */
#[allow(dead_code)]
fn transpose_trick(
	game_type: GameType,
	trick: Trick,
	new_forehand: Player,
) -> (PerPlayer<Card>, Player) {
	let winner = game_type.trick_taker(&trick);
	let prev_forehand = winner.into_absolute_reverse(new_forehand);
	let mut player = prev_forehand;
	let mut cards: PerPlayer<Option<Card>> = PerPlayer::default();
	for card in trick.into_iter() {
		cards[player] = Some(card);
		player.wrapping_inc();
	}
	(
		cards.into_iter().map(|x| x.unwrap()).collect(),
		prev_forehand,
	)
}

#[cfg(test)]
mod tests {
	use super::*;

	use crate::cards::{Rank, Suit};
	use crate::model::RelativePlayer;

	fn take_from(set: &mut CardSet, card: Card) -> Card {
		if !set.remove(&card) {
			panic!("card {:?} already gone!", card)
		}
		card
	}

	fn build_state() -> PlayingState {
		let mut deck: CardSet = crate::cards::full_deck().into_iter().collect();
		let mut hands: PerPlayer<CardSet> = [
			[
				take_from(&mut deck, Rank::Jack | Suit::Club),
				take_from(&mut deck, Rank::Jack | Suit::Spade),
				take_from(&mut deck, Rank::Ace | Suit::Heart),
				take_from(&mut deck, Rank::Ten | Suit::Heart),
				take_from(&mut deck, Rank::King | Suit::Heart),
				take_from(&mut deck, Rank::Queen | Suit::Heart),
				take_from(&mut deck, Rank::Seven | Suit::Heart),
				take_from(&mut deck, Rank::Ace | Suit::Diamond),
				take_from(&mut deck, Rank::Ten | Suit::Spade),
				take_from(&mut deck, Rank::Eight | Suit::Club),
			]
			.into_iter()
			.collect(),
			[
				take_from(&mut deck, Rank::Jack | Suit::Diamond),
				take_from(&mut deck, Rank::Ace | Suit::Club),
				take_from(&mut deck, Rank::Ten | Suit::Diamond),
				take_from(&mut deck, Rank::Queen | Suit::Diamond),
				take_from(&mut deck, Rank::Nine | Suit::Diamond),
				take_from(&mut deck, Rank::Queen | Suit::Club),
				take_from(&mut deck, Rank::Seven | Suit::Club),
				take_from(&mut deck, Rank::Eight | Suit::Heart),
				take_from(&mut deck, Rank::Nine | Suit::Club),
				take_from(&mut deck, Rank::King | Suit::Club),
			]
			.into_iter()
			.collect(),
			[].into_iter().collect(),
		]
		.into();
		let pushed = [
			take_from(&mut deck, Rank::Seven | Suit::Diamond),
			take_from(&mut deck, Rank::Eight | Suit::Diamond),
		];
		hands[Player::Third].extend(deck.into_iter());

		PlayingState::new(Player::First, GameType::Hearts, hands, pushed)
	}

	#[test]
	fn test_init_state() {
		let state = build_state();
		assert_eq!(state.last_trick(), None);
		assert_eq!(state.table().len(), 0);
		assert_eq!(state.current_player(), Player::First);
		let won_cards = state.won_cards(Player::First);
		assert_eq!(won_cards.len(), 2);
		assert!(won_cards.contains(&(Rank::Seven | Suit::Diamond)));
		assert!(won_cards.contains(&(Rank::Eight | Suit::Diamond)));

		assert_eq!(state.won_cards(Player::Second).len(), 0);
		assert_eq!(state.won_cards(Player::Third).len(), 0);
	}

	#[test]
	fn test_rejects_non_player() {
		let mut state = build_state();
		match state.play(Player::Second, Rank::Jack | Suit::Diamond) {
			Err(GameError::NotYourTurn) => (),
			other => panic!("unexpected play result: {:?}", other),
		}
		assert_eq!(state.table().len(), 0);
		assert_eq!(state.hand(Player::Second).len(), 10);
	}

	#[test]
	fn test_rejects_non_card() {
		let mut state = build_state();
		match state.play(Player::First, Rank::Jack | Suit::Diamond) {
			Err(GameError::NoSuchCard) => (),
			other => panic!("unexpected play result: {:?}", other),
		}
		assert_eq!(state.table().len(), 0);
		assert_eq!(state.hand(Player::First).len(), 10);
	}

	#[test]
	fn test_play_removes_card_from_hand() {
		let mut state = build_state();
		match state.play(Player::First, Rank::Jack | Suit::Club) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		}
		assert_eq!(state.hand(Player::First).len(), 9);
		assert!(!state
			.hand(Player::First)
			.contains(&(Rank::Jack | Suit::Club)));
	}

	#[test]
	fn test_play_puts_card_on_table() {
		let mut state = build_state();
		match state.play(Player::First, Rank::Jack | Suit::Club) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		}
		assert_eq!(state.table().len(), 1);
		assert_eq!(state.table()[0], Rank::Jack | Suit::Club);
	}

	#[test]
	fn test_play_advances_player() {
		let mut state = build_state();
		match state.play(Player::First, Rank::Jack | Suit::Club) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		}
		assert_eq!(state.current_player(), Player::Second);
	}

	#[test]
	fn test_play_rejects_suit_divergence() {
		let mut state = build_state();
		match state.play(Player::First, Rank::Jack | Suit::Club) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		}
		match state.play(Player::Second, Rank::Ten | Suit::Diamond) {
			Err(GameError::MustFollowSuit) => (),
			other => panic!("unexpected play result: {:?}", other),
		}
	}

	#[test]
	fn test_play_allows_forced_suit_divergence() {
		let mut state = build_state();
		match state.play(Player::First, Rank::Ten | Suit::Spade) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		}
		// second player has absolutely no Spade (and Spade isn't trump)
		match state.play(Player::Second, Rank::Ten | Suit::Diamond) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		}
	}

	#[test]
	fn test_play_complete_trick() {
		let mut state = build_state();
		match state.play(Player::First, Rank::Jack | Suit::Spade) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		};

		match state.play(Player::Second, Rank::Eight | Suit::Heart) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		};
		assert_eq!(state.table().len(), 2);
		assert_eq!(state.table()[0], Rank::Jack | Suit::Spade);
		assert_eq!(state.table()[1], Rank::Eight | Suit::Heart);
		assert_eq!(state.current_player(), Player::Third);
		assert_eq!(state.hand(Player::Second).len(), 9);

		match state.play(Player::Third, Rank::Nine | Suit::Heart) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		};
		assert_eq!(state.table().len(), 0);
		assert_eq!(state.current_player(), Player::First);
		assert_eq!(state.hand(Player::Third).len(), 9);

		let trick = match state.last_trick() {
			Some(v) => v,
			None => panic!("no trick!"),
		};
		assert_eq!(trick[RelativePlayer::Forehand], Rank::Jack | Suit::Spade);
		assert_eq!(trick[RelativePlayer::Midhand], Rank::Eight | Suit::Heart);
		assert_eq!(trick[RelativePlayer::Rearhand], Rank::Nine | Suit::Heart);
		assert_eq!(
			&trick.iter().copied().collect::<Vec<_>>()[..],
			&state.won_cards(Player::First)[2..]
		)
	}

	#[test]
	fn test_play_complete_trick_with_forehandship_change() {
		let mut state = build_state();
		match state.play(Player::First, Rank::Eight | Suit::Club) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		};

		match state.play(Player::Second, Rank::King | Suit::Club) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		};

		match state.play(Player::Third, Rank::Ten | Suit::Club) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		};
		assert_eq!(state.table().len(), 0);
		assert_eq!(state.current_player(), Player::Third);

		let trick = match state.last_trick() {
			Some(v) => v,
			None => panic!("no trick!"),
		};
		assert_eq!(
			&trick.iter().copied().collect::<CardStack>(),
			state.won_cards(Player::Third)
		)
	}

	#[test]
	fn test_play_two_tricks_with_forehandship_change() {
		let mut state = build_state();
		match state.play(Player::First, Rank::Seven | Suit::Heart) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		};

		match state.play(Player::Second, Rank::Eight | Suit::Heart) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		};

		match state.play(Player::Third, Rank::Nine | Suit::Heart) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		};
		assert_eq!(state.current_player(), Player::Third);

		match state.play(Player::Third, Rank::Ten | Suit::Club) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		};

		match state.play(Player::First, Rank::Eight | Suit::Club) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		};

		match state.play(Player::Second, Rank::Ace | Suit::Club) {
			Ok(_) => (),
			other => panic!("unexpected play result: {:?}", other),
		};
		assert_eq!(state.current_player(), Player::Second);
	}

	#[test]
	fn transpose_trick_maps_cards_to_players() {
		let trick: Trick = [
			Rank::Jack | Suit::Club,
			Rank::Jack | Suit::Spade,
			Rank::Jack | Suit::Heart,
		]
		.into();

		let (per_player, prev_forehand) = transpose_trick(GameType::Grand, trick, Player::Third);
		assert_eq!(per_player[Player::Third], Rank::Jack | Suit::Club);
		assert_eq!(per_player[Player::First], Rank::Jack | Suit::Spade);
		assert_eq!(per_player[Player::Second], Rank::Jack | Suit::Heart);
	}
}
