use aes::cipher::{BlockEncrypt, KeyInit};

use ring::digest;

use serde::{Deserialize, Serialize};

use rand::RngCore;

use byteorder::{BigEndian, ByteOrder};

use crate::cards::CardStack;

pub const SHUFFLE_INPUT_SIZE: usize = 16;

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub struct Seed([u8; SHUFFLE_INPUT_SIZE]);

impl Seed {
	pub fn new() -> Self {
		let mut buf = [0u8; SHUFFLE_INPUT_SIZE];
		rand::thread_rng().fill_bytes(&mut buf[..]);
		Self(buf)
	}

	pub const fn from_bytes(raw: [u8; SHUFFLE_INPUT_SIZE]) -> Self {
		Self(raw)
	}
}

impl AsRef<[u8]> for Seed {
	fn as_ref(&self) -> &[u8] {
		&self.0[..]
	}
}

struct ByteSource {
	aes: aes::Aes128,
	state: [u8; 16],
	data: [u8; 16],
	offset: Option<usize>,
	blockctr: u16,
}

impl ByteSource {
	fn make_block(&mut self) -> Option<[u8; 16]> {
		self.blockctr = self.blockctr.checked_add(1)?;
		let mut block = self.state;
		self.aes.encrypt_block((&mut block).into());
		let numeric = BigEndian::read_u128(&self.state[..]);
		BigEndian::write_u128(&mut self.state[..], numeric + 1);
		Some(block)
	}

	fn from_bytes<B: AsRef<[u8]>>(bytes: B) -> Self {
		let bytes = bytes.as_ref();
		assert!(bytes.len() == 32);
		let key = &bytes[..16];
		let iv = &bytes[16..];
		let mut state = [0u8; 16];
		state.copy_from_slice(iv);
		Self {
			aes: aes::Aes128::new_from_slice(key).expect("key size mismatch"),
			state,
			data: [0u8; 16],
			offset: None,
			blockctr: 0,
		}
	}
}

impl Iterator for ByteSource {
	type Item = u8;

	fn next(&mut self) -> Option<Self::Item> {
		match self.offset.as_mut() {
			Some(i) => {
				if *i < self.data.len() {
					let index = *i;
					*i += 1;
					return Some(self.data[index]);
				}
			}
			None => (),
		};
		self.offset = None;
		self.data = self.make_block()?;
		self.offset = Some(1);
		Some(self.data[0])
	}
}

#[derive(Debug, Clone)]
pub enum ShuffleError {
	IncorrectInputSize,
}

pub struct Shuffler {
	ctx: Option<digest::Context>,
}

fn sample<T: Iterator<Item = u8>>(src: &mut T, hi: u8) -> Option<u8> {
	if hi == 0 {
		panic!("cannot return a non-existent number");
	}
	let max = hi - 1;
	let bitmask = match max {
		0 => return Some(0),
		1..=1 => 0b1,
		2..=3 => 0b11,
		4..=7 => 0b111,
		8..=15 => 0b1111,
		16..=31 => 0b1_1111,
		32..=63 => 0b11_1111,
		64..=127 => 0b111_1111,
		128..=255 => 0b1111_1111,
	};
	while let Some(next) = src.next() {
		let value = next & bitmask;
		if value <= max {
			return Some(value);
		}
	}
	return None;
}

fn shuffle_from_bytes<T: Iterator<Item = u8>, X>(src: &mut T, stack: &mut [X]) {
	assert!(stack.len() <= 32);
	let len = stack.len();
	for i in 0..len {
		let remaining = len - i;
		assert!(remaining <= u8::MAX as usize);
		let source_index = i + sample(src, remaining as u8).expect("end of randomness") as usize;
		stack.swap(i, source_index);
	}
}

impl Shuffler {
	pub fn new() -> Self {
		Self { ctx: None }
	}

	pub fn feed(&mut self, data: &[u8]) -> Result<(), ShuffleError> {
		if data.len() != SHUFFLE_INPUT_SIZE {
			return Err(ShuffleError::IncorrectInputSize);
		}
		let ctx = match self.ctx.as_mut() {
			None => self.ctx.insert(digest::Context::new(&digest::SHA256)),
			Some(v) => v,
		};
		ctx.update(data);
		Ok(())
	}

	pub fn shuffle(mut self, stack: &mut CardStack) {
		let ctx = self.ctx.take().expect("Shuffler not seeded");
		let mut bytes = ByteSource::from_bytes(ctx.finish());
		shuffle_from_bytes(&mut bytes, stack);
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	use crate::cards::{full_deck, Rank, Suit};

	#[test]
	fn basic_shuffle_variant_1() {
		let mut deck = full_deck();
		assert_eq!(deck[0], Suit::Diamond | Rank::Seven,);
		let mut shuffler = Shuffler::new();
		shuffler.feed(&[0; 16][..]).expect("feed ok");
		shuffler.shuffle(&mut deck);
		assert_eq!(deck[0], Suit::Diamond | Rank::Queen,);
	}

	#[test]
	fn basic_shuffle_variant_2() {
		let mut deck = full_deck();
		assert_eq!(deck[0], Suit::Diamond | Rank::Seven,);
		let mut shuffler = Shuffler::new();
		shuffler.feed(&[1; 16][..]).expect("feed ok");
		shuffler.shuffle(&mut deck);
		assert_eq!(deck[0], Suit::Spade | Rank::King,);
	}
}
