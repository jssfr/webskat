use std::time::Duration;

use reqwest::{Client, StatusCode};

use serde::Serialize;

use clap::Parser;

use skat::{
	bot::{Declaration, HandDeclaration, HandDeclarationStep, PimcPlayer},
	Commitment, PerPlayer, PlaySkat, Player, Seed, State,
};

use webskat_common::{PlayRequest, PlayResponse};

#[derive(Parser, Debug)]
struct Args {
	#[command(subcommand)]
	command: Command,
	#[arg(long)]
	insecure_seed: bool,
}

#[derive(clap::Subcommand, Debug)]
enum Command {
	Join {
		join_url: String,
		join_key: String,
		#[arg(short, long)]
		name: Option<String>,
	},
	Resume {
		play_url: String,
	},
}

async fn play_move(
	client: &Client,
	play_url: &str,
	req: &PlayRequest,
) -> reqwest::Result<PlayResponse> {
	Ok(client
		.post(play_url)
		.json(&req)
		.send()
		.await?
		.json()
		.await?)
}

async fn poll_state(client: &Client, play_url: &str) -> reqwest::Result<PlayResponse> {
	play_move(client, play_url, &PlayRequest::PollState).await
}

#[derive(thiserror::Error, Debug)]
enum JoinError {
	#[error("invalid join key")]
	InvalidJoinKey,
	#[error("no seat left at table")]
	NoSeatLeft,
	#[error("no such table")]
	NoSuchTable,
	#[error("missing play url")]
	MissingPlayUrl,
	#[error("unexpected status code: {0}")]
	UnexpectedStatusCode(reqwest::StatusCode),
	#[error("request failed: {0}")]
	RequestError(#[from] reqwest::Error),
}

async fn join_game(
	client: &Client,
	join_url: &str,
	join_key: &str,
	name: &str,
) -> Result<String, JoinError> {
	#[derive(Serialize)]
	struct JoinForm<'x> {
		join_key: &'x str,
		name: &'x str,
	}

	let response = client
		.post(join_url)
		.form(&JoinForm { join_key, name })
		.send()
		.await?;
	match response.status() {
		StatusCode::SEE_OTHER => {
			// success!
			let play_path = match response
				.headers()
				.get("location")
				.and_then(|x| x.to_str().ok())
			{
				None => return Err(JoinError::MissingPlayUrl),
				Some(path) => path,
			};
			let table_path = join_url.rsplit_once('/').expect("malformed table URL").0;
			Ok(format!("{}/{}", table_path, play_path))
		}
		StatusCode::NOT_FOUND => Err(JoinError::NoSuchTable),
		StatusCode::UNAUTHORIZED => Err(JoinError::InvalidJoinKey),
		StatusCode::CONFLICT => Err(JoinError::NoSeatLeft),
		other => Err(JoinError::UnexpectedStatusCode(other)),
	}
}

#[derive(thiserror::Error, Debug)]
enum PlayError {
	#[error("error in reply to action request: {0}")]
	ServerError(String),
	#[error("dealer changed the commitment mid-game")]
	DealerCommitmentChanged,
	#[error("player {0:?} changed the commitment mid-game")]
	PlayerCommitmentChanged(Player),
	#[error("{0}")]
	Player(skat::bot::PlayError),
}

impl From<skat::bot::PlayError> for PlayError {
	fn from(other: skat::bot::PlayError) -> Self {
		Self::Player(other)
	}
}

#[derive(Debug)]
enum Action {
	Delay(Duration),
	SendRequest(PlayRequest),
}

struct SkatPlayer<T> {
	seed: Seed,
	observed_dealer_commitment: Option<Commitment>,
	observed_player_commitments: PerPlayer<Option<Commitment>>,
	observed_player_seeds: PerPlayer<Option<Seed>>,
	inner: T,
}

impl<T: PlaySkat> SkatPlayer<T> {
	fn new(insecure_seed: bool, inner: T) -> Self {
		let seed = if insecure_seed {
			Seed::from_bytes([0u8; 16])
		} else {
			Seed::new()
		};
		Self {
			seed,
			observed_dealer_commitment: None,
			observed_player_commitments: PerPlayer::default(),
			observed_player_seeds: PerPlayer::default(),
			inner,
		}
	}

	fn check_dealer_commitment(&mut self, observation: &Commitment) -> Result<(), PlayError> {
		if let Some(previous) = self.observed_dealer_commitment.as_ref() {
			if previous != observation {
				Err(PlayError::DealerCommitmentChanged)
			} else {
				Ok(())
			}
		} else {
			self.observed_dealer_commitment = Some(observation.clone());
			Ok(())
		}
	}

	fn check_player_commitments(
		&mut self,
		observation: &PerPlayer<Option<&Commitment>>,
	) -> Result<(), PlayError> {
		for ((i, previous), current) in self
			.observed_player_commitments
			.enumerate_mut()
			.zip(observation.iter())
		{
			if previous.is_none() && current.is_some() {
				*previous = current.cloned();
			}
			if previous.is_some() && current.is_none() {
				// this can happen while we haven't sent our commitment to the server yet. as we still don't allow resetting of seeds, it's ok.
				continue;
			}
			if previous.as_ref() != *current {
				return Err(PlayError::PlayerCommitmentChanged(i));
			}
		}
		Ok(())
	}

	fn check_player_seeds(
		&mut self,
		observation: &PerPlayer<Option<&Seed>>,
		require_commitments: bool,
	) -> Result<(), PlayError> {
		for ((i, previous), current) in self
			.observed_player_seeds
			.enumerate_mut()
			.zip(observation.iter())
		{
			if previous.is_none() && current.is_some() {
				// we need to check against the commitment here
				let current = current.unwrap().clone();
				if require_commitments {
					let commitment = self.observed_player_commitments[i].unwrap();
					if commitment != Commitment::from_seed(&current) {
						return Err(PlayError::PlayerCommitmentChanged(i));
					}
				}
				*previous = Some(current);
			}
			if previous.is_some() && current.is_none() {
				// this can happen while we haven't sent our commitment to the server yet. as we still don't allow resetting of seeds, it's ok.
				continue;
			}
			if previous.as_ref() != *current {
				return Err(PlayError::PlayerCommitmentChanged(i));
			}
		}
		Ok(())
	}

	fn handle_play_state(&mut self, state: &State) -> Result<Action, PlayError> {
		match state {
			State::Committing {
				your_index,
				player_commitments,
				dealer_commitment,
			} => {
				// We need to set this first in order to not spoil our observations with whatever the server told us.
				if self.observed_player_commitments[*your_index].is_none() {
					let commitment = Commitment::from_seed(&self.seed);
					self.observed_player_commitments[*your_index] = Some(commitment);
				}
				self.check_dealer_commitment(dealer_commitment)?;
				self.check_player_commitments(&player_commitments.map_ref(|x| x.as_ref()))?;
				if player_commitments[*your_index].is_none() {
					let commitment = Commitment::from_seed(&self.seed);
					self.observed_player_commitments[*your_index] = Some(commitment.clone());
					Ok(Action::SendRequest(PlayRequest::CommitSeed { commitment }))
				} else {
					Ok(Action::Delay(Duration::new(1, 0)))
				}
			}
			State::Shuffling {
				your_index,
				player_commitments,
				player_seeds,
				dealer_commitment,
			} => {
				// We need to set this first in order to not spoil our observations with whatever the server told us.
				if self.observed_player_seeds[*your_index].is_none() {
					self.observed_player_seeds[*your_index] = Some(self.seed.clone());
				}
				self.check_dealer_commitment(dealer_commitment)?;
				self.check_player_commitments(&player_commitments.map_ref(|x| Some(x)))?;
				self.check_player_seeds(&player_seeds.map_ref(|x| x.as_ref()), true)?;
				if player_seeds[*your_index].is_none() {
					Ok(Action::SendRequest(PlayRequest::SetSeed {
						seed: self.seed,
					}))
				} else {
					Ok(Action::Delay(Duration::new(1, 0)))
				}
			}
			State::Bidding {
				your_index,
				player_seeds,
				player_bids,
				caller,
				responder,
				awaiting_response,
				your_hand,
				hands: _,
				dealer_commitment,
			} => {
				self.check_dealer_commitment(dealer_commitment)?;
				// commitments may be missing here if we're resuming in this state
				self.check_player_seeds(&player_seeds.map_ref(|x| Some(x)), false)?;
				if caller == your_index && !*awaiting_response {
					Ok(Action::SendRequest(PlayRequest::CallBid {
						bid: self.inner.call_bid(*your_index, *your_hand, *player_bids)?,
					}))
				} else if *responder == Some(*your_index) && *awaiting_response {
					Ok(Action::SendRequest(PlayRequest::RespondBid {
						hold: self.inner.respond_bid(
							*your_index,
							*your_hand,
							*player_bids,
							*caller,
						)?,
					}))
				} else {
					Ok(Action::Delay(Duration::new(1, 0)))
				}
			}
			State::PreDeclaration {
				your_index,
				declarer,
				called_game_value,
				your_hand,
				hands: _,
				skat_taken,
			} => {
				if declarer == your_index {
					let action = if *skat_taken {
						let Declaration {
							game_type,
							push_cards,
						} = self
							.inner
							.declare(*your_index, *your_hand, *called_game_value)?;
						PlayRequest::Declare {
							game_type,
							modifiers: None,
							push_cards: Some(push_cards),
						}
					} else {
						match self.inner.declare_hand(
							*your_index,
							*your_hand,
							*called_game_value,
						)? {
							HandDeclarationStep::Declare(HandDeclaration {
								game_type,
								modifiers,
							}) => PlayRequest::Declare {
								game_type,
								modifiers,
								push_cards: None,
							},
							HandDeclarationStep::TakeSkat => PlayRequest::TakeSkat,
						}
					};
					Ok(Action::SendRequest(action))
				} else {
					Ok(Action::Delay(Duration::new(1, 0)))
				}
			}
			State::Playing {
				your_index,
				declarer,
				turn,
				declarer_hand,
				your_hand,
				hands,
				game_type,
				last_trick,
				base_modifiers,
				table,
				forehand,
			} => {
				if your_index == turn {
					let card = self.inner.play(
						*your_index,
						*your_hand,
						*hands,
						*declarer,
						*forehand,
						*declarer_hand,
						*game_type,
						*base_modifiers,
						*table,
						*last_trick,
					)?;
					Ok(Action::SendRequest(PlayRequest::Play { card }))
				} else {
					Ok(Action::Delay(Duration::new(1, 0)))
				}
			}
			State::Concluded { .. } | State::PassedOut { .. } => {
				Ok(Action::Delay(Duration::new(1, 0)))
			}
		}
	}
}

#[tokio::main]
async fn main() -> std::result::Result<(), Box<dyn std::error::Error>> {
	env_logger::init();
	let args = Args::parse();
	let client = reqwest::Client::builder()
		.redirect(reqwest::redirect::Policy::none())
		.build()?;
	let play_url = match args.command {
		Command::Resume { play_url } => play_url,
		Command::Join {
			join_url,
			join_key,
			name,
		} => {
			let name = name.unwrap_or_else(|| format!("bot {}", std::process::id()));
			log::debug!("trying to join game at {:?} with name {:?}", join_url, name);
			let play_url = join_game(&client, &join_url, &join_key, &name).await?;
			log::debug!("successfully joined game as {:?}", name);
			play_url
		}
	};
	log::info!("using play URL: {:?}", play_url);
	let mut state = poll_state(&client, &play_url).await?;
	let mut game_index = match state {
		PlayResponse::Lobby { .. } | PlayResponse::Error { .. } => None,
		// TODO: if games are not logged, this counting does not work.'
		PlayResponse::Playing { ref past_games, .. } => past_games.as_ref().map(|x| x.len()),
	};
	let mut player = SkatPlayer::new(args.insecure_seed, PimcPlayer::new());
	loop {
		log::trace!("current state: {:?}", state);
		let action = match state {
			PlayResponse::Lobby { .. } => {
				log::trace!("still in lobby, polling for next state...");
				Action::Delay(Duration::new(1, 0))
			}
			PlayResponse::Playing {
				ref game,
				ref past_games,
				..
			} => {
				let new_game_index = past_games.as_ref().map(|x| x.len());
				if game_index != new_game_index {
					log::info!("starting new game!");
					player = SkatPlayer::new(args.insecure_seed, PimcPlayer::new());
					game_index = new_game_index;
				};
				player.handle_play_state(&game)?
			}
			PlayResponse::Error { message } => {
				log::error!("error reply from server: {:?}", message);
				return Err(Box::new(PlayError::ServerError(message)) as _);
			}
		};
		log::trace!("action: {:?}", action);
		match action {
			Action::Delay(t) => {
				tokio::time::sleep(t).await;
				state = poll_state(&client, &play_url).await?;
			}
			Action::SendRequest(req) => {
				let new_state = play_move(&client, &play_url, &req).await?;
				match new_state {
					PlayResponse::Error { message } => {
						log::error!(
							"error reply from server in response to request {:?}: {:?}",
							req,
							message
						);
						return Err(Box::new(PlayError::ServerError(message)) as _);
					}
					_ => (),
				}
				state = new_state;
			}
		};
	}
}
