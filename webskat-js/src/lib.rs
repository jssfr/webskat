use wasm_bindgen::prelude::*;

use wasm_bindgen::JsError;

use js_sys::Array;

use wasm_bindgen_futures::JsFuture;

use skat::bidding::get_next_bid;
use skat::{
	AnnouncableModifier, Card, CardSet, CardStack, Commitment, GameType, GameValue, Matadors,
	Modifiers, PerPlayer, Player, Rank, RelativePlayer, Seed, State, Suit, Trick,
};

use webskat_common::{GameSummary, PerSeat, PlayRequest, PlayResponse};

use web_sys::{
	Document, Element, HtmlElement, HtmlInputElement, HtmlLabelElement, HtmlTableCellElement,
	Request, RequestInit, RequestMode, Response,
};

const ID_SCOREBOARD: &'static str = "scoreboard";
const ID_SCOREBOARD_SPACER: &'static str = "scoreboard-spacer";
const ID_SCOREBOARD_BODY: &'static str = "score-table-body";

const ID_LOBBY_SEAT_LIST: &'static str = "lobby-seats";

const ID_TABLE_OPPONENT_NAME_LEFT: &'static str = "opponent-name-left";
const ID_TABLE_OPPONENT_NAME_RIGHT: &'static str = "opponent-name-right";
const ID_TABLE_OPPONENT_OFFER_LEFT: &'static str = "opponent-offer-left";
const ID_TABLE_OPPONENT_OFFER_RIGHT: &'static str = "opponent-offer-right";
const ID_TABLE_OPPONENT_HAND_LEFT: &'static str = "opponent-hand-left";
const ID_TABLE_OPPONENT_HAND_RIGHT: &'static str = "opponent-hand-right";
const ID_TABLE_YOUR_HAND: &'static str = "your-hand";
const ID_TABLE_BID_UI: &'static str = "bid-ui";
const ID_TABLE_BIDDING_STATE_TEXT: &'static str = "state-text";
const ID_TABLE_BIDDING_CALL_INPUT: &'static str = "bid-call-input";
const ID_TABLE_SKAT: &'static str = "skat";
const ID_TABLE_DECLARE_UI: &'static str = "declare-ui";
const ID_TABLE_DECLARING_STATE_TEXT: &'static str = "state-text";
const ID_TABLE_PLAYING_STATE_TEXT: &'static str = "state-text";
const ID_TABLE_SPACE: &'static str = "table";
const ID_TABLE_LAST_TRICK: &'static str = "last-trick";

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum TableSeat {
	Own,
	Left,
	Right,
}

impl TableSeat {
	fn hand_id(&self) -> &'static str {
		match self {
			Self::Own => ID_TABLE_YOUR_HAND,
			Self::Left => ID_TABLE_OPPONENT_HAND_LEFT,
			Self::Right => ID_TABLE_OPPONENT_HAND_RIGHT,
		}
	}

	fn require_hand_el(&self, document: &Document) -> Result<Element, RenderError> {
		require_element_by_id(document, self.hand_id())
	}
}

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[derive(thiserror::Error, Debug)]
enum ApiError {
	#[error("generic javascript error: {0:?}")]
	GenericError(JsValue),
	#[error("unexpected status code: {0}")]
	UnexpectedStatusCode(u16),
	#[error("failed to obtain user agent window")]
	NoWindow,
	#[error("JSON structuring error: {0}")]
	JsonStructuringError(#[from] serde_wasm_bindgen::Error),
	#[error("JSON serde error: {0}")]
	JsonSerdeError(#[from] serde_json::Error),
}

impl From<JsValue> for ApiError {
	fn from(other: JsValue) -> Self {
		Self::GenericError(other)
	}
}

#[wasm_bindgen(start)]
pub fn init() {
	std::panic::set_hook(Box::new(console_error_panic_hook::hook));
	let _: Result<_, _> = console_log::init();
	log::info!("Hello from WASM!");
}

async fn send_request(play_api: &str, req: &PlayRequest) -> Result<PlayResponse, ApiError> {
	let mut opts = RequestInit::new();
	opts.method("POST");
	opts.mode(RequestMode::Cors);
	opts.body(Some(&JsValue::from_str(&serde_json::to_string(req)?)));

	let req = Request::new_with_str_and_init(play_api, &opts)?;
	let _: Result<_, _> = req.headers().set("Accept", "application/json");
	let _: Result<_, _> = req.headers().set("Content-Type", "application/json");

	let window = web_sys::window().ok_or(ApiError::NoWindow)?;
	let resp = JsFuture::from(window.fetch_with_request(&req)).await?;
	assert!(resp.is_instance_of::<Response>());
	let resp: Response = resp.dyn_into()?;
	match resp.status() {
		200 | 400 => {
			let json = JsFuture::from(resp.json()?).await?;
			Ok(serde_wasm_bindgen::from_value(json)?)
		}
		other => Err(ApiError::UnexpectedStatusCode(other)),
	}
}

#[derive(thiserror::Error, Debug)]
enum RenderError {
	#[error("generic javascript error: {0:?}")]
	GenericError(JsValue),
	#[error("required element not found by ID: {0}")]
	ElementNotFound(&'static str),
}

impl From<JsValue> for RenderError {
	fn from(other: JsValue) -> Self {
		Self::GenericError(other)
	}
}

fn require_element_by_id(doc: &Document, id: &'static str) -> Result<Element, RenderError> {
	doc.get_element_by_id(id)
		.ok_or(RenderError::ElementNotFound(id))
}

#[derive(Debug)]
enum BidState {
	Passive,
	MustCall { next_in_sequence: u16 },
	MustRespond,
}

#[derive(Debug)]
enum DeclareState {
	Passive { declarer: Player },
	Active,
}

#[derive(Debug)]
enum PlayState {
	Passive { turn: Player },
	Active,
}

#[derive(Debug)]
#[allow(dead_code)]
enum TableView {
	Bidding {
		player_bids: PerPlayer<Option<GameValue>>,
		caller: Player,
		responder: Option<Player>,
		awaiting_response: bool,
		state: BidState,
	},
	Declaring {
		called_game_value: GameValue,
		skat_taken: bool,
		state: DeclareState,
	},
	Playing {
		game_type: GameType,
		declarer: Player,
		declarer_hand: Option<CardSet>,
		last_trick: Option<Trick>,
		base_modifiers: Modifiers,
		table: CardStack,
		forehand: Player,
		state: PlayState,
	},
}

#[derive(Debug)]
#[allow(dead_code)]
enum GameView<'x> {
	Lobby {
		seats: PerSeat<Option<&'x str>>,
	},
	Shuffling {
		your_index: Player,
		seats: PerSeat<&'x str>,
		players: PerPlayer<&'x str>,
		past_games: Option<&'x Vec<Option<GameSummary>>>,
	},
	Table {
		your_index: Player,
		seats: PerSeat<&'x str>,
		players: PerPlayer<&'x str>,
		past_games: Option<&'x Vec<Option<GameSummary>>>,
		your_hand: CardSet,
		hands: PerPlayer<u8>,
		table: TableView,
	},
	Concluded {
		your_index: Player,
		seats: PerSeat<&'x str>,
		players: PerPlayer<&'x str>,
		past_games: Option<&'x Vec<Option<GameSummary>>>,
		summary: Option<skat::GameSummary>,
	},
}

enum CardControl<'x> {
	Passive,
	SingleSelect {
		onclick: Option<&'x js_sys::Function>,
	},
	MultiSelect,
}

impl<'x> CardControl<'x> {
	fn set_card_content(parent: &Element, card: Card) -> Result<(), RenderError> {
		let (rank, suit) = card.split();
		let suit_offset = 0x1f0a0 + (((((suit as u32) ^ 0x03) + 3) % 4) << 4);
		let mut rank_offset = (rank as u32) + 7;
		if rank == Rank::Ace {
			rank_offset = 1;
		} else if rank == Rank::Queen || rank == Rank::King {
			rank_offset += 1;
		}
		let ch = char::from_u32(suit_offset + rank_offset).expect("invalid codepoint generated");
		let mut buf = String::with_capacity(4);
		buf.push(ch);
		parent.set_text_content(Some(buf.as_str()));
		Ok(())
	}

	fn render(&self, parent: &Element, card: Card) -> Result<(), RenderError> {
		let (type_, onclick) = match self {
			Self::Passive => return Self::set_card_content(parent, card),
			Self::MultiSelect => ("checkbox", None),
			Self::SingleSelect { onclick } => ("radio", *onclick),
		};
		let document = parent.owner_document().unwrap();
		let el_id = format!("card-{}", card.bits());

		let card_el: HtmlLabelElement = document.create_element("label")?.dyn_into().unwrap();
		Self::set_card_content(&card_el, card)?;
		card_el.set_html_for(&el_id);

		let input_el: HtmlInputElement = document.create_element("input")?.dyn_into().unwrap();
		input_el.set_id(&el_id);
		input_el.set_type(type_);
		input_el.set_class_name("card-input");
		if type_ == "radio" {
			input_el.set_name("card");
		}
		input_el.set_onclick(onclick);
		input_el.dataset().set("card", &card.bits().to_string())?;

		parent.append_child(&input_el)?;
		parent.append_child(&card_el)?;
		Ok(())
	}
}

impl<'x> GameView<'x> {
	fn clear_states(body: &HtmlElement) -> Result<(), RenderError> {
		let classes = body.class_list();
		classes.remove_6(
			"lobby",
			"shuffling",
			"bidding",
			"table",
			"declaring",
			"playing",
		)?;
		Ok(())
	}

	fn from_response(response: &'x PlayResponse) -> Self {
		match response {
			PlayResponse::Error { message } => panic!("cannot render erroneous state: {}", message),
			PlayResponse::Lobby { seats } => Self::Lobby {
				seats: seats.map_ref(|x| x.as_ref().map(|x| x.as_str())),
			},
			PlayResponse::Playing {
				seats,
				players,
				past_games,
				game,
			} => match game {
				State::Committing { your_index, .. } | State::Shuffling { your_index, .. } => {
					GameView::Shuffling {
						your_index: *your_index,
						seats: seats.map_ref(|x| x.as_str()),
						players: players.map_ref(|x| x.as_str()),
						past_games: past_games.as_ref(),
					}
				}
				State::Bidding {
					your_index,
					player_bids,
					caller,
					responder,
					awaiting_response,
					your_hand,
					hands,
					..
				} => GameView::Table {
					your_index: *your_index,
					seats: seats.map_ref(|x| x.as_str()),
					players: players.map_ref(|x| x.as_str()),
					past_games: past_games.as_ref(),
					your_hand: *your_hand,
					hands: *hands,
					table: TableView::Bidding {
						player_bids: *player_bids,
						caller: *caller,
						responder: *responder,
						awaiting_response: *awaiting_response,
						state: if *your_index == *caller && !*awaiting_response {
							let max = player_bids
								.into_iter()
								.filter_map(|x| *x)
								.max()
								.unwrap_or(0) as u16;
							BidState::MustCall {
								next_in_sequence: get_next_bid(max).unwrap_or(max + 1),
							}
						} else if *awaiting_response && Some(*your_index) == *responder {
							BidState::MustRespond
						} else {
							BidState::Passive
						},
					},
				},
				State::PreDeclaration {
					your_index,
					declarer,
					called_game_value,
					your_hand,
					hands,
					skat_taken,
				} => GameView::Table {
					your_index: *your_index,
					seats: seats.map_ref(|x| x.as_str()),
					players: players.map_ref(|x| x.as_str()),
					past_games: past_games.as_ref(),
					your_hand: *your_hand,
					hands: *hands,
					table: TableView::Declaring {
						called_game_value: *called_game_value,
						skat_taken: *skat_taken,
						state: if declarer == your_index {
							DeclareState::Active
						} else {
							DeclareState::Passive {
								declarer: *declarer,
							}
						},
					},
				},
				State::Playing {
					your_index,
					declarer,
					turn,
					declarer_hand,
					your_hand,
					hands,
					game_type,
					last_trick,
					base_modifiers,
					table,
					forehand,
				} => GameView::Table {
					your_index: *your_index,
					seats: seats.map_ref(|x| x.as_str()),
					players: players.map_ref(|x| x.as_str()),
					past_games: past_games.as_ref(),
					your_hand: *your_hand,
					hands: *hands,
					table: TableView::Playing {
						game_type: *game_type,
						declarer_hand: *declarer_hand,
						last_trick: *last_trick,
						base_modifiers: *base_modifiers,
						table: *table,
						forehand: *forehand,
						declarer: *declarer,
						state: if *turn == *your_index {
							PlayState::Active
						} else {
							PlayState::Passive { turn: *turn }
						},
					},
				},
				State::Concluded {
					your_index,
					summary,
					..
				} => GameView::Concluded {
					your_index: *your_index,
					seats: seats.map_ref(|x| x.as_str()),
					players: players.map_ref(|x| x.as_str()),
					past_games: past_games.as_ref(),
					summary: Some(*summary),
				},
				State::PassedOut { your_index, .. } => GameView::Concluded {
					your_index: *your_index,
					seats: seats.map_ref(|x| x.as_str()),
					players: players.map_ref(|x| x.as_str()),
					past_games: past_games.as_ref(),
					summary: None,
				},
			},
		}
	}

	fn map_opponent(my_index: Player, other: Player) -> TableSeat {
		match RelativePlayer::from_absolute(my_index, other) {
			RelativePlayer::Forehand => TableSeat::Own,
			RelativePlayer::Midhand => TableSeat::Left,
			RelativePlayer::Rearhand => TableSeat::Right,
		}
	}

	fn split_opponents_visually<T>(my_index: Player, data: &'_ PerPlayer<T>) -> (&T, &T, &T) {
		let own = &data[my_index];
		let lhs = &data[RelativePlayer::Midhand.into_absolute(my_index)];
		let rhs = &data[RelativePlayer::Rearhand.into_absolute(my_index)];
		(own, lhs, rhs)
	}

	fn render_covered_hand(parent: &Element, cards: u8) -> Result<(), RenderError> {
		parent.class_list().add_1("covered")?;
		if cards == 0 {
			parent.replace_children_with_node_0();
			return Ok(());
		}
		let ref_el = parent.owner_document().unwrap().create_element("li")?;
		ref_el.set_text_content(Some("\u{1f0a0}"));
		let children = Array::new();
		for _ in 0..(cards - 1) {
			children.push(&ref_el.clone_node_with_deep(true)?.into());
		}
		children.push(&ref_el.into());
		parent.replace_children_with_node(&children);
		Ok(())
	}

	fn sort_cards(cards: &mut [Card], game_type: GameType) {
		cards.sort_by_key(|c| (game_type.effective_suit(&c), game_type.power(&c)))
	}

	fn render_hand<T: IntoIterator<Item = Card>>(
		parent: &Element,
		cards: T,
		game_type: Option<GameType>,
		control: CardControl,
	) -> Result<(), RenderError> {
		parent.class_list().remove_1("covered")?;
		let document = parent.owner_document().unwrap();
		let ref_el = document.create_element("li")?;
		let children = Array::new();
		let mut cards: CardStack = cards.into_iter().collect();
		if let Some(game_type) = game_type {
			Self::sort_cards(&mut cards, game_type);
		}
		let mut prev_suit = None;
		for card in cards {
			let el: Element = ref_el.clone_node()?.dyn_into().expect("not an Element");
			let class_list = el.class_list();
			if let Some(game_type) = game_type {
				let effective_suit = game_type.effective_suit(&card);
				if prev_suit != Some(effective_suit) {
					prev_suit = Some(effective_suit);
					class_list.add_1("brk")?;
				}
			}
			match card.suit() {
				Suit::Diamond => class_list.add_1("suit-diamond")?,
				Suit::Heart => class_list.add_1("suit-heart")?,
				Suit::Spade => class_list.add_1("suit-spade")?,
				Suit::Club => class_list.add_1("suit-club")?,
			}
			control.render(&el, card)?;
			children.push(&el.into());
		}
		parent.replace_children_with_node(&children);
		Ok(())
	}

	fn format_bid(bid: Option<GameValue>) -> String {
		// TODO: i18n
		match bid {
			Some(v) => format!(" ({})", v),
			None => "".to_string(),
		}
	}

	fn format_game_type_fancy_into(
		parent: &Element,
		game_type: GameType,
	) -> Result<(), RenderError> {
		let (abbr, title) = match game_type {
			GameType::Null => ("N", "Null"),
			GameType::Grand => ("G", "Grand"),
			GameType::Diamonds => ("♦", "Diamonds"),
			GameType::Hearts => ("♥", "Hearts"),
			GameType::Spades => ("♠", "Spades"),
			GameType::Clubs => ("♣", "Clubs"),
		};
		let abbr_el: HtmlElement = parent
			.owner_document()
			.unwrap()
			.create_element("abbr")?
			.dyn_into()
			.unwrap();
		abbr_el.set_title(title);
		abbr_el.set_text_content(Some(abbr));
		parent.append_child(&abbr_el)?;
		Ok(())
	}

	fn format_matadors_fancy_into(parent: &Element, matadors: Matadors) -> Result<(), RenderError> {
		let text = match matadors {
			Matadors::With(count) => format!("w/ {}", count),
			Matadors::Without(count) => format!("w/o {}", count),
		};
		parent.set_text_content(Some(&text));
		Ok(())
	}

	fn fill_past_games(
		document: &Document,
		seats: PerSeat<&str>,
		past_games: Option<&Vec<Option<GameSummary>>>,
	) -> Result<(), RenderError> {
		let scoreboard_root = require_element_by_id(document, ID_SCOREBOARD)?;
		let scoreboard_spacer = require_element_by_id(document, ID_SCOREBOARD_SPACER)?;
		if let Some(past_games) = past_games {
			scoreboard_root.set_class_name("");
			scoreboard_spacer.set_class_name("");

			for (i, seat) in seats.iter().enumerate() {
				match document.get_element_by_id(&format!("score-head-{}", i)) {
					Some(v) => v.set_text_content(Some(seat)),
					None => (),
				};
			}

			let scoreboard_body = require_element_by_id(document, ID_SCOREBOARD_BODY)?;
			let scoreboard_rows = scoreboard_body.children();
			let mut tally: PerSeat<i32> = PerSeat::default();
			for (i, game) in past_games.into_iter().enumerate() {
				if let Some(game) = game {
					for (seat, score) in game.scores.enumerate() {
						tally[seat] += score;
					}
				}

				if scoreboard_rows.get_with_index(i as u32).is_some() {
					continue;
				}

				let tr = document.create_element("tr")?;
				if let Some(game) = game {
					let td: HtmlTableCellElement =
						document.create_element("td")?.dyn_into().unwrap();
					Self::format_game_type_fancy_into(&td, game.game_type)?;
					td.set_class_name("desc");
					tr.append_child(&td)?;

					if game.game_type == GameType::Null {
						td.set_col_span(2);
					} else {
						let td: HtmlTableCellElement =
							document.create_element("td")?.dyn_into().unwrap();
						Self::format_matadors_fancy_into(&td, game.matadors)?;
						td.set_class_name("desc");
						tr.append_child(&td)?;
					}

					let td: HtmlTableCellElement =
						document.create_element("td")?.dyn_into().unwrap();
					td.set_text_content(Some(&game.game_value.to_string()));
					td.set_class_name("desc");
					tr.append_child(&td)?;

					for score in game.scores.into_iter() {
						let td: HtmlTableCellElement =
							document.create_element("td")?.dyn_into().unwrap();
						td.set_text_content(Some(&score.to_string()));
						tr.append_child(&td)?;
					}
				} else {
					let td: HtmlTableCellElement =
						document.create_element("td")?.dyn_into().unwrap();
					td.set_text_content(Some("passed out"));
					td.set_col_span(6);
					tr.set_class_name("game-passed-out");
					tr.append_child(&td)?;
				}
				scoreboard_body.append_child(&tr)?;
			}

			for (i, score) in tally.into_iter().enumerate() {
				document
					.get_element_by_id(&format!("score-foot-{}", i))
					.unwrap()
					.set_text_content(Some(&score.to_string()));
			}
		} else {
			scoreboard_root.set_class_name("hidden");
			scoreboard_spacer.set_class_name("hidden");
		}
		Ok(())
	}

	fn render_into_web(self) -> Result<bool, RenderError> {
		let window = web_sys::window().expect("window object");
		let document = window.document().expect("document");
		let body = document.body().expect("body");

		Self::clear_states(&body)?;
		match self {
			Self::Lobby { seats } => {
				body.class_list().add_1("lobby")?;
				let seats_ul = require_element_by_id(&document, ID_LOBBY_SEAT_LIST)?;
				let seat_children = seats_ul.children();
				for (i, name) in seats.into_iter().enumerate() {
					let name = match name {
						Some(v) => v,
						None => continue,
					};
					match seat_children.get_with_index(i as u32) {
						Some(el) => {
							el.set_text_content(Some(name));
						}
						None => {
							let el = document.create_element("li")?;
							el.set_text_content(Some(name));
							seats_ul.append_child(&el)?;
						}
					}
				}
				Ok(true)
			}
			Self::Shuffling { .. } => {
				body.class_list().add_1("shuffling")?;
				Ok(true)
			}
			Self::Table {
				seats,
				players,
				your_index,
				past_games,
				your_hand,
				hands,
				table,
			} => {
				body.class_list().add_1("table")?;

				Self::fill_past_games(&document, seats, past_games)?;

				let names = Self::split_opponents_visually(your_index, &players);
				require_element_by_id(&document, ID_TABLE_OPPONENT_NAME_LEFT)?
					.set_text_content(Some(names.1));
				require_element_by_id(&document, ID_TABLE_OPPONENT_NAME_RIGHT)?
					.set_text_content(Some(names.2));
				match table {
					TableView::Bidding {
						player_bids,
						caller,
						responder,
						awaiting_response,
						state,
					} => {
						body.class_list().add_1("bidding")?;
						Self::render_covered_hand(
							&require_element_by_id(&document, ID_TABLE_SPACE)?,
							0,
						)?;
						Self::render_covered_hand(
							&require_element_by_id(&document, ID_TABLE_SKAT)?,
							2,
						)?;

						let hands = Self::split_opponents_visually(your_index, &hands);
						Self::render_covered_hand(
							&TableSeat::Left.require_hand_el(&document)?,
							*hands.1,
						)?;
						Self::render_covered_hand(
							&TableSeat::Right.require_hand_el(&document)?,
							*hands.2,
						)?;
						Self::render_hand(
							&TableSeat::Own.require_hand_el(&document)?,
							your_hand,
							Some(GameType::Grand),
							CardControl::Passive,
						)?;

						let bid_ui = require_element_by_id(&document, ID_TABLE_BID_UI)?;

						let bids = Self::split_opponents_visually(your_index, &player_bids);
						require_element_by_id(&document, ID_TABLE_OPPONENT_OFFER_LEFT)?
							.set_text_content(Some(&Self::format_bid(*bids.1)));
						require_element_by_id(&document, ID_TABLE_OPPONENT_OFFER_RIGHT)?
							.set_text_content(Some(&Self::format_bid(*bids.2)));

						let bid_class_list = bid_ui.class_list();
						bid_class_list.remove_3("active", "call", "resp")?;
						let (state_text, poll) = match state {
							BidState::Passive => {
								let caller_bid = player_bids[caller];
								let max = player_bids.iter().filter_map(|x| *x).max();
								(
									match caller_bid {
										Some(v) => {
											if awaiting_response {
												format!(
													"{} called {}, waiting for {} to respond.",
													players[caller],
													v,
													players[responder.unwrap()]
												)
											} else {
												format!(
													"{} must call higher than {}.",
													players[caller],
													max.unwrap_or(0)
												)
											}
										}
										None => match max {
											Some(v) => format!(
												"Waiting for {} to call higher than {}.",
												players[caller], v
											),
											None => format!(
												"Waiting for {} to make a first call.",
												players[caller]
											),
										},
									},
									true,
								)
							}
							BidState::MustCall { next_in_sequence } => {
								bid_class_list.add_2("active", "call")?;
								let max = player_bids.iter().filter_map(|x| *x).max();
								require_element_by_id(&document, ID_TABLE_BIDDING_CALL_INPUT)?
									.dyn_into::<HtmlInputElement>()
									.unwrap()
									.set_value_as_number(next_in_sequence as f64);
								(
									match max {
										Some(v) => format!("Current highest bid is {}.", v),
										None => format!("You must place a first bid."),
									},
									false,
								)
							}
							BidState::MustRespond => {
								bid_class_list.add_2("active", "resp")?;
								let called_bid = player_bids[caller].unwrap();
								(format!("{} called {}.", players[caller], called_bid), false)
							}
						};
						require_element_by_id(&document, ID_TABLE_BIDDING_STATE_TEXT)?
							.set_text_content(Some(&state_text));
						Ok(poll)
					}
					TableView::Declaring {
						called_game_value,
						skat_taken,
						state,
					} => {
						body.class_list().add_1("declaring")?;
						Self::render_covered_hand(
							&require_element_by_id(&document, ID_TABLE_SPACE)?,
							0,
						)?;
						let declare_ui = require_element_by_id(&document, ID_TABLE_DECLARE_UI)?;

						let hands = Self::split_opponents_visually(your_index, &hands);
						Self::render_covered_hand(
							&TableSeat::Left.require_hand_el(&document)?,
							*hands.1,
						)?;
						Self::render_covered_hand(
							&TableSeat::Right.require_hand_el(&document)?,
							*hands.2,
						)?;

						let declare_class_list = declare_ui.class_list();
						declare_class_list.remove_2("active", "skat-taken")?;
						if !skat_taken {
							Self::render_covered_hand(
								&require_element_by_id(&document, ID_TABLE_SKAT)?,
								2,
							)?;
						} else {
							declare_class_list.add_1("skat-taken")?;
							Self::render_covered_hand(
								&require_element_by_id(&document, ID_TABLE_SKAT)?,
								0,
							)?;
						}

						let (state_text, poll) = match state {
							DeclareState::Passive { declarer } => {
								Self::render_hand(
									&TableSeat::Own.require_hand_el(&document)?,
									your_hand,
									Some(GameType::Grand),
									CardControl::Passive,
								)?;
								(
									format!(
										"{} took the game with {}, waiting for their declaration.",
										players[declarer], called_game_value
									),
									true,
								)
							}
							DeclareState::Active => {
								declare_class_list.add_1("active")?;
								Self::render_hand(
									&TableSeat::Own.require_hand_el(&document)?,
									your_hand,
									Some(GameType::Grand),
									if skat_taken {
										CardControl::MultiSelect
									} else {
										CardControl::Passive
									},
								)?;
								(
									format!(
										"You took the game with {}, make a declaration.",
										called_game_value
									),
									false,
								)
							}
						};
						require_element_by_id(&document, ID_TABLE_DECLARING_STATE_TEXT)?
							.set_text_content(Some(&state_text));
						Ok(poll)
					}
					TableView::Playing {
						game_type,
						declarer_hand,
						last_trick,
						table,
						declarer,
						state,
						..
					} => {
						body.class_list().add_1("playing")?;
						Self::render_covered_hand(
							&require_element_by_id(&document, ID_TABLE_SKAT)?,
							0,
						)?;
						Self::render_hand(
							&require_element_by_id(&document, ID_TABLE_SPACE)?,
							table,
							None,
							CardControl::Passive,
						)?;
						if let Some(last_trick) = last_trick {
							Self::render_hand(
								&require_element_by_id(&document, ID_TABLE_LAST_TRICK)?,
								last_trick.into_iter(),
								None,
								CardControl::Passive,
							)?;
						} else {
							Self::render_covered_hand(
								&require_element_by_id(&document, ID_TABLE_LAST_TRICK)?,
								0,
							)?;
						}

						let hands = Self::split_opponents_visually(your_index, &hands);
						Self::render_covered_hand(
							&TableSeat::Left.require_hand_el(&document)?,
							*hands.1,
						)?;
						Self::render_covered_hand(
							&TableSeat::Right.require_hand_el(&document)?,
							*hands.2,
						)?;

						if let Some(declarer_hand) = declarer_hand {
							let seat = Self::map_opponent(your_index, declarer);
							if seat != TableSeat::Own {
								Self::render_hand(
									&seat.require_hand_el(&document)?,
									declarer_hand,
									Some(game_type),
									CardControl::Passive,
								)?;
							}
						}

						let state_text_lead = if declarer == your_index {
							format!("You are playing a game of {}", game_type)
						} else {
							format!("{} is playing a game of {}", players[declarer], game_type)
						};
						let (state_text, poll) = match state {
							PlayState::Passive { turn } => {
								Self::render_hand(
									&TableSeat::Own.require_hand_el(&document)?,
									your_hand,
									Some(game_type),
									CardControl::Passive,
								)?;
								(
									format!(
										"{}. Waiting for {} to pick a card.",
										state_text_lead, players[turn]
									),
									true,
								)
							}
							PlayState::Active => {
								Self::render_hand(
									&TableSeat::Own.require_hand_el(&document)?,
									your_hand,
									Some(game_type),
									CardControl::SingleSelect {
										onclick: Some(
											&js_sys::Reflect::get(&window, &"play_card".into())?
												.into(),
										),
									},
								)?;
								(format!("{}. Pick a card to play.", state_text_lead), false)
							}
						};

						require_element_by_id(&document, ID_TABLE_PLAYING_STATE_TEXT)?
							.set_text_content(Some(&state_text));
						Ok(poll)
					}
				}
			}
			Self::Concluded { .. } => Ok(true),
		}
	}
}

static SEED: Seed = Seed::from_bytes([0u8; 16]);

async fn advance_state(play_api: &str, resp: PlayResponse) -> Result<PlayResponse, JsError> {
	let resp = match &resp {
		PlayResponse::Playing { game, .. } => match game {
			State::Committing {
				your_index,
				player_commitments,
				..
			} => {
				// TODO: tracking of commitments and stuff
				if player_commitments[*your_index].is_none() {
					// render first, send request after
					apply_state(&resp)?;
					let commitment = Commitment::from_seed(&SEED);
					send_request(play_api, &PlayRequest::CommitSeed { commitment }).await?
				} else {
					resp
				}
			}
			State::Shuffling {
				your_index,
				player_seeds,
				..
			} => {
				// TODO: tracking of commitments and stuff
				if player_seeds[*your_index].is_none() {
					// render first, send request after
					apply_state(&resp)?;
					send_request(play_api, &PlayRequest::SetSeed { seed: SEED }).await?
				} else {
					resp
				}
			}
			_ => resp,
		},
		_ => resp,
	};
	Ok(resp)
}

fn apply_state(resp: &PlayResponse) -> Result<bool, JsError> {
	let view = GameView::from_response(&resp);
	log::info!("resulting view: {:?}", view);
	Ok(view.render_into_web()?)
}

#[wasm_bindgen]
pub async fn poll_state(play_api: &str, client_id: &str) -> Result<bool, JsError> {
	log::info!(
		"polling play_api {} with client_id = {}",
		play_api,
		client_id
	);
	let resp = send_request(play_api, &PlayRequest::PollState).await?;
	let resp = advance_state(play_api, resp).await?;
	apply_state(&resp)
}

#[wasm_bindgen]
pub async fn call_bid(play_api: &str, bid: GameValue) -> Result<Option<String>, JsError> {
	let resp = send_request(play_api, &PlayRequest::CallBid { bid: Some(bid) }).await?;
	match resp {
		PlayResponse::Error { message } => return Ok(Some(message)),
		other => apply_state(&other)?,
	};
	Ok(None)
}

#[wasm_bindgen]
pub async fn pass_bid(play_api: &str) -> Result<Option<String>, JsError> {
	let resp = send_request(play_api, &PlayRequest::CallBid { bid: None }).await?;
	match resp {
		PlayResponse::Error { message } => return Ok(Some(message)),
		other => apply_state(&other)?,
	};
	Ok(None)
}

#[wasm_bindgen]
pub async fn respond_bid(play_api: &str, hold: bool) -> Result<Option<String>, JsError> {
	let resp = send_request(play_api, &PlayRequest::RespondBid { hold }).await?;
	match resp {
		PlayResponse::Error { message } => return Ok(Some(message)),
		other => apply_state(&other)?,
	};
	Ok(None)
}

#[wasm_bindgen]
pub async fn take_skat(play_api: &str) -> Result<Option<String>, JsError> {
	let resp = send_request(play_api, &PlayRequest::TakeSkat).await?;
	match resp {
		PlayResponse::Error { message } => return Ok(Some(message)),
		other => apply_state(&other)?,
	};
	Ok(None)
}

#[wasm_bindgen]
pub async fn declare_game(
	play_api: &str,
	game_type: &str,
	modifiers: Option<String>,
	discard_cards: Vec<u8>,
) -> Result<Option<String>, JsError> {
	log::debug!("{:?}, {:?}, {:?}", game_type, modifiers, discard_cards);
	let game_type = match game_type {
		"diamonds" => GameType::Diamonds,
		"hearts" => GameType::Hearts,
		"spades" => GameType::Spades,
		"clubs" => GameType::Clubs,
		"grand" => GameType::Grand,
		"null" => GameType::Null,
		other => return Ok(Some(format!("invalid game type: {:?}", other))),
	};
	let modifiers = match modifiers.as_ref().map(|x| x.as_str()).unwrap_or("") {
		"" => None,
		"schneider" => Some(AnnouncableModifier::Schneider),
		"schwarz" => Some(AnnouncableModifier::Schwarz),
		"ouvert" => Some(AnnouncableModifier::Ouvert),
		other => return Ok(Some(format!("invalid modifier: {:?}", other))),
	};
	if discard_cards.len() != 2 && discard_cards.len() != 0 {
		return Ok(Some(format!("can only discard exactly two or no cards")));
	}
	let push_cards = if discard_cards.len() == 0 {
		None
	} else {
		let c1 = Card::from_bits(discard_cards[0]).unwrap();
		let c2 = Card::from_bits(discard_cards[1]).unwrap();
		Some([c1, c2])
	};
	let resp = send_request(
		play_api,
		&PlayRequest::Declare {
			game_type,
			modifiers,
			push_cards,
		},
	)
	.await?;
	match resp {
		PlayResponse::Error { message } => return Ok(Some(message)),
		other => apply_state(&other)?,
	};
	Ok(None)
}

#[wasm_bindgen]
pub async fn play_card(play_api: &str, card: u8) -> Result<Option<String>, JsError> {
	let card = Card::from_bits(card).unwrap();
	let resp = send_request(play_api, &PlayRequest::Play { card }).await?;
	match resp {
		PlayResponse::Error { message } => return Ok(Some(message)),
		other => apply_state(&other)?,
	};
	Ok(None)
}
