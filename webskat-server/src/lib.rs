mod service;

pub use service::{ClientId, Error as ServiceError, GameId, JoinKey, LogMode, Service, TableId};
