use std::fmt;
use std::sync::{
	atomic::{AtomicBool, Ordering},
	RwLock,
};

use poem::{
	get, handler,
	http::StatusCode,
	listener::TcpListener,
	web::{Form, Json, Path, Query, Redirect},
	IntoResponse, Response, Result, Route, Server,
};

use base64::Engine;

use serde::{de::Error as DeError, Deserialize, Deserializer, Serialize, Serializer};

use skat::{GameError, PerPlayer, State};

use webskat_common::{GameSummary, PerSeat, PlayRequest, PlayResponse};
use webskat_server::{ClientId, JoinKey, LogMode, Service, ServiceError, TableId};

fn init_service() -> Service {
	let db_path = std::env::var("SKAT_DATABASE_PATH").unwrap_or("./db".to_string());
	Service::new(db_path).expect("failed to open database")
}

#[cfg(debug_assertions)]
static DEVEL_MODE: AtomicBool = AtomicBool::new(true);

#[cfg(not(debug_assertions))]
static DEVEL_MODE: AtomicBool = AtomicBool::new(false);

lazy_static::lazy_static! {
	static ref SKAT: Service = init_service();
	static ref TERA: RwLock<tera::Tera> = RwLock::new(tera::Tera::new("templates/**/*.*").unwrap());
}

#[derive(Serialize)]
struct Empty {}

fn render_template_inner(name: &str, ctx: &tera::Context) -> tera::Result<String> {
	if DEVEL_MODE.load(Ordering::Relaxed) {
		let mut tera = TERA.write().expect("template engine poisoned");
		tera.full_reload()?;
		tera.render(name, ctx)
	} else {
		let tera = TERA.read().expect("template engine poisoned");
		tera.render(name, ctx)
	}
}

fn render_template<T: Serialize>(name: &str, data: T) -> String {
	let ctx = tera::Context::from_serialize(data).expect("invalid data passed to render_template");
	render_template_inner(name, &ctx).expect("failed to render template")
}

#[inline(always)]
fn render_html<T: Serialize>(name: &str, data: T) -> poem::web::Html<String> {
	poem::web::Html(render_template(name, data))
}

#[derive(Debug)]
struct OpaqueError(String);

impl fmt::Display for OpaqueError {
	fn fmt<'f>(&self, f: &'f mut fmt::Formatter) -> fmt::Result {
		f.write_str(self.0.as_str())
	}
}

impl std::error::Error for OpaqueError {}

macro_rules! opaque {
	($s:literal $(, $v:expr)*) => {
		OpaqueError(format!($s $(, $v)*))
	}
}

enum RedirectOr<T> {
	Redirect(Redirect),
	Other(T),
}

impl<T: IntoResponse> IntoResponse for RedirectOr<T> {
	fn into_response(self) -> Response {
		match self {
			Self::Redirect(r) => r.into_response(),
			Self::Other(o) => o.into_response(),
		}
	}
}

impl<T> From<T> for RedirectOr<T> {
	fn from(other: T) -> Self {
		Self::Other(other)
	}
}

struct Base64<T>(T);

impl<T: AsRef<[u8]>> fmt::Display for Base64<T> {
	fn fmt<'f>(&self, f: &'f mut fmt::Formatter) -> fmt::Result {
		let buf = base64::engine::general_purpose::URL_SAFE_NO_PAD.encode(self.0.as_ref());
		f.write_str(buf.as_str())
	}
}

impl<T: AsRef<[u8]>> Serialize for Base64<T> {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		let buf = base64::engine::general_purpose::URL_SAFE_NO_PAD.encode(self.0.as_ref());
		buf.serialize(serializer)
	}
}

impl<'de, E: fmt::Display, T: TryFrom<Vec<u8>, Error = E>> Deserialize<'de> for Base64<T> {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		let s = String::deserialize(deserializer)?;
		let v = base64::engine::general_purpose::URL_SAFE_NO_PAD
			.decode(&s)
			.map_err(D::Error::custom)?;
		Ok(Base64(v.try_into().map_err(D::Error::custom)?))
	}
}

#[handler]
fn init(Path(id): Path<String>) -> Result<poem::web::Html<String>> {
	let init_secret = match std::env::var("SKAT_TABLE_INIT_KEY") {
		Ok(v) => v,
		_ => {
			return Err(poem::error::InternalServerError(opaque!(
				"init key not set to a valid string"
			)))
		}
	};
	if init_secret != id {
		return Err(poem::error::Unauthorized(opaque!("invalid init key")));
	}
	Ok(render_html("init.html", Empty {}))
}

#[derive(Deserialize)]
struct InitForm {
	#[serde(default)]
	with_log: bool,
}

#[handler]
fn init_post(
	Path(id): Path<String>,
	Form(join_form): Form<InitForm>,
) -> Result<RedirectOr<poem::web::Html<String>>> {
	let init_secret = match std::env::var("SKAT_TABLE_INIT_KEY") {
		Ok(v) => v,
		_ => {
			return Err(poem::error::InternalServerError(opaque!(
				"init key not set to a valid string"
			)))
		}
	};
	if init_secret != id {
		return Err(poem::error::Unauthorized(opaque!("invalid init key")));
	}

	let log_mode = match join_form.with_log {
		true => Some(LogMode::SeegerFabian),
		false => None,
	};
	let (table_id, table) = match SKAT.new_table(log_mode) {
		Ok(v) => v,
		Err(_) => {
			return Err(poem::error::InternalServerError(opaque!(
				"failed to create new table"
			)))
		}
	};

	return Ok(RedirectOr::Redirect(Redirect::see_other(format!(
		"../../table/{}/join?join_key={}",
		Base64(table_id),
		Base64(*table.join_key())
	))));
}

fn handle_service_error(e: ServiceError) -> poem::error::Error {
	match e {
		ServiceError::NotFound => poem::error::NotFound(opaque!("no such table")),
		other => {
			log::error!("internal service error: {:?}", other);
			poem::error::InternalServerError(opaque!("ISE"))
		}
	}
}

#[derive(Deserialize, Serialize)]
struct JoinParams {
	join_key: Base64<JoinKey>,
}

enum JoinError {
	Service(ServiceError),
	InvalidJoinKey,
	NoSeatLeft,
}

impl From<ServiceError> for JoinError {
	fn from(other: ServiceError) -> Self {
		Self::Service(other)
	}
}

#[handler]
fn join(
	Path(Base64(_)): Path<Base64<TableId>>,
	Query(join_params): Query<JoinParams>,
) -> Result<poem::web::Html<String>> {
	Ok(render_html("join.html", join_params))
}

#[derive(Deserialize, Serialize)]
struct JoinPost {
	join_key: Base64<JoinKey>,
	name: String,
}

#[handler]
fn execute_join(
	Path(Base64(table_id)): Path<Base64<TableId>>,
	Form(JoinPost {
		join_key: Base64(join_key),
		name,
	}): Form<JoinPost>,
) -> Result<Redirect> {
	match SKAT.with_table_mut(&table_id, |table| {
		if table.join_key() != &join_key {
			return Err(JoinError::InvalidJoinKey);
		}
		match table.add_seat(name) {
			Some((client_id, _)) => Ok(client_id),
			None => Err(JoinError::NoSeatLeft),
		}
	}) {
		Ok(client_id) => Ok(Redirect::see_other(format!("play/{}", Base64(client_id)))),
		Err(JoinError::Service(e)) => Err(handle_service_error(e)),
		Err(JoinError::InvalidJoinKey) => {
			Err(poem::error::Unauthorized(opaque!("invalid join key")))
		}
		Err(JoinError::NoSeatLeft) => Err(poem::error::Conflict(opaque!("no seat left"))),
	}
}

enum PlayError {
	Service(ServiceError),
	NoSuchClient,
	Game(GameError),
}

impl From<ServiceError> for PlayError {
	fn from(other: ServiceError) -> Self {
		Self::Service(other)
	}
}

impl From<GameError> for PlayError {
	fn from(other: GameError) -> Self {
		Self::Game(other)
	}
}

#[derive(Serialize)]
struct PlayData {
	client_id: Base64<ClientId>,
	join_key: Base64<JoinKey>,
}

#[handler]
fn play(
	Path((Base64(table_id), Base64(client_id))): Path<(Base64<TableId>, Base64<ClientId>)>,
) -> Result<poem::web::Html<String>> {
	let join_key = match SKAT.with_table(&table_id, |table| {
		if table.client_info(&client_id).is_none() {
			Err(PlayError::NoSuchClient)
		} else {
			Ok(*table.join_key())
		}
	}) {
		Ok(v) => v,
		Err(PlayError::NoSuchClient) => {
			return Err(poem::error::Unauthorized(opaque!("invalid client key")))
		}
		Err(PlayError::Game(e)) => return Err(poem::error::BadRequest(e)),
		Err(PlayError::Service(e)) => return Err(handle_service_error(e)),
	};
	Ok(render_html(
		"play.html",
		PlayData {
			client_id: Base64(client_id),
			join_key: Base64(join_key),
		},
	))
}

enum SkipCommit<T, E> {
	SkipOnly(T),
	Err(E),
}

impl<T, E: From<ServiceError>> From<ServiceError> for SkipCommit<T, E> {
	fn from(other: ServiceError) -> Self {
		Self::Err(E::from(other))
	}
}

impl<T, E: From<GameError>> From<GameError> for SkipCommit<T, E> {
	fn from(other: GameError) -> Self {
		Self::Err(E::from(other))
	}
}

fn deskip<T, E>(result: Result<T, SkipCommit<T, E>>) -> Result<T, E> {
	match result {
		Ok(v) => Ok(v),
		Err(SkipCommit::SkipOnly(v)) => Ok(v),
		Err(SkipCommit::Err(e)) => Err(e),
	}
}

enum PlayState {
	Lobby {
		seats: PerSeat<Option<String>>,
	},
	Playing {
		players: PerPlayer<String>,
		seats: PerSeat<String>,
		past_games: Option<Vec<Option<GameSummary>>>,
		game: State,
	},
}

impl From<PlayState> for PlayResponse {
	fn from(other: PlayState) -> Self {
		match other {
			PlayState::Lobby { seats } => Self::Lobby { seats },
			PlayState::Playing {
				players,
				seats,
				past_games,
				game,
			} => Self::Playing {
				players,
				seats,
				past_games,
				game,
			},
		}
	}
}

trait ToJson: Sized {
	fn to_json(self) -> JsonExt<Self>;
}

impl ToJson for PlayResponse {
	fn to_json(self) -> JsonExt<Self> {
		JsonExt {
			data: Json(self),
			code: StatusCode::OK,
		}
	}
}

struct JsonExt<T> {
	code: poem::http::StatusCode,
	data: Json<T>,
}

impl<T> JsonExt<T> {
	fn with_code(mut self, code: poem::http::StatusCode) -> Self {
		self.code = code;
		self
	}
}

impl<T: Serialize + Send> IntoResponse for JsonExt<T> {
	fn into_response(self) -> poem::Response {
		let mut result = self.data.into_response();
		result.set_status(self.code);
		result
	}
}

impl<T: Serialize + Send> From<T> for JsonExt<T> {
	fn from(other: T) -> Self {
		Self {
			data: Json(other),
			code: poem::http::StatusCode::OK,
		}
	}
}

#[handler]
fn play_api(
	Path((Base64(table_id), Base64(client_id))): Path<(Base64<TableId>, Base64<ClientId>)>,
	Json(request): Json<PlayRequest>,
) -> JsonExt<PlayResponse> {
	match deskip(
		SKAT.with_table_and_current_game_mut(&table_id, |mut table_game| {
			let player = match table_game.table.client_player(&client_id) {
				Some(v) => v,
				None => return Err(SkipCommit::Err(PlayError::NoSuchClient)),
			};
			let players = table_game
				.table
				.player_info()
				.map_ref(|x| x.as_ref().map(|y| y.1.name().to_string()));
			let past_games = table_game
				.table
				.log_mode()
				.and_then(|_| Some(table_game.table.past_games().iter().map(|x| x.1).collect()));
			let seats = table_game
				.table
				.seats()
				.map_ref(|x| x.as_ref().map(|x| x.name().to_string()));
			let game = match table_game.get_or_create_game() {
				Some(game) => game,
				None => {
					return Ok(PlayState::Lobby {
						seats: table_game
							.table
							.seats()
							.map_ref(|x| x.as_ref().map(|y| y.name().to_string())),
					})
				}
			};
			// once we enter playing state, we MUST have all players present.
			let players = players.map(|x| x.unwrap());
			let seats = seats.map(|x| x.unwrap());
			let state = game.state_mut();
			match request {
				PlayRequest::PollState => {
					let player_state = state.state(player);
					table_game.try_end_game();
					if table_game.needs_commit() {
						// changed something, must not skip commit
						return Ok(PlayState::Playing {
							players,
							past_games,
							seats,
							game: player_state,
						});
					} else {
						return Err(SkipCommit::SkipOnly(PlayState::Playing {
							players,
							past_games,
							seats,
							game: player_state,
						}));
					}
				}
				PlayRequest::CommitSeed { commitment } => state.commit_seed(player, commitment)?,
				PlayRequest::SetSeed { seed } => state.set_seed(player, seed)?,
				PlayRequest::CallBid { bid } => state.call_bid(player, bid)?,
				PlayRequest::RespondBid { hold } => state.respond_bid(player, hold)?,
				PlayRequest::TakeSkat => state.take_skat(player)?,
				PlayRequest::Declare {
					game_type,
					modifiers,
					push_cards,
				} => state.declare(player, game_type, push_cards, modifiers)?,
				PlayRequest::Play { card } => state.play(player, card)?,
			};
			let player_state = state.state(player);
			table_game.try_end_game();
			Ok(PlayState::Playing {
				players,
				seats,
				past_games,
				game: player_state,
			})
		}),
	) {
		Ok(play_state) => PlayResponse::from(play_state).to_json(),
		Err(PlayError::NoSuchClient) => PlayResponse::Error {
			message: "no such client".to_string(),
		}
		.to_json()
		.with_code(StatusCode::UNAUTHORIZED),
		Err(PlayError::Service(_)) => PlayResponse::Error {
			message: "internal server error".to_string(),
		}
		.to_json()
		.with_code(StatusCode::INTERNAL_SERVER_ERROR),
		Err(PlayError::Game(e)) => PlayResponse::Error {
			message: format!("invalid action: {}", e),
		}
		.to_json()
		.with_code(StatusCode::BAD_REQUEST),
	}
}

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
	env_logger::init();
	match std::env::var("SKAT_DEVEL_MODE")
		.ok()
		.and_then(|x| match x.trim() {
			"true" | "1" => Some(true),
			"false" | "0" => Some(false),
			other => {
				log::warn!(
					"unrecognized value for SKAT_DEVEL_MODE, ignoring: {:?}",
					other
				);
				None
			}
		}) {
		Some(v) => {
			log::info!("development mode set to {:?} via environment", v);
			DEVEL_MODE.store(v, Ordering::Relaxed);
		}
		None => {
			log::info!(
				"development defaulted to {:?}",
				DEVEL_MODE.load(Ordering::Relaxed)
			);
		}
	};
	let app = Route::new()
		.at("/table/new/:key", get(init).post(init_post))
		.at("/table/:table_id/join", get(join).post(execute_join))
		.at("/table/:table_id/play/:client_id", get(play).post(play_api))
		.nest(
			"/static",
			poem::endpoint::StaticFilesEndpoint::new("static").show_files_listing(),
		);
	log::debug!("starting listener on http://127.0.0.1:7528/");
	Server::new(TcpListener::bind("[::]:7528")).run(app).await
}
