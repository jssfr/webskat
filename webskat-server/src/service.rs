/*!
# Skat Service

- Database of tables
- Each table has:

	- A set of associated client IDs and their seats
	- A list of associated game IDs plus their summary if available

- Each game has:

	- The current server game state
	- Future work: a log of actions in addition/instead of the server state
*/
use std::borrow::Borrow;
use std::fmt;
use std::io;

use thiserror::Error;

use rand::RngCore;

use serde::{de::DeserializeOwned, Deserialize, Serialize};

use skat::{state::ServerGameState, PerPlayer, Player};

use webskat_common::{GameSummary, PerSeat, Seat};

type StdResult<T, E> = std::result::Result<T, E>;

const ID_LEN: usize = 16;

fn serialize<T: Serialize>(t: &T) -> StdResult<Vec<u8>, rmp_serde::encode::Error> {
	let mut result = rmp_serde::to_vec_named(t)?;
	result.push(0x00);
	Ok(result)
}

fn deserialize<'de, T: Deserialize<'de>>(v: &'de [u8]) -> StdResult<T, rmp_serde::decode::Error> {
	let v = match v.split_last() {
		None => {
			return Err(rmp_serde::decode::Error::Uncategorized(
				io::Error::new(
					io::ErrorKind::UnexpectedEof,
					"missing format byte (empty data)",
				)
				.to_string(),
			)
			.into())
		}
		Some((&0, v)) => v,
		Some(_) => {
			return Err(rmp_serde::decode::Error::Uncategorized(
				io::Error::new(
					io::ErrorKind::InvalidData,
					format!("unknown format byte: {:?}", v[0]),
				)
				.to_string(),
			)
			.into())
		}
	};
	rmp_serde::from_slice(v)
}

#[inline(always)]
fn init_id() -> [u8; ID_LEN] {
	let mut result = [0u8; ID_LEN];
	rand::thread_rng().fill_bytes(&mut result[..]);
	result
}

#[derive(Error, Debug, Clone, Copy)]
pub enum IdFromVecError {
	#[error("wrong length")]
	WrongLength,
}

macro_rules! id_type {
	($name:ident) => {
		#[derive(Clone, Copy, Serialize, Deserialize, PartialEq, Eq, Hash)]
		pub struct $name([u8; ID_LEN]);

		impl $name {
			pub fn new() -> Self {
				Self(init_id())
			}
		}

		impl Borrow<[u8]> for $name {
			fn borrow(&self) -> &[u8] {
				&self.0[..]
			}
		}

		impl AsRef<[u8]> for $name {
			fn as_ref(&self) -> &[u8] {
				&self.0[..]
			}
		}

		impl AsRef<$name> for $name {
			fn as_ref(&self) -> &Self {
				self
			}
		}

		impl From<[u8; ID_LEN]> for $name {
			fn from(other: [u8; ID_LEN]) -> Self {
				Self(other)
			}
		}

		impl fmt::Debug for $name {
			fn fmt<'f>(&self, f: &'f mut fmt::Formatter) -> fmt::Result {
				let name = stringify!($name);
				write!(f, "{}(", name)?;
				for i in 0..ID_LEN {
					if i % 4 == 0 && i != 0 {
						f.write_str("-")?;
					}
					write!(f, "{:02x}", self.0[i])?;
				}
				f.write_str(")")
			}
		}

		impl TryFrom<&Vec<u8>> for $name {
			type Error = IdFromVecError;

			fn try_from(other: &Vec<u8>) -> StdResult<Self, Self::Error> {
				if other.len() != ID_LEN {
					return Err(IdFromVecError::WrongLength);
				}
				let mut v = [0u8; ID_LEN];
				v.copy_from_slice(&other);
				Ok(Self(v))
			}
		}

		impl TryFrom<Vec<u8>> for $name {
			type Error = IdFromVecError;

			fn try_from(other: Vec<u8>) -> StdResult<Self, Self::Error> {
				Self::try_from(&other)
			}
		}
	};
}

id_type!(TableId);
id_type!(GameId);
id_type!(ClientId);
id_type!(JoinKey);

#[derive(Debug, Serialize, Deserialize)]
pub struct SeatInfo {
	client_id: ClientId,
	name: String,
}

impl SeatInfo {
	pub fn name(&self) -> &str {
		&self.name
	}
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum LogMode {
	#[serde(rename = "seeger_fabian")]
	SeegerFabian = 0,
}

fn default_log_mode() -> Option<LogMode> {
	Some(LogMode::SeegerFabian)
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Table {
	client_map: PerSeat<Option<SeatInfo>>,
	join_key: JoinKey,
	past_games: Vec<(GameId, Option<GameSummary>)>,
	current_game: Option<GameId>,
	#[serde(default = "default_log_mode")]
	log_mode: Option<LogMode>,
}

impl Table {
	pub fn new() -> Self {
		Self {
			client_map: PerSeat::default(),
			join_key: JoinKey::new(),
			past_games: Vec::new(),
			log_mode: Some(LogMode::SeegerFabian),
			current_game: None,
		}
	}

	pub fn join_key(&self) -> &JoinKey {
		&self.join_key
	}

	pub fn seats(&self) -> &PerSeat<Option<SeatInfo>> {
		&self.client_map
	}

	fn map_seat(&self, seat: Seat) -> Option<Player> {
		// TODO: four seat games
		let offset = self.past_games.len();
		let seat_index = seat as u8 as usize;
		let seat_index = (seat_index + (offset % 3)) % 3;
		let result = (seat_index as u8).try_into().unwrap();
		Some(result)
	}

	fn map_player(&self, player: Player) -> Seat {
		// TODO: four seat games
		let offset = self.past_games.len();
		let player_index = player as u8 as usize;
		let player_index = (player_index + (3 - (offset % 3))) % 3;
		let result = (player_index as u8).try_into().unwrap();
		result
	}

	pub fn is_full(&self) -> bool {
		// TODO: four seat games
		self.client_map.iter().all(|x| x.is_some())
	}

	pub fn player_info(&self) -> PerPlayer<Option<(Seat, &SeatInfo)>> {
		let mut result = PerPlayer::default();
		for (seat, info) in self.client_map.enumerate_ref() {
			if let Some((player, info)) = self.map_seat(seat).zip(info.as_ref()) {
				result[player] = Some((seat, info));
			}
		}
		result
	}

	pub fn client_info(&self, client_id: &ClientId) -> Option<(Seat, &SeatInfo)> {
		for (seat, info) in self.client_map.enumerate_ref() {
			if let Some(info) = info.as_ref() {
				if info.client_id == *client_id {
					return Some((seat, info));
				}
			}
		}
		None
	}

	pub fn past_games(&self) -> &[(GameId, Option<GameSummary>)] {
		&self.past_games
	}

	pub fn client_player(&self, client_id: &ClientId) -> Option<Player> {
		// TODO(4seats): We need to figure out what happens if the fourth seat
		// is taken only after the first game was played.
		let seat = self.client_info(client_id)?.0;
		self.map_seat(seat)
	}

	pub fn add_seat(&mut self, name: String) -> Option<(ClientId, Seat)> {
		for (seat, info) in self.client_map.enumerate_mut() {
			if info.is_none() {
				let new_id = ClientId::new();
				*info = Some(SeatInfo {
					client_id: new_id,
					name,
				});
				return Some((new_id, seat));
			}
		}
		None
	}

	pub fn log_mode(&self) -> Option<LogMode> {
		self.log_mode
	}
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Game {
	table: TableId,
	state: ServerGameState,
}

impl Game {
	pub fn new(table_id: TableId) -> Self {
		Self {
			table: table_id,
			state: ServerGameState::new_game(),
		}
	}

	pub fn state(&self) -> &ServerGameState {
		&self.state
	}

	pub fn state_mut(&mut self) -> &mut ServerGameState {
		&mut self.state
	}
}

const CF_NAME_TABLES: &'static str = "tables";
const CF_NAME_GAMES: &'static str = "games";

#[derive(Error, Debug)]
pub enum Error {
	#[error("database error: {0}")]
	Db(rocksdb::Error),
	#[error("data write error: {0}")]
	Write(rmp_serde::encode::Error),
	#[error("data read error: {0}")]
	Read(rmp_serde::decode::Error),
	#[error("record not found")]
	NotFound,
}

impl From<rocksdb::Error> for Error {
	fn from(other: rocksdb::Error) -> Self {
		Self::Db(other)
	}
}

pub type Result<T> = StdResult<T, Error>;

pub struct TableGameMut<'x> {
	pub table_id: &'x TableId,
	pub table: &'x mut Table,
	pub game_id: &'x GameId,
	pub game: &'x mut Option<Game>,
	needs_commit: bool,
}

impl<'x> TableGameMut<'x> {
	pub fn get_or_create_game(&mut self) -> Option<&mut Game> {
		if !self.table.is_full() {
			return None;
		}
		Some(self.game.get_or_insert_with(|| {
			self.needs_commit = true;
			Game {
				table: *self.table_id,
				state: ServerGameState::new_game(),
			}
		}))
	}

	pub fn needs_commit(&self) -> bool {
		self.needs_commit
	}

	pub fn try_end_game(&mut self) {
		let game = match self.game.as_mut() {
			None => return,
			Some(game) => game,
		};

		let summary = match game.state.summarize() {
			Some(v) => Some(v),
			None => {
				if game.state.passed_out() {
					None
				} else {
					return;
				}
			}
		};

		// we have a summary, we need to pop the game out of the option, to have it dereferenced.
		*self.game = None;
		let summary = summary.map(|x| GameSummary::map(x, self.table.map_player(x.declarer)));
		self.table.past_games.push((*self.game_id, summary));
		self.needs_commit = true;
	}
}

pub struct Service {
	inner: rocksdb::TransactionDB,
}

impl Service {
	pub fn new(path: impl AsRef<std::path::Path>) -> StdResult<Self, rocksdb::Error> {
		let mut options = rocksdb::Options::default();
		options.create_if_missing(true);
		options.create_missing_column_families(true);
		let inner: rocksdb::TransactionDB = rocksdb::TransactionDB::open_cf(
			&options,
			&rocksdb::TransactionDBOptions::default(),
			path,
			[CF_NAME_TABLES, CF_NAME_GAMES],
		)?;
		Ok(Self { inner })
	}

	fn fetch_object<T: DeserializeOwned>(
		&self,
		cf: &rocksdb::ColumnFamily,
		txn: &rocksdb::Transaction<rocksdb::TransactionDB>,
		k: impl AsRef<[u8]>,
	) -> Result<Option<T>> {
		let data = match txn.get_pinned_for_update_cf(cf, k, true) {
			Ok(Some(v)) => v,
			Ok(None) => return Ok(None),
			Err(e) => return Err(e.into()),
		};
		log::trace!("deserializing {:?}", data.as_ref());
		Ok(Some(deserialize(&data).map_err(Error::Read)?))
	}

	fn fetch_table(
		&self,
		txn: &rocksdb::Transaction<rocksdb::TransactionDB>,
		table_id: impl AsRef<TableId>,
	) -> Result<Option<Table>> {
		let cf = self.inner.cf_handle(CF_NAME_TABLES).expect("tables cf");
		self.fetch_object(cf, txn, table_id.as_ref())
	}

	fn fetch_game(
		&self,
		txn: &rocksdb::Transaction<rocksdb::TransactionDB>,
		game_id: impl AsRef<GameId>,
	) -> Result<Option<Game>> {
		let cf = self.inner.cf_handle(CF_NAME_GAMES).expect("games cf");
		self.fetch_object(cf, txn, game_id.as_ref())
	}

	fn write_object<T: Serialize + fmt::Debug>(
		&self,
		cf: &rocksdb::ColumnFamily,
		txn: &rocksdb::Transaction<rocksdb::TransactionDB>,
		k: impl AsRef<[u8]>,
		v: &T,
	) -> Result<()> {
		let serialized = serialize(v).map_err(Error::Write)?;
		log::trace!("serialized {:?} as {:?}", v, serialized);
		txn.put_cf(cf, k, serialized)?;
		Ok(())
	}

	fn write_table(
		&self,
		txn: &rocksdb::Transaction<rocksdb::TransactionDB>,
		table_id: impl AsRef<TableId>,
		v: &Table,
	) -> Result<()> {
		let cf = self.inner.cf_handle(CF_NAME_TABLES).expect("tables cf");
		self.write_object(cf, txn, table_id.as_ref(), v)
	}

	fn write_game(
		&self,
		txn: &rocksdb::Transaction<rocksdb::TransactionDB>,
		game_id: impl AsRef<GameId>,
		v: &Game,
	) -> Result<()> {
		let cf = self.inner.cf_handle(CF_NAME_GAMES).expect("games cf");
		self.write_object(cf, txn, game_id.as_ref(), v)
	}

	pub fn new_table(&self, log_mode: Option<LogMode>) -> Result<(TableId, Table)> {
		let txn = self.inner.transaction();
		let id = TableId::new();
		let mut table = Table::new();
		table.log_mode = log_mode;
		self.write_table(&txn, id, &table)?;
		txn.commit().map_err(Error::Db)?;
		Ok((id, table))
	}

	pub fn with_table<T, E: From<Error>, F: FnOnce(&Table) -> StdResult<T, E>>(
		&self,
		table_id: impl AsRef<TableId>,
		f: F,
	) -> StdResult<T, E> {
		let txn = self.inner.transaction();
		let table = match self.fetch_table(&txn, &table_id)? {
			Some(v) => v,
			None => return Err(Error::NotFound.into()),
		};
		let v = f(&table)?;
		txn.rollback().map_err(Error::Db)?;
		Ok(v)
	}

	pub fn with_table_mut<T, E: From<Error>, F: FnOnce(&mut Table) -> StdResult<T, E>>(
		&self,
		table_id: impl AsRef<TableId>,
		f: F,
	) -> StdResult<T, E> {
		let txn = self.inner.transaction();
		let mut table = match self.fetch_table(&txn, &table_id)? {
			Some(v) => v,
			None => return Err(Error::NotFound.into()),
		};
		let v = f(&mut table)?;
		self.write_table(&txn, &table_id, &table)?;
		txn.commit().map_err(Error::Db)?;
		Ok(v)
	}

	pub fn with_table_and_current_game<
		T,
		E: From<Error>,
		F: FnOnce(&Table, &Option<Game>) -> StdResult<T, E>,
	>(
		&self,
		table_id: impl AsRef<TableId>,
		f: F,
	) -> StdResult<T, E> {
		let txn = self.inner.transaction();
		let table = match self.fetch_table(&txn, &table_id)? {
			Some(v) => v,
			None => return Err(Error::NotFound.into()),
		};
		let game = match &table.current_game {
			Some(game_id) => match self.fetch_game(&txn, game_id)? {
				Some(v) => Some(v),
				None => return Err(Error::NotFound.into()),
			},
			None => None,
		};
		let v = f(&table, &game)?;
		txn.rollback().map_err(Error::Db)?;
		Ok(v)
	}

	pub fn with_table_and_current_game_mut<
		T,
		E: From<Error>,
		F: FnOnce(TableGameMut<'_>) -> StdResult<T, E>,
	>(
		&self,
		table_id: impl AsRef<TableId>,
		f: F,
	) -> StdResult<T, E> {
		let table_id = table_id.as_ref();
		let txn = self.inner.transaction();
		let mut table = match self.fetch_table(&txn, table_id)? {
			Some(v) => v,
			None => return Err(Error::NotFound.into()),
		};
		let (game_id, mut game) = match &table.current_game {
			Some(game_id) => match self.fetch_game(&txn, game_id)? {
				Some(v) => (*game_id, Some(v)),
				None => return Err(Error::NotFound.into()),
			},
			None => (GameId::new(), None),
		};
		let v = f(TableGameMut {
			table_id: &table_id,
			table: &mut table,
			game_id: &game_id,
			game: &mut game,
			needs_commit: false,
		})?;
		let game_id = match game {
			Some(game) => {
				self.write_game(&txn, &game_id, &game)?;
				Some(game_id)
			}
			None => None,
		};
		table.current_game = game_id;
		self.write_table(&txn, &table_id, &table)?;
		txn.commit().map_err(Error::Db)?;
		Ok(v)
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_table_deserialize_v20230319() {
		// NOTE: if this test fails, that means that the newly built executable is unable to read existing databases.
		// That must be fixed before a release.
		let data = [
			132, 170, 99, 108, 105, 101, 110, 116, 95, 109, 97, 112, 147, 192, 192, 192, 168, 106,
			111, 105, 110, 95, 107, 101, 121, 220, 0, 16, 69, 204, 226, 29, 85, 87, 204, 148, 204,
			184, 204, 150, 50, 204, 131, 204, 220, 204, 151, 123, 85, 49, 61, 170, 112, 97, 115,
			116, 95, 103, 97, 109, 101, 115, 144, 172, 99, 117, 114, 114, 101, 110, 116, 95, 103,
			97, 109, 101, 192, 0,
		];
		let _: Table = deserialize(&data).expect("deserialization");
	}

	#[test]
	fn test_table_serialize_stability() {
		// NOTE: this test will fail when the data format changes significantly
		// That is intentional. You should then copy the old data into a deserialize test and adapt this test case so that it passes.

		let mut table = Table::new();
		// must fix the join key and other entropy in order to have this reproducible
		table.join_key = [
			69, 226, 29, 85, 87, 148, 184, 150, 50, 131, 220, 151, 123, 85, 49, 61,
		]
		.into();
		let data = serialize(&table).expect("serialization");
		assert_eq!(
			&data,
			&[
				132, 170, 99, 108, 105, 101, 110, 116, 95, 109, 97, 112, 147, 192, 192, 192, 168,
				106, 111, 105, 110, 95, 107, 101, 121, 220, 0, 16, 69, 204, 226, 29, 85, 87, 204,
				148, 204, 184, 204, 150, 50, 204, 131, 204, 220, 204, 151, 123, 85, 49, 61, 170,
				112, 97, 115, 116, 95, 103, 97, 109, 101, 115, 144, 172, 99, 117, 114, 114, 101,
				110, 116, 95, 103, 97, 109, 101, 192, 0
			][..]
		);
	}
}
